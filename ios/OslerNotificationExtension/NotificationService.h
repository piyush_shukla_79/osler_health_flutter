//
//  NotificationService.h
//  OslerNotificationExtension
//
//  Created by cosmos on 07/05/19.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
