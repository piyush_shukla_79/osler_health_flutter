import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:osler_health/models/chronic_diseases.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/reminder_notification.dart';
import 'package:osler_health/models/responses/error.dart';
import 'package:osler_health/models/responses/response_data.dart';
import 'package:osler_health/models/transaction.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/models/user_doctor.dart';
import 'package:osler_health/models/user_notes.dart';
import 'package:osler_health/models/user_payer.dart';
import 'package:osler_health/models/user_practice.dart';
import 'package:osler_health/serializers/types.dart';

part 'serializers.g.dart';

@SerializersFor([
  User,
  AppNotification,
  ReminderNotification,
  Reminder,
  UserNotes,
  UserPractice,
  UserDoctor,
  UserPayer,
  ResponseData,
  ChronicDiseases,
  NetworkError,
  Transaction
])
final Serializers serializers = (_$serializers.toBuilder()
      ..addBuilderFactory(userList, () => ListBuilder<User>())
      ..addBuilderFactory(
          notificationList, () => ListBuilder<AppNotification>())
      ..addBuilderFactory(
          reminderNotificationList, () => ListBuilder<ReminderNotification>())
      ..addBuilderFactory(reminderList, () => ListBuilder<Reminder>())
      ..addBuilderFactory(medicineList, () => ListBuilder<Medicine>())
      ..addBuilderFactory(userNotesList, () => ListBuilder<UserNotes>())
      ..addBuilderFactory(userPracticeList, () => ListBuilder<UserPractice>())
      ..addBuilderFactory(userDoctorList, () => ListBuilder<UserDoctor>())
      ..addBuilderFactory(userPayerList, () => ListBuilder<UserPayer>())
      ..addBuilderFactory(
          responseDataList, () => ListBuilder<ResponseDataList>())
      ..addBuilderFactory(
          chronicDiseasesList, () => ListBuilder<ChronicDiseases>())
      ..addBuilderFactory(transactionsList, () => ListBuilder<Transaction>())
      ..addPlugin(new StandardJsonPlugin())
      ).build();
