// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(AppNotification.serializer)
      ..add(ChronicDiseases.serializer)
      ..add(Gender.serializer)
      ..add(Medicine.serializer)
      ..add(NetworkError.serializer)
      ..add(Reminder.serializer)
      ..add(ReminderNotification.serializer)
      ..add(ResponseData.serializer)
      ..add(ResponseDataList.serializer)
      ..add(Transaction.serializer)
      ..add(User.serializer)
      ..add(UserDoctor.serializer)
      ..add(UserNotes.serializer)
      ..add(UserPayer.serializer)
      ..add(UserPractice.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ChronicDiseases)]),
          () => new ListBuilder<ChronicDiseases>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ReminderNotification)]),
          () => new ListBuilder<ReminderNotification>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(bool)]),
          () => new ListBuilder<bool>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(int)]),
          () => new ListBuilder<int>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>())
      ..addBuilderFactory(
          const FullType(ResponseDataList, const [const FullType(Medicine)]),
          () => new ResponseDataListBuilder<Medicine>())
      ..addBuilderFactory(
          const FullType(ResponseDataList, const [const FullType(UserNotes)]),
          () => new ResponseDataListBuilder<UserNotes>())
      ..addBuilderFactory(
          const FullType(ResponseDataList, const [const FullType(Reminder)]),
          () => new ResponseDataListBuilder<Reminder>())
      ..addBuilderFactory(
          const FullType(ResponseData, const [const FullType(UserPractice)]),
          () => new ResponseDataBuilder<UserPractice>())
      ..addBuilderFactory(
          const FullType(ResponseData, const [const FullType(UserDoctor)]),
          () => new ResponseDataBuilder<UserDoctor>())
      ..addBuilderFactory(
          const FullType(ResponseData, const [const FullType(UserPayer)]),
          () => new ResponseDataBuilder<UserPayer>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ChronicDiseases)]),
          () => new ListBuilder<ChronicDiseases>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
