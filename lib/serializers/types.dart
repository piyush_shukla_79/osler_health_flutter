import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/reminder_notification.dart';
import 'package:osler_health/models/responses/response_data.dart';
import 'package:osler_health/models/transaction.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/models/user_doctor.dart';
import 'package:osler_health/models/user_notes.dart';
import 'package:osler_health/models/user_payer.dart';
import 'package:osler_health/models/user_practice.dart';
import 'package:osler_health/presentation/user_details/chronic_disease.dart';

const userList = FullType(BuiltList, [FullType(User)]);
const notificationList = FullType(BuiltList, [FullType(AppNotification)]);
const reminderNotificationList =
    FullType(BuiltList, [FullType(ReminderNotification)]);
const reminderList = FullType(BuiltList, [FullType(Reminder)]);
const medicineList = FullType(BuiltList, [FullType(Medicine)]);
const userNotesList = FullType(BuiltList, [FullType(UserNotes)]);
const responseDataList = FullType(BuiltList, [FullType(ResponseDataList)]);
const chronicDiseasesList = FullType(BuiltList, [FullType(ChronicDiseasePage)]);
const transactionsList = FullType(BuiltList, [FullType(Transaction)]);
const userPracticeList = FullType(BuiltList, [FullType(UserPractice)]);
const userDoctorList = FullType(BuiltList, [FullType(UserDoctor)]);
const userPayerList = FullType(BuiltList, [FullType(UserPayer)]);
