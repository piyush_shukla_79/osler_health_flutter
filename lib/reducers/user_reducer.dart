import 'package:built_collection/built_collection.dart';
import 'package:osler_health/actions/actions.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/transaction.dart';
import 'package:redux/redux.dart';

Reducer<UserState> userReducer = combineReducers(
  [
    TypedReducer<UserState, LogIn>(logInReducer),
    TypedReducer<UserState, CheckSessionComplete>(checkSessionReducer),
    TypedReducer<UserState, UpdateUserInfo>(updateUserInfo),
    TypedReducer<UserState, LogInComplete>(logInCompleteReducer),
    TypedReducer<UserState, FetchLoggedInUserDetails>(startLoading),
    TypedReducer<UserState, UpdateLoggedInUser>(startLoading),
    TypedReducer<UserState, LogIn>(startLoading),
    TypedReducer<UserState, LogInComplete>(stopLoading),
    TypedReducer<UserState, StopLoading>(loadingStop),
    TypedReducer<UserState, LoadTransactionsComplete>(loadTransactions),
  ],
);

UserState updateUserInfo(UserState userState, UpdateUserInfo action) {
  return userState.rebuild((b) => b
    ..profile.replace(action.user)
    ..isLoading = false
    ..isLoaded = true
    ..isLoggedIn = true);
}

UserState checkSessionReducer(
    UserState userState, CheckSessionComplete action) {
  return userState.rebuild((b) => b
    ..profile =
        action.currentUser != null ? action.currentUser.toBuilder() : null
    ..isLoggedIn = action.isSuccess);
}

UserState logInReducer(UserState state, LogIn action) {
  state = state.rebuild((b) => b..isLoading = true);
  return state;
}

UserState logInCompleteReducer(UserState state, LogInComplete action) {
  return state.rebuild((b) => b
    ..profile = action.currentUser.toBuilder()
    ..isLoading = false);
}

UserState startLoading(UserState state, LoadingTrigger action) {
  return state.rebuild((b) => b..isLoading = true);
}

UserState stopLoading(UserState state, LoadingTrigger action) {
  return state.rebuild((b) => b
    ..isLoading = false
    ..isLoggedIn = true);
}

UserState loadingStop(UserState state, StopLoading action) {
  return state.rebuild((b) => b..isLoading = false);
}

UserState loadTransactions(UserState state, LoadTransactionsComplete action) {
  return state.rebuild((b) => b
    ..transactions =
        BuiltList<Transaction>.from(action.transactions).toBuilder());
}
