import 'package:osler_health/actions/medicine_action.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:redux/redux.dart';

Reducer<MedicineState> medicineReducer = combineReducers([
  TypedReducer<MedicineState, ListMedicinesComplete>(listMedicinesComplete),
  TypedReducer<MedicineState, FetchLoggedInUserDetails>(startLoading),
  TypedReducer<MedicineState, CreateMedicineComplete>(createComplete)
]);

MedicineState listMedicinesComplete(
    MedicineState state, ListMedicinesComplete action) {
  return state.addAll(action.medicines).rebuild(((b) => b..isLoading = false));
}

MedicineState startLoading(
    MedicineState state, FetchLoggedInUserDetails action) {
  return state.rebuild((b) => b..isLoading = true);
}

MedicineState createComplete(
    MedicineState state, CreateMedicineComplete action) {
  return state.addOne(action.medicine);
}
