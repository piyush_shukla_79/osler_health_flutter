import 'package:osler_health/actions/reminder_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:redux/redux.dart';

Reducer<ReminderState> reminderReducer = combineReducers([
  TypedReducer<ReminderState, ListReminders>(listReminders),
  TypedReducer<ReminderState, ListRemindersComplete>(listReminderComplete),
  TypedReducer<ReminderState, UpdateReminder>(updateReminder),
  TypedReducer<ReminderState, CreateReminder>(createReminder),
  TypedReducer<ReminderState, UpdateReminderComplete>(updateComplete),
  TypedReducer<ReminderState, CreateReminderComplete>(createComplete)
]);

ReminderState listReminders(ReminderState state, ListReminders action) {
  return state.rebuild((b) => b..isLoading = true);
}

ReminderState listReminderComplete(
    ReminderState state, ListRemindersComplete action) {
  return state.addAll(action.reminders).rebuild((b) => b..isLoading = false);
}

ReminderState updateReminder(ReminderState state, UpdateReminder action) {
  return state.rebuild((b) => b..isLoading = true);
}

ReminderState createReminder(ReminderState state, CreateReminder action) {
  return state.rebuild((b) => b..isLoading = true);
}

ReminderState updateComplete(
    ReminderState state, UpdateReminderComplete action) {
  return state.updateOne(action.reminder).rebuild((b) => b..isLoading = false);
}

ReminderState createComplete(
    ReminderState state, CreateReminderComplete action) {
  return state.addOne(action.reminder).rebuild((b) => b..isLoading = false);
}
