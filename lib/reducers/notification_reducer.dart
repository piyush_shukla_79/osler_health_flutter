import 'package:osler_health/actions/notification_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:redux/redux.dart';

Reducer<AppNotificationState> notificationReducer = combineReducers([
  TypedReducer<AppNotificationState, ListNotification>(listNotification),
  TypedReducer<AppNotificationState, ListNotificationComplete>(
      listNotificationComplete),
  TypedReducer<AppNotificationState, UpdateNotification>(updateNotification)
]);

AppNotificationState listNotification(
    AppNotificationState appNotificationState, ListNotification action) {
  return appNotificationState.rebuild((b) => b..isLoading = true);
}

AppNotificationState listNotificationComplete(
    AppNotificationState appNotificationState,
    ListNotificationComplete action) {
  return appNotificationState.addAll(action.notifications).rebuild((b) => b
    ..isLoading = false
    ..isLoaded = true);
}

AppNotificationState updateNotification(
    AppNotificationState state, UpdateNotification action) {
  return state.updateOne(action.notification);
}
