import 'package:osler_health/actions/actions.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/reducers/medicine_reducer.dart';
import 'package:osler_health/reducers/notification_reducer.dart';
import 'package:osler_health/reducers/reminder_reducer.dart';
import 'package:osler_health/reducers/user_reducer.dart';
import 'package:redux/redux.dart';

AppState appReducer(AppState state, action) {
  if(action.runtimeType==Logout){
    print("log out");
    return AppState();
  }
  else {
    state = state.rebuild((b) =>
    b
      ..user.replace(userReducer(state.user, action))
      ..appNotification
          .replace(notificationReducer(state.appNotification, action))
      ..reminder.replace(reminderReducer(state.reminder, action))
      ..medicines.replace(medicineReducer(state.medicines, action))
    );
  }
  return state;
}

Reducer<bool> loadingReducer =
    combineReducers<bool>([TypedReducer<bool, LoadingAction>(setLoading)]);

Reducer<AppState> logoutReducer =
    combineReducers([TypedReducer<AppState, Logout>(logout)]);

bool setLoading(bool state, LoadingAction action) {
  return true;
}

bool setLoadingComplete(bool state, LoadingCompleteAction action) {
  return false;
}

AppState logout(AppState state, Logout action) {
//  return AppState();
  return AppState();
}
