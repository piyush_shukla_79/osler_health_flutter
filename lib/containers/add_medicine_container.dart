import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/actions/medicine_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/presentation/medicines/medicine_form.dart';
import 'package:redux/redux.dart';

class AddMedicineContainer extends StatelessWidget {
  AddMedicineContainer();

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, _ViewModel model) {
        return MedicineForm(
          onSave: model.save,
        );
      },
    );
  }
}

class _ViewModel {
  Function save;

  _ViewModel({this.save});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(save: (Map<String, dynamic> medicine) {
      print("in onsave function in add medicine container");
      print(medicine);
      store.dispatch(CreateMedicine(medicine));
    });
  }
}
