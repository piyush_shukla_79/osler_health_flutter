import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/login_page.dart';
import 'package:redux/redux.dart';

class LogInContainer extends StatelessWidget {
  final User user;

  LogInContainer({this.user});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, _ViewModel model) {
        return Scaffold(
          body: LoginPage(
            logIn: model.logIn,
            isLoading: model.isLoading,
          ),
        );
      },
    );
  }
}

class _ViewModel {
  Function logIn;
  bool isLoading;

  _ViewModel({this.isLoading, this.logIn});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
        logIn: (userName, password) {
          store.dispatch(
            LogIn(userName, password),
          );
        },
        isLoading: store.state.user.isLoading);
  }
}
