import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/home/home_screen.dart';
import 'package:osler_health/presentation/home_page.dart';
import 'package:redux/redux.dart';

class HomeContainer extends HomePageChild {
  HomeContainer({Function toggleSidebar}) : super(toggleSidebar);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, _ViewModel model) {
        return model.userIsLoading && model.notificationIsLoading &&
            !model.userIsLoaded && !model.notificationIsLoaded
            ? Center(
          child: CircularProgressIndicator(),
        )
            : HomeScreen(
          user: model.user,
          medicines: model.medicines,
          notifications: model.notifications,
          reminders: model.reminders,
          toggleSidebar: toggleSidebar,
        );
      },
    );
  }
}

class _ViewModel {
  User user;
  List<AppNotification> notifications;
  List<Medicine> medicines;
  List<Reminder> reminders;
  bool userIsLoading;
  bool notificationIsLoading;
  bool userIsLoaded;
  bool notificationIsLoaded;

  _ViewModel(
      {this.user, this.notifications, this.medicines, this.reminders, this.notificationIsLoading, this.userIsLoading, this.notificationIsLoaded, this.userIsLoaded});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      user: store.state.user.profile,
      notifications: store.state.appNotification.getAll(),
      medicines: store.state.medicines.getAll(),
      reminders: store.state.reminder.getAll(),
      userIsLoading: store.state.user.isLoading,
      notificationIsLoading: store.state.appNotification.isLoading,
      userIsLoaded: store.state.user.isLoaded,
      notificationIsLoaded: store.state.appNotification.isLoaded,
    );
  }
}
