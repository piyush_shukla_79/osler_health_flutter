import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/actions/reminder_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/presentation/medicines/reminder/reminder_page.dart';
import 'package:redux/redux.dart';

class ReminderContainer extends StatelessWidget {
  final int medicationId;

  ReminderContainer({this.medicationId});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (state) {
        return _ViewModel.fromStore(state, medicationId);
      },
      builder: (context, _ViewModel model) {
        return ReminderPage(
          reminder: model.reminder,
          medicine: model.medicine,
          reminderUpdated: model.didUpdateReminder,
          isSaving: model.isSaving
        );
      },
    );
  }
}

class _ViewModel {
  Reminder reminder;
  Medicine medicine;
  Function didUpdateReminder;
  bool isSaving;

  _ViewModel({this.reminder, this.medicine, this.didUpdateReminder,this.isSaving});

  static _ViewModel fromStore(Store<AppState> store, int id) {
    return _ViewModel(
        reminder: store.state.reminder.getEntities()[id],
        medicine: store.state.medicines.getEntities()[id],
        isSaving: store.state.reminder.isLoading,
        didUpdateReminder: (Reminder reminder) {
          if (reminder.id != null) {
            store.dispatch(UpdateReminder(reminder));
          } else {
            store.dispatch(CreateReminder(reminder));
          }
        });
  }
}
