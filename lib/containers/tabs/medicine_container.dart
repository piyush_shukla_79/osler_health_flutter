import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/home_page.dart';
import 'package:osler_health/presentation/medicines/medicine_page.dart';
import 'package:redux/redux.dart';

class MedicineContainer extends HomePageChild {
  MedicineContainer({Function toggleSidebar}) : super(toggleSidebar);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, _ViewModel model) {
        return MedicinePage(
          medicines: model.medicines,
          reminders: model.reminders,
          isLoading: model.isLoading,
          toggleSidebar: toggleSidebar,
          user: model.user,
        );
      },
    );
  }
}

class _ViewModel {
  bool isLoading;
  List<Medicine> medicines;
  List<Reminder> reminders;
  User user;

  _ViewModel(
      {@required this.medicines,
      @required this.isLoading,
      @required this.reminders,
      @required this.user});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      medicines: store.state.medicines.getAll(),
      reminders: store.state.reminder.getAll(),
      isLoading: store.state.medicines.isLoading,
      user: store.state.user.profile,
    );
  }
}
