import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/actions/notification_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/presentation/home_page.dart';
import 'package:osler_health/presentation/notifications/notifications_page.dart';
import 'package:redux/redux.dart';

class NotificationContainer extends HomePageChild {
  NotificationContainer({this.toggleSidebar}) : super(toggleSidebar);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
        converter: (store) => _ViewModel.fromStore(store, context),
        builder: (context, _ViewModel model) {
          return model.isLoading && !model.isLoaded
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : NotificationsPage(
                  notifications: model.notifications,
                  isLoading: model.isLoading,
                  notificationTapped: model.notificationTapped,
                  onRefresh: model.onRefresh,
                  toggleSidebar: toggleSidebar,
                );
        });
  }

  @override
  final Function toggleSidebar;
}

class _ViewModel {
  List<AppNotification> notifications;
  bool isLoading;
  bool isLoaded;
  Function notificationTapped;
  Function onRefresh;

  _ViewModel(
      {@required this.notifications,
      @required this.isLoading,
      @required this.isLoaded,
      @required this.notificationTapped,
      @required this.onRefresh});

  static _ViewModel fromStore(Store<AppState> store, BuildContext context) {
    return _ViewModel(
        notifications: store.state.appNotification.getAll(),
        isLoading: store.state.appNotification.isLoading,
        isLoaded: store.state.appNotification.isLoaded,
        notificationTapped: (AppNotification notification) {
          store.dispatch(UpdateNotification(notification: notification));
        },
        onRefresh: () {
          store.dispatch(ListNotification());
        });
  }
}
