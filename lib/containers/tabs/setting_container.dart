import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/setting.dart';
import 'package:osler_health/presentation/home_page.dart';
import 'package:osler_health/presentation/settings/setting_page.dart';
import 'package:redux/redux.dart';

class SettingContainer extends HomePageChild {
//  final Setting setting;

  SettingContainer({Function toggleSidebar}) : super(toggleSidebar);

//  SettingContainer({this.setting});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, _ViewModel model) {
        return SettingPage(
            setting: model.setting,
            settingUpdate: model.settingUpdate,
            toggleSidebar: toggleSidebar);
      },
    );
  }
}

class _ViewModel {
  Function settingUpdate;
  Setting setting;

  _ViewModel({this.setting, this.settingUpdate});

  static _ViewModel fromStore(Store<AppState> store) {
    final user = store.state.user.profile;
    final settings = Setting((b) => b
      ..email = user.emailSubscribed
      ..notification = user.notificationSubscribed
      ..messages = user.smsSubscribed);
    return _ViewModel(
        setting: settings,
        settingUpdate: (Setting setting) {
          store.dispatch(UpdateLoggedInUser(user.rebuild((b) => b
            ..emailSubscribed = setting.email
            ..smsSubscribed = setting.messages
            ..notificationSubscribed = setting.notification)));
        });
  }
}
