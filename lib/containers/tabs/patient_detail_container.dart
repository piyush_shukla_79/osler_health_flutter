import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/home_page.dart';
import 'package:osler_health/presentation/user_details/patient_details.dart';
import 'package:redux/redux.dart';

class PatientDetailContainer extends HomePageChild {
  PatientDetailContainer({Function toggleSidebar}) : super(toggleSidebar);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, _ViewModel model) {
        return PatientDetails(
          user: model.user,
          onUserUpdate: model.onUserUpdate,
          toggleSidebar: toggleSidebar,
        );
      },
    );
  }
}

class _ViewModel {
  Function onUserUpdate;
  User user;

  _ViewModel({this.user, this.onUserUpdate});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
        user: store.state.user.profile,
        onUserUpdate: (User user) {
          store.dispatch(
            UpdateUserInfo(user),
          );
        });
  }
}
