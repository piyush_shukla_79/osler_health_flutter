import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/user_details/health_details.dart';
import 'package:redux/redux.dart';

class HealthDetailsContainer extends StatelessWidget {
  HealthDetailsContainer();

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, _ViewModel model) {
        return HealthDetails(
          user: model.user,
        );
      },
    );
  }
}

class _ViewModel {
  User user;

  _ViewModel({@required this.user});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(user: store.state.user.profile);
  }
}
