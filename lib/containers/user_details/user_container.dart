import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/user_details/contact_details_form.dart';
import 'package:osler_health/presentation/user_details/general_details.dart';
import 'package:redux/redux.dart';

class UserDetailsContainer extends StatelessWidget {
  UserDetailsContainer();

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, _ViewModel model) {
        return GeneralDetails(
          user: model.user,
          isSaving: model.isLoading,
          onSave: model.onSave,
        );
      },
    );
  }
}

class _ViewModel {
  User user;
  bool isLoading;
  OnSave onSave;

  _ViewModel({this.user, this.isLoading, this.onSave});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
        user: store.state.user.profile,
        isLoading: store.state.user.isLoading,
        onSave: (User user) {
          print("on save called");
          store.dispatch(UpdateLoggedInUser(user));
        });
  }
}
