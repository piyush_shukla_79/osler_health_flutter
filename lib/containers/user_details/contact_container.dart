import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/user_details/contact_details.dart';
import 'package:osler_health/presentation/user_details/contact_details_form.dart';
import 'package:redux/redux.dart';

class ContactContainer extends StatelessWidget {
  ContactContainer();

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (context, _ViewModel model) {
        return Scaffold(
          body: ContactDetails(
            user: model.user,
            onSave: model.onSave,
            isSaving: model.isLoading,
          ),
        );
      },
    );
  }
}

class _ViewModel {
  User user;
  OnSave onSave;
  bool isLoading;

  _ViewModel({@required this.user, @required this.onSave, @required this.isLoading});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
        user: store.state.user.profile,
        isLoading: store.state.user.isLoading,
        onSave: (User user) {
          store.dispatch(UpdateLoggedInUser(user));
        });
  }
}