import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/transaction.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/user_details/transactions_list.dart';
import 'package:redux/redux.dart';

class TransactionsContainer extends StatelessWidget {
  TransactionsContainer();

  @override
  Widget build(BuildContext context) {
    return StoreConnector(
      converter: _ViewModel.fromStore,
      builder: (context, _ViewModel model) {
        return TransactionsList(
          transactions: model.transactions,
          user: model.user,
        );
      },
    );
  }
}

class _ViewModel {
  List<Transaction> transactions;
  User user;

  _ViewModel({@required this.transactions, @required this.user});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
        transactions: store.state.user.transactions.toList(),
        user: store.state.user.profile);
  }
}
