import 'package:flutter/material.dart';
import 'package:osler_health/models/mixins/unsubscribe.dart';
import 'package:osler_health/presentation/forget_password.dart';
import 'package:osler_health/presentation/home_page.dart';
import 'package:osler_health/services/broadcaster_service.dart';
import 'package:osler_health/services/error_service.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/string.dart';
import 'package:osler_health/utils/styles.dart';

class LoginPage extends StatefulWidget {
  final Function logIn;
  final bool isLoading;

  LoginPage({this.logIn, this.isLoading});

  @override
  State<StatefulWidget> createState() {
    return _LoginPage(isLoading);
  }
}

class _LoginPage extends State<LoginPage> with UnsubscribeMixin {
  String email;
  String password;
  bool isLoading;
  bool hasErrorOccurred = false;

  _LoginPage(this.isLoading);

  @override
  void initState() {
    super.initState();

    ErrorService.getInstance()
        .on(ErrorCode.login)
        .takeUntil(destroy$)
        .listen((ErrorEvent event) {
      setState(() {
        isLoading = false;
        hasErrorOccurred = true;
      });

      showSnackBar(context, event.error.message);
    });
    ErrorService.getInstance()
        .on(ErrorCode.noUser)
        .takeUntil(destroy$)
        .listen((ErrorEvent event) {
      setState(() {
        isLoading = false;
        hasErrorOccurred = true;
      });
      showSnackBar(context, "No User Found");
    });

    BroadcasterService()
        .on(BroadcasterEventType.loginComplete)
        .takeUntil(destroy$)
        .listen((event) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (_context) => HomePage()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Container(),
                      flex: 3,
                    ),
                    Expanded(
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        width: MediaQuery.of(context).size.width * 0.46,
                        child: Image.asset('assets/app_logo.png'),
                      ),
                      flex: 15,
                    ),
                    Expanded(
                      flex: 90,
                      child: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                'assets/login_page.png',
                              ),
                              fit: BoxFit.fill),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.12),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Expanded(
                        child: Container(),
                        flex: 27,
                      ),
                      Expanded(
                        child: Container(
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'Login',
                                style: TextStyle(
                                  fontSize: 32,
                                  letterSpacing: 1.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontFamily: 'Righteous',
                                ),
                              ),
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01,
                              ),
                              Container(
                                width: 50,
                                height: 4.0,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(4.0)),
                              )
                            ],
                          ),
                        ),
                        flex: 11,
                      ),
                      Expanded(
                        child: Container(),
                        flex: 22,
                      ),
                      Expanded(
                        child: Container(
                          height: Styles.textFieldHeight,
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.07),
                          decoration: Styles.textFieldDecoration,
                          child: TextField(
                            style: Styles.inputTextStyle,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Email',
                              hintStyle: TextStyle(
                                  color: Styles.primaryColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            onChanged: (value) {
                              email = value;
                            },
                          ),
                        ),
                        flex: 7,
                      ),
                      Expanded(
                        child: Container(),
                        flex: 4,
                      ),
                      Expanded(
                        child: Container(
                          height: Styles.textFieldHeight,
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.07),
                          decoration: Styles.textFieldDecoration,
                          child: TextField(
                            obscureText: true,
                            style: Styles.inputTextStyle,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Password',
                              hintStyle: TextStyle(
                                  color: Styles.primaryColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            onChanged: (value) {
                              password = value;
                            },
                          ),
                        ),
                        flex: 7,
                      ),
                      Expanded(
                        child: Container(),
                        flex: 4,
                      ),
                      Expanded(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width*0.25,
                              height: 50,
                              alignment: Alignment.centerLeft,
                              child: FlatButton(
                                padding: EdgeInsets.only(right: 10.0),
                                child: Text('Forget Password?',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        color: Styles.primaryColor,
                                        fontWeight: FontWeight.bold),
                                    maxLines: null),
                                onPressed: _forgetPassword,
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width*0.35,
                              height: 50.0,
                              child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0)),
                                  color: Styles.primaryColor,
                                  child: isLoading
                                      ? CircularProgressIndicator()
                                      : HeaderText(
                                          data: LOG_IN_BUTTON + ' > ',
                                          style: Styles.buttonText),
                                  onPressed: isLoading ? null : _login),
                            ),
                          ],
                        ),
                        flex: 7,
                      ),
                      Expanded(
                        child: Container(),
                        flex: 19,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _forgetPassword() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => ForgetPassword()));
  }

  _login() async {
    if (email != null && password != null) {
      if (email.isNotEmpty && password.isNotEmpty) {
        setState(() {
          isLoading = true;
          hasErrorOccurred = false;
        });
        widget.logIn(email, password);
      } else {
        showSnackBar(context, "Email and Password are required");
      }
    }
    else {
      showSnackBar(context, "Email and Password are required");
    }
  }

  @override
  void dispose() {
    onDispose();
    super.dispose();
  }
}
