import 'dart:async';

import 'package:flutter/material.dart';
import 'package:osler_health/actions/notification_action.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/containers/tabs/home_container.dart';
import 'package:osler_health/containers/tabs/medicine_container.dart';
import 'package:osler_health/containers/tabs/notification_container.dart';
import 'package:osler_health/containers/tabs/patient_detail_container.dart';
import 'package:osler_health/containers/tabs/setting_container.dart';
import 'package:osler_health/main.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/sidebar.dart';
import 'package:osler_health/utils/styles.dart';

class HomePage extends StatefulWidget {
  final HomePageIndex currentIndex;
  final User user;

  HomePage({this.currentIndex = HomePageIndex.home, this.user});

  @override
  State<StatefulWidget> createState() {
    OslerHealthApp.store.dispatch(FetchLoggedInUserDetails());
    OslerHealthApp.store.dispatch(ListNotification());
    return _HomePage(currentIndex, user);
  }
}

class _HomePage extends State<HomePage> {
  HomePageIndex _currentIndex;
  final User user;

  _HomePage(this._currentIndex, this.user);

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    OslerHealthApp.store.dispatch(LoadTransactions());
  }

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () => onBackHome(),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Styles.appColor,
          key: _scaffoldKey,
          drawer: Sidebar(onTabTapped: onTabTapped,currentIndex: _currentIndex,),
          body: _getPage(_currentIndex),
        ),
      ),
    );
  }

  onTabTapped(HomePageIndex index) {
    if (_scaffoldKey.currentState.isDrawerOpen)
      Navigator.of(context).pop();
    setState(() {
      _currentIndex = index;
    });

    if (index == HomePageIndex.notifications &&
        !OslerHealthApp.store.state.appNotification.isLoaded &&
        !OslerHealthApp.store.state.appNotification.isLoading) {
      OslerHealthApp.store.dispatch(ListNotification());
    }
  }

  _getPage(HomePageIndex index) {
    switch (index) {
      case HomePageIndex.home:
        return HomeContainer(toggleSidebar: _toggleSidebar);
        break;
      case HomePageIndex.profile:
        return PatientDetailContainer(toggleSidebar: _toggleSidebar);
        break;
      case HomePageIndex.medicines:
        return MedicineContainer(toggleSidebar: _toggleSidebar);
        break;
      case HomePageIndex.notifications:
        return NotificationContainer(toggleSidebar: _toggleSidebar);
        break;
      case HomePageIndex.settings:
        return SettingContainer(toggleSidebar: _toggleSidebar);
        break;
    }
  }

  _toggleSidebar() {
    if (_scaffoldKey.currentState != null) {
      _scaffoldKey.currentState.openDrawer();
    }
  }

  onBackHome() {
    if (_currentIndex != HomePageIndex.home) {
      onTabTapped(HomePageIndex.home);
    }
    else
      return Future.value(true);
  }
}

abstract class HomePageChild extends StatelessWidget {
  final Function toggleSidebar;

  HomePageChild(this.toggleSidebar);
}

enum HomePageIndex { home, profile, medicines, notifications, settings }
