import 'package:flutter/material.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';

class ForgetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body:
        SingleChildScrollView(
          child: Container(
            height: MediaQuery
                .of(context)
                .size
                .height,
            width: MediaQuery
                .of(context)
                .size
                .width,
            child: Stack(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Container(),
                      flex: 3,
                    ),
                    Expanded(
                      child: Container(
                        height: MediaQuery
                            .of(context)
                            .size
                            .height * 0.1,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width * 0.46,
                        child: Image.asset('assets/app_logo.png'),
                      ),
                      flex: 15,
                    ),

                    Expanded(
                      flex: 90,
                      child: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/login_page.png',),
                              fit: BoxFit.fill
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: MediaQuery
                      .of(context)
                      .size
                      .width * 0.12),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Expanded(child: Container(), flex: 27,),
                      Expanded(
                        child: Container(
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'Forgot Password',
                                style: TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontFamily: 'Righteous',
                                ),
                              ),
                              SizedBox(
                                height: MediaQuery
                                    .of(context)
                                    .size
                                    .height * 0.01,
                              ),
                              Container(
                                width: 190,
                                height: 4.0,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(4.0)),
                              )
                            ],
                          ),
                        ),
                        flex: 11,
                      ),
                      Expanded(child: Container(),
                        flex: 22,),
                      Expanded(
                        child: Text(
                          'A link will be sent to your registered account to reset your password.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Styles.primaryColor,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w400,
                            fontFamily: 'JosefinSans',
                          ),
                        ),
                        flex: 7,
                      ),
                      Expanded(child: Container(),
                        flex: 4,),
                      Expanded(
                        child: Container(
                          height: Styles.textFieldHeight,
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(left: MediaQuery
                              .of(context)
                              .size
                              .width * 0.07),
                          decoration: Styles.textFieldDecoration,
                          child: TextField(
                            style: Styles.inputTextStyle,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Email',
                              hintStyle: TextStyle(
                                  color: Styles.primaryColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            onChanged: (value) {},
                          ),
                        ),
                        flex: 7,
                      ),
                      Expanded(child: Container(),
                        flex: 4,),
                      Expanded(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              height: 50.0,
                              child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0)),
                                  color: Styles.primaryColor,
                                  child: HeaderText(
                                      data: 'Reset Password' + ' > ',
                                      style: Styles.buttonText),
                                  onPressed: () {}),
                            ),
                          ],
                        ),
                        flex: 7,
                      ),
                      Expanded(child: Container(),
                        flex: 19,)
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
