import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/presentation/user_details/contact_details_form.dart';
import 'package:osler_health/utils/helpers.dart';
import 'package:osler_health/utils/styles.dart';

class GeneralDetails extends StatefulWidget {
  final User user;
  final bool isSaving;
  final OnSave onSave;

  GeneralDetails({
    @required this.user,
    @required this.isSaving,
    @required this.onSave,
  });

  @override
  State<StatefulWidget> createState() {
    return GeneralDetailsState(user: user);
  }
}

class GeneralDetailsState extends State<GeneralDetails> {
  User user;
  BuildContext _context;

  GeneralDetailsState({this.user});
  ScrollController _controller = new ScrollController();

  @override
  void initState() {
    super.initState();
    _controller.addListener((){
      if(_controller.position.userScrollDirection ==
          ScrollDirection.forward ||
          _controller.position.userScrollDirection ==
              ScrollDirection.reverse){
        FocusScope.of(context).requestFocus(FocusNode());
      }
    });
  }

  @override
  void didUpdateWidget(GeneralDetails oldWidget) {
    if (oldWidget.isSaving && !widget.isSaving) {
      setState(() {
        isEditing = false;
      });
    }

    super.didUpdateWidget(oldWidget);
  }

  final _formKey = GlobalKey<FormState>();

  bool isEditing = false;

//  TextEditingController _textController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    _context = context;
    _formKey.currentState?.validate();
    print(user.createdAt);

    return SafeArea(
      child: Scaffold(
        body: GestureDetector(
          onTap: (){
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Column(
            children: <Widget>[
              CustomAppbar(
                isEditing: isEditing,
                topLeftCTAIcon: Image.asset('assets/back_arrow_icon.png'),
                bottomRightCTAIcon:
                    (!isEditing) ? Image.asset('assets/edit_icon.png') : Icon(
                      Icons.save,
                      color: Colors.white,
                    ),
                navTitle: user.getFullName(),
                pageTitle: "General Details",
                pageImage: Image.asset('assets/general_image.png'),
                onTopLeftIconTap: (!isEditing) ? _backAction : _editModeOff,
                onBottomRightIconTap: (!isEditing) ? _editModeOn : _onSaveTap,
                isSaving: widget.isSaving,
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                isShrinked: false,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  controller: _controller,
                  padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding:
                          EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                          width: MediaQuery.of(context).size.width - 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color:!isEditing? Colors.white:Styles.opacityColor,),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 7.0,
                              ),
                              Text(
                                'Patient Id:',
                                style: Styles.detailsTitleText,
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                              !isEditing
                                  ? Text(user.id.toString()??'N/A',
                                  style: Styles.detailsResponseText)
                                  : TextFormField(
                                decoration:
                                InputDecoration(border: InputBorder.none),
                                style: Styles.detailsResponseText,
                                controller: TextEditingController(
                                    text: user.id.toString()),
                                enabled: false,
                                validator: null,
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          padding:
                          EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                          width: MediaQuery.of(context).size.width - 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              color: Colors.white),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 7.0,
                              ),
                              Text(
                                'First Name:',
                                style: Styles.detailsTitleText,
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                              !isEditing
                                  ? Text(user.firstName??'N/A', style: Styles.detailsResponseText)
                                  : TextFormField(
                                decoration:
                                InputDecoration(border: InputBorder.none),
                                initialValue: user.firstName,
                                autovalidate: true,
                                style: Styles.detailsResponseText,
                                onSaved: _updateFirstName,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "Required";
                                  }
                                },
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          padding:
                          EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                          width: MediaQuery.of(context).size.width - 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              color: Colors.white),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 7.0,
                              ),
                              Text(
                                'Last Name:',
                                style: Styles.detailsTitleText,
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                              !isEditing
                                  ? Text(user.lastName??'N/A', style: Styles.detailsResponseText)
                                  : TextFormField(
                                decoration:
                                InputDecoration(border: InputBorder.none),
                                style: Styles.detailsResponseText,
                                initialValue: user.lastName,
                                onSaved: _updateLastName,
                                validator: null,
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          padding:
                          EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                          width: MediaQuery.of(context).size.width - 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              color: Colors.white),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 7.0,
                              ),
                              Text(
                                'Date of birth:',
                                style: Styles.detailsTitleText,
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                              !isEditing
                                  ? Text(user.dob??'N/A', style: Styles.detailsResponseText)
                                  : InkWell(
                                onTap: () {
                                  _selectedDate();
                                },
                                child: IgnorePointer(
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                        border: InputBorder.none),
                                    style: Styles.detailsResponseText,
                                    controller:
                                    TextEditingController(text: user.dob),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          padding:
                          EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                          width: MediaQuery.of(context).size.width - 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: !isEditing? Colors.white:Styles.opacityColor,),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 7.0,
                              ),
                              Text(
                                'Status:',
                                style: Styles.detailsTitleText,
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                              !isEditing
                                  ? Text(user.status??'N/A', style: Styles.detailsResponseText)
                                  : TextFormField(
                                decoration:
                                InputDecoration(border: InputBorder.none),
                                style: Styles.detailsResponseText,

                                controller:
                                TextEditingController(text: user.status),
                                enabled: false,
                                validator: null,
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          padding:
                          EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                          width: MediaQuery.of(context).size.width - 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: !isEditing? Colors.white:Styles.opacityColor,),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 7.0,
                              ),
                              Text(
                                'Date Added:',
                                style: Styles.detailsTitleText,
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                              !isEditing
                                  ? Text(user.createdAt??"N/A", style: Styles.detailsResponseText)
                                  : TextFormField(
                                decoration:
                                InputDecoration(border: InputBorder.none),
                                style: Styles.detailsResponseText,

                                controller: TextEditingController(
                                    text: user.createdAt),
                                enabled: false,
                                validator: null,
                              ),
                              SizedBox(
                                height: 7.0,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _backAction() {
    Navigator.pop(_context);
  }

  _editModeOn() {
    if (!isEditing) {
      setState(() {
        isEditing = true;
      });
    }
  }

  _editModeOff() {
    if (isEditing) {
      setState(() {
        isEditing = false;
      });
    }
  }

  _updateFirstName(String fn) {
    setState(() {
      user = user.rebuild((b) => b..firstName = fn);
    });
  }

  _updateLastName(String ln) {
    setState(() {
      user = user.rebuild((b) => b..lastName = ln);
    });
  }

  _updateDob(String dob) {
    setState(() {
      user = user.rebuild((b) => b..dob = dob);
    });
  }

  _selectedDate() async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year - 100),
      lastDate: DateTime.now(),
      builder: (BuildContext context,Widget child){
        return FittedBox(
          child: child,
        );
      }
    );
    if (picked != null) {
      _updateDob(yyyyMdddd(picked));
    }
  }

  _onSaveTap() {
    if (widget.isSaving != null && !widget.isSaving) {

      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        widget.onSave(user);
      }
    }
  }
}
