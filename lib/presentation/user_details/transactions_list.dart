import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:osler_health/models/transaction.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/helpers.dart';
import 'package:osler_health/utils/string.dart';
import 'package:osler_health/utils/styles.dart';

class TransactionsList extends StatelessWidget {
  final List<Transaction> transactions;

  final User user;

  TransactionsList({@required this.transactions, this.user});

  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    double width = MediaQuery.of(context).size.width;
    double usableWidth = (width * 0.60);
    double avatarWidth = width * 0.2;
    ScrollController _controller = new ScrollController();

    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: Column(
          children: <Widget>[
            CustomAppbar(
              topLeftCTAIcon: Image.asset('assets/back_arrow_icon.png'),
              navTitle: user.getFullName(),
              pageTitle: REWARD_HISTORY,
              pageImage: Image.asset('assets/transaction_image.png'),
              onTopLeftIconTap: _backAction,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              isShrinked: false,
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              height: 0.5,
              color: Styles.opacityColor,
            ),
            transactions.length > 0
                ? Expanded(
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      controller: _controller,
                      child: ListView.builder(
                        controller: _controller,
                        shrinkWrap: true,
                        itemCount: transactions.length,
                        itemBuilder: (context, index) {
                          final transaction = transactions.elementAt(index);
                          return Column(
                            children: <Widget>[
                              HeaderText(
                                data: transactionDate(
                                    getDateTime(transaction.createdAt)),
                                style: TextStyle(
                                    color: Styles.iconBackColor,
                                    fontSize: 12.0),
                              ),
                              SizedBox(
                                height: 2.0,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Container(
                                    width: avatarWidth,
                                    child: Stack(
                                      children: <Widget>[
                                        Image.asset(
                                            'assets/read_transaction_symbol.png'),
                                        Container(
                                          width: avatarWidth * 0.99,
                                          height: avatarWidth * 0.99,
                                          alignment: Alignment.center,
                                          child: transaction.isCredit()
                                              ? Icon(
                                                  Icons.add,
                                                  color: Styles.accentColor,
                                                  size: 50,
                                                )
                                              : Icon(
                                                  Icons.remove,
                                                  color: Colors.orange,
                                                  size: 50,
                                                ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: usableWidth,
                                    margin: EdgeInsets.only(
                                        top: 10.0, right: 10.0, bottom: 10.0),
                                    child: Text(
                                      getText(transaction),
                                      style: Styles.detailsResponseText,
                                      maxLines: 2,
                                    ),
                                  ),
                                ],
                              ),
                              Divider(
                                height: 20.0,
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  )
                : Container(
              height: MediaQuery.of(context).size. height * 0.25,
              alignment: Alignment.center,
                    child: Text(
                      'No Rewards',
                      style: Styles.titleText,
                    ),
                  )
          ],
        ),
      ),
    );
  }

  String getText(Transaction transaction) {
    if (transaction.points > 0) {
      return "${transaction.points.abs()} points have been credited to your account.";
    } else {
      return "${transaction.points.abs()} points have been debited from your account.";
    }
  }

  _backAction() {
    Navigator.pop(_context);
  }
}
