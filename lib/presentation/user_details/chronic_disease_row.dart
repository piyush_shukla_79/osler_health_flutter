import 'package:flutter/material.dart';
import 'package:osler_health/models/chronic_diseases.dart';
import 'package:osler_health/services/notifcation_listener_service.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';

class ChronicDiseaseRow extends StatefulWidget {
  final double height;
  final double width;
  final ChronicDiseases chronicDisease;
  bool isExpanded;
  final ScrollController controller;
  final int index;
  BuildContext contextParent;

  ChronicDiseaseRow(
      {this.width,
      this.height,
      this.chronicDisease,
      this.isExpanded,
      this.controller,
      this.index,
      this.contextParent});

  @override
  State<StatefulWidget> createState() {
    return _ChronicDiseaseRow(
        height: height,
        width: width,
        chronicDisease: chronicDisease,
        controller: controller);
  }
}

class _ChronicDiseaseRow extends State<ChronicDiseaseRow> {
  final double height;
  final double width;
  final ChronicDiseases chronicDisease;
  final ScrollController controller;
  OnTap onTap;

  _ChronicDiseaseRow(
      {this.width, this.height, this.chronicDisease, this.controller});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          OnTap(tapId: widget.index).dispatch(widget.contextParent);
        });
      },
      child: AnimatedContainer(
        duration: Duration(seconds: 5),
        margin: EdgeInsets.symmetric(
            horizontal: 30.0, vertical: 10.0),
        padding: EdgeInsets.only(
            top: 7.0, bottom: 7.0, left: 8.0, right: 8.0),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            HeaderText(
              data: chronicDisease.diagnosisDate != null
                  ? chronicDisease.diagnosisDate
                  : "N/A",
              style: Styles.detailsTitleText,
            ),
            SizedBox(
              height: 10.0,
            ),
            HeaderText(
              data: chronicDisease.label != null ? chronicDisease.label : "N/A",
              style: Styles.detailsResponseText,
            ),
            Column(
              children: <Widget>[
                widget.isExpanded
                    ? Column(
                        children: <Widget>[
                          SizedBox(
                            height: 5.0,
                          ),
                          ListView.builder(
                              controller: controller,
                              shrinkWrap: true,
                              itemCount:
                                  chronicDisease.subchronicDiseases.length,
                              itemBuilder: (BuildContext context, int index) {
                                ChronicDiseases item =
                                    chronicDisease.subchronicDiseases[index];
                                return subDiseases(item);
                              },
                          ),
                          (chronicDisease.subchronicDiseases.length > 0) ?
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Icon(Icons.keyboard_arrow_up,
                                color: Styles.accentColor,),
                            ],
                          ) : SizedBox(),
                        ],
                      )
                    : SizedBox(),
              ],
            ),
            (!widget.isExpanded && chronicDisease.subchronicDiseases.length > 0)
                ? Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Icon(Icons.keyboard_arrow_down,
                  color: Styles.accentColor,),
              ],
            )
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  Widget subDiseases(ChronicDiseases item) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 5.0,
        ),
        HeaderText(
          data: item.label != null ? item.label : "N/A",
          style: TextStyle(
              color: Styles.iconBackColor,
              fontSize: 15.0,
              fontWeight: FontWeight.w500,
              fontFamily: 'Righteous'
          ),
        ),
        SizedBox(
          height: 5.0,
        ),
        HeaderText(
          data: item.diagnosisDate != null
              ? item.diagnosisDate.toString()
              : "N/A",
          style: TextStyle(
              color: Styles.primaryColor,
              fontSize: 15.0,
              fontWeight: FontWeight.w500,
              fontFamily: 'JosefinSans'),
        ),
        SizedBox(
          height: 5.0,
        ),
      ],
    );
  }
}
