import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/services/broadcaster_service.dart';
import 'package:osler_health/utils/styles.dart';
import 'package:osler_health/utils/validation.dart';

typedef OnSave = void Function(User user);

class ContactDetailsForm extends StatefulWidget {
  final User user;

  final Function onSave;
  final bool isSaving;

  ContactDetailsForm({this.user, this.onSave, this.isSaving});

  @override
  ContactDetailsFormState createState() {
    return ContactDetailsFormState(user: user);
  }
}

class ContactDetailsFormState extends State<ContactDetailsForm> {
  User user;

  final _formKey = GlobalKey<FormState>();

  ContactDetailsFormState({this.user});

  ScrollController controller = ScrollController();

  @override
  void initState() {
    BroadcasterService().on(BroadcasterEventType.onSavedTap).listen((data) {
      onSaveTap();
    });
    super.initState();

    controller.addListener(() {
      if (controller.positions.first.userScrollDirection ==
              ScrollDirection.forward ||
          controller.positions.first.userScrollDirection ==
              ScrollDirection.reverse) {
        FocusScope.of(context).requestFocus(FocusNode());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        controller: controller,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 27.0),
          padding: EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
            child:
            Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                width: MediaQuery.of(context).size.width - 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10.0,
                    ),
                    Text("CELL PHONE", style: Styles.detailsTitleText),
                    TextFormField(
                      decoration: InputDecoration(border: InputBorder.none),
                      initialValue: user.cellPhone,
                      style: Styles.detailsResponseText,
                      keyboardType: TextInputType.phone,
                      onEditingComplete: () {
                        _validateForm();
                      },
                      onFieldSubmitted: _removeFocus,
                      onSaved: (value) {
                        _updateCellPhoneValue(value);
                      },
                      validator: (value) {
                      },
                    ),
                    SizedBox(
                      height: 10.0,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                width: MediaQuery.of(context).size.width - 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10.0,
                    ),
                    Text("WORK PHONE", style: Styles.detailsTitleText),
                    TextFormField(
                      decoration: InputDecoration(border: InputBorder.none),
                      initialValue: user.workPhone,
                      style: Styles.detailsResponseText,
                      keyboardType: TextInputType.phone,
                      onEditingComplete: () {
                        _validateForm();
                      },
                      onFieldSubmitted: _removeFocus,
                      onSaved: (value) {
                        _updateWorkPhoneValue(value);
                      },
                      validator: (value) {
//                            if (!_phoneValidity[index]) {
//                              return "Not a valied phone number";
//                            }
                      },
                    ),
                    SizedBox(
                      height: 10.0,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                width: MediaQuery.of(context).size.width - 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10.0,
                    ),
                    Text("HOME PHONE", style: Styles.detailsTitleText),
                    TextFormField(
                      decoration: InputDecoration(border: InputBorder.none),
                      initialValue: user.homePhone,
                      style: Styles.detailsResponseText,
                      keyboardType: TextInputType.phone,
                      onEditingComplete: () {
                        _validateForm();
                      },
                      onFieldSubmitted: _removeFocus,
                      onSaved: (value) {
                        _updateHomePhoneValue(value);
                      },
                      validator: (value) {},
                    ),
                    SizedBox(
                      height: 10.0,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                width: MediaQuery.of(context).size.width - 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "PERSONAL EMAIL",
                      style: Styles.detailsTitleText,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      decoration:
                      InputDecoration(border: InputBorder.none),
                      initialValue: user.personalEmail,
                      style: Styles.detailsResponseText,
                      onEditingComplete: () {
                        _validateForm();
                      },
                      onFieldSubmitted: _removeFocus,
                      onSaved: _updatePersonalEmail,
                      validator: (value) {
                        if (value.isNotEmpty && !Validate.isValidEmail(value)) {
                          return "Not a valid e-mail address";
                        }
                      },
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                width: MediaQuery.of(context).size.width - 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "WORK EMAIL",
                        style: Styles.detailsTitleText,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        decoration:
                        InputDecoration(border: InputBorder.none),
                        initialValue: user.workEmail,
                        style: Styles.detailsResponseText,
                        onEditingComplete: () {
                          _validateForm();
                        },
                        onFieldSubmitted: _removeFocus,
                        onSaved: _updateWorkEmail,
                        validator: (value) {
                          if (value.isNotEmpty &&
                              !Validate.isValidEmail(value)) {
                            return "Not a valid e-mail address";
                          }
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                    ]),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                width: MediaQuery.of(context).size.width - 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "CITY",
                      style: Styles.detailsTitleText,
                    ),
                    TextFormField(
                        decoration: InputDecoration(border: InputBorder.none),
                        initialValue: user.city,
                        style: Styles.detailsResponseText,
                        onEditingComplete: () {
                          _validateForm();
                        },
                        onFieldSubmitted: _removeFocus,
                        onSaved: _updateCity),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                width: MediaQuery.of(context).size.width - 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "STATE",
                      style: Styles.detailsTitleText,
                    ),
                    TextFormField(
                      decoration: InputDecoration(border: InputBorder.none),
                      initialValue: user.state,
                      style: Styles.detailsResponseText,
                      onFieldSubmitted: _removeFocus,
                      onEditingComplete: () {
                        _validateForm();
                      },
                      onSaved: _updateState,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                width: MediaQuery.of(context).size.width - 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "ZIP CODE",
                      style: Styles.detailsTitleText,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      decoration:
                      InputDecoration(border: InputBorder.none),
                      initialValue: user.zipCode,
                      style: Styles.detailsResponseText,
                      onEditingComplete: () {
                        _validateForm();
                      },
                      onFieldSubmitted: _removeFocus,
                      onSaved: _updateZipCode,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                width: MediaQuery.of(context).size.width - 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "ADDRESS LINE 1",
                      style: Styles.detailsTitleText,
                    ),
                    TextFormField(
                      decoration: InputDecoration(border: InputBorder.none),
                      initialValue: user.address1,
                      style: Styles.detailsResponseText,
                      onEditingComplete: () {
                        _validateForm();
                      },
                      onFieldSubmitted: _removeFocus,
                      onSaved: _updateAddress1,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
                width: MediaQuery.of(context).size.width - 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "ADDRESS LINE 2",
                      style: Styles.detailsTitleText,
                    ),
                    TextFormField(
                      decoration: InputDecoration(border: InputBorder.none),
                      initialValue: user.address2,
                      style: Styles.detailsResponseText,
                      onFieldSubmitted: _removeFocus,
                      onEditingComplete: () {
                        _validateForm();
                      },
                      onSaved: _updateAddress2,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ));
  }

  bool _validateForm() {
    return _formKey.currentState.validate();
  }

  _updatePersonalEmail(String value) {
    setState(() {
      user = user.rebuild((b) => b..personalEmail = value);
    });
  }

  _updateWorkEmail(String value) {
    setState(() {
      user = user.rebuild((b) => b..workEmail = value);
    });
  }

  _updateCellPhoneValue(String value) {
    setState(() {
      user = user.rebuild((b) => b..cellPhone = value);
    });
  }

  _updateWorkPhoneValue(String value) {
    setState(() {
      user = user.rebuild((b) => b..workPhone = value);
    });
  }

  _updateHomePhoneValue(String value) {
    setState(() {
      user = user.rebuild((b) => b..homePhone = value);
    });
  }

  _updateCity(String value) {
    setState(() {
      user = user.rebuild((b) => b..city = value);
    });
  }

  _updateState(String value) {
    setState(() {
      user = user.rebuild((b) => b..state = value);
    });
  }

  _updateZipCode(String value) {
    setState(() {
      user = user.rebuild((b) => b..zipCode = value);
    });
  }

  _updateAddress1(String value) {
    setState(() {
      user = user.rebuild((b) => b..address1 = value);
    });
  }

  _updateAddress2(String value) {
    setState(() {
      user = user.rebuild((b) => b.address2 = value);
    });
  }

  onSaveTap() {
    if (widget.isSaving != null && !widget.isSaving) {
      if (_validateForm()) {
        _formKey.currentState.save();
        widget.onSave(user);
      }
    }
  }

  void _removeFocus(value){
    FocusScope.of(context).requestFocus(FocusNode());
  }
}
