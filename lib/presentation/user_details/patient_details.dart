import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:osler_health/containers/user_details/chronic_diseases_container.dart';
import 'package:osler_health/containers/user_details/contact_container.dart';
import 'package:osler_health/containers/user_details/health_details_container.dart';
import 'package:osler_health/containers/user_details/transactions_container.dart';
import 'package:osler_health/containers/user_details/user_container.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/utils/string.dart';
import 'package:osler_health/utils/styles.dart';

import '../home_page.dart';

class PatientDetails extends StatelessWidget {
  final User user;
  final Function onUserUpdate;
  final Function toggleSidebar;

  PatientDetails({this.user, this.onUserUpdate, this.toggleSidebar});

  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: Column(
          children: <Widget>[
            CustomAppbar(
              topLeftCTAIcon: Image.asset('assets/sidebar_icon.png'),
              onTopLeftIconTap: toggleSidebar,
              topRightCTAIcon: Image.asset('assets/notifications_icon.png'),
              onTopRightIconTap: _notifications,
              pageImage: Hero(
                  tag: 'profileImage',
                  child: Image(image: CachedNetworkImageProvider(user
                      .getAvatarUrl()))),
              pageTitle: user.getFullName(),
              navTitle: "",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              isShrinked: true,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 24.69,
                  width: 23.09,
                  margin: EdgeInsets.only(right: 10.0),
                  child: Image.asset(
                    "assets/star.png",
                    color: Styles.accentColor,
                    fit: BoxFit.cover,
                  ),
                ),
                Text(
                  user.points?.toString() ?? "0",
                  style: TextStyle(
                      color: Styles.accentColor,
                      fontSize: 24,
                      letterSpacing: 1.5),
                ),
              ],
            ),
            SizedBox(
              height: 15.0,
            ),
            Expanded(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(_context).size.width*0.1),
                  child: IntrinsicWidth(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        FlatButton(
                          child: _recordRow(
                            "assets/general_image.png",
                            GENERAL_DETAILS,
                            "Name , Date of Birth etc",
                          ),
                          onPressed: _generalDetails,
                        ),
                        FlatButton(
                          child: _recordRow(
                            "assets/contact_image.png",
                            CONTACT_DETAILS,
                            "Phones , Email Address etc",
                          ),
                          onPressed: _contact,
                        ),
                        FlatButton(
                          child: _recordRow(
                            "assets/health_image.png",
                            HEALTH_DETAILS,
                            "Temperature , BP etc",
                          ),
                          onPressed: _healthDetails,
                        ),
                        FlatButton(
                          child: _recordRow(
                            "assets/chronic_diseases_image.png",
                            CHRONIC_DISEASE,
                            "List of Chronic Diseases",
                          ),
                          onPressed: _chronicDiseases,
                        ),
                        FlatButton(
                          child: _recordRow(
                            "assets/transaction_image.png",
                            REWARD_HISTORY,
                            "Reward Points Transactions",
                          ),
                          onPressed: _transactions,
                        ),
                        SizedBox(
                          height: 25.9,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _recordRow(imgUrl, title, describe) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: MediaQuery
          .of(_context)
          .size
          .height * 0.018,),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            height: MediaQuery
                .of(_context)
                .size
                .height * 0.06,
            width: MediaQuery
                .of(_context)
                .size
                .height * 0.06,
            padding: EdgeInsets.all(MediaQuery
                .of(_context)
                .size
                .height * 0.01),
            child: Image.asset(imgUrl),
            decoration: BoxDecoration(
              color: Styles.accentColor,
              border: Border.all(color: Styles.primaryColor, width: 1.5),
              shape: BoxShape.circle,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 5.0),
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: Styles.detailsResponseText,
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  describe,
                  style: Styles.subtitleText,
                  textAlign: TextAlign.start,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _generalDetails() {
    Navigator.push(_context,
        MaterialPageRoute(builder: (context) => UserDetailsContainer()));
  }

  _healthDetails() {
    Navigator.push(_context,
        MaterialPageRoute(builder: (context) => HealthDetailsContainer()));
  }

  _contact() {
    Navigator.push(
        _context, MaterialPageRoute(builder: (context) => ContactContainer()));
  }

  _chronicDiseases() {
    Navigator.push(_context,
        MaterialPageRoute(builder: (context) => ChronicDiseasesContainer()));
  }

  _transactions() {
    Navigator.push(_context,
        MaterialPageRoute(builder: (context) => TransactionsContainer()));
  }

  _notifications() {
    Navigator.pushReplacement(
        _context,
        MaterialPageRoute(
            builder: (context) => HomePage(
              currentIndex: HomePageIndex.notifications,
            )));
  }
}
