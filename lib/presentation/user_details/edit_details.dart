import 'package:flutter/material.dart';
import 'package:osler_health/utils/styles.dart';

class EditDetails extends StatelessWidget {
  String head;
  String value;
  double width;
  Function updateValue;

  EditDetails({this.head, this.value, this.width, this.updateValue});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        SizedBox(
          height: 7.5,
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
          width: width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0), color: Colors.white),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 5.0,
              ),
              Text(
                head,
                style: TextStyle(
                  color: Styles.accentColor,
                  fontSize: 14.0,
                  fontWeight: FontWeight.w400,
                  fontFamily: 'Righteous',
                ),
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                /*TextFormField(
                          decoration: InputDecoration(
                              border: InputBorder.none
                          ),
                          autovalidate: true,
                          initialValue: value,
                          style: Styles.detailText,
                          keyboardType: TextInputType.emailAddress,
                          onEditingComplete: () {
                            _validateForm();
                          },
                          onSaved: (value) {
                            _updateEmailValue(value, index);
                          },
                          validator: (value) {
                            if (value.isNotEmpty &&
                                !Validate.isValidEmail(value)) {
                              return "Not a valid e-mail address";
                            }
                          },
                        ),*/
                (value == null) ? value : "N/A",
                style: TextStyle(
                    color: Styles.primaryColor,
                    fontSize: 16.0,
                    letterSpacing: 1.0,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'JosefinSans'),
              ),
              SizedBox(
                height: 5.0,
              ),
            ],
          ),
        ),
        SizedBox(
          height: 7.5,
        ),
      ],
    );
  }
}
