import 'package:flutter/material.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';

class DisplayDetails extends StatelessWidget {
  String head;
  String value;
  double width;
  CrossAxisAlignment align;

  DisplayDetails(
      {this.head,
      this.value,
      this.width,
      this.align = CrossAxisAlignment.start});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 9.0,
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 12.0),
          width: width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0), color: Colors.white),
          child: Column(
            crossAxisAlignment: align,
            children: <Widget>[
              SizedBox(
                height: 7.0,
              ),
              HeaderText(
                data: head,
                  style: Styles.detailsTitleText
              ),
              SizedBox(
                height: 7.0,
              ),
              HeaderText(
                data: (value != null)
                    ? value.length > 0 ? value : "N/A"
                    : "N/A",
                style: Styles.detailsResponseText,
              ),
              SizedBox(
                height: 7.0,
              ),
            ],
          ),
        ),
        SizedBox(
          height: 9.0,
        ),
      ],
    );
  }
}
