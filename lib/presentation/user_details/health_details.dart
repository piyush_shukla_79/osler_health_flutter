import 'package:flutter/material.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/presentation/user_details/display_details.dart';
import 'package:osler_health/utils/styles.dart';

class HealthDetails extends StatelessWidget {
  final User user;

  BuildContext _context;

  HealthDetails({this.user});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 50;

    _context = context;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: Column(
          children: <Widget>[
            CustomAppbar(
              topLeftCTAIcon: Image.asset('assets/back_arrow_icon.png'),
              onTopLeftIconTap: _backAction,
              navTitle: user.getFullName(),
              pageTitle: "Health Details",
              pageImage: Image.asset('assets/health_image.png'),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              isShrinked: false,
            ),
            Expanded(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 15.0),
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      DisplayDetails(
                        head: 'Primary Physician',
                        value: user.doctor.data.getFullName(),
                        width: width,
                      ),
                      DisplayDetails(
                        head: 'Primary Care Group',
                        value: user.practice.data.label,
                        width: width,
                      ),
                      DisplayDetails(
                        head: 'Member Plan Association',
                        value: user.payer.data.label,
                        width: width,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          DisplayDetails(
                            head: 'Height',
                            value: user.height.toString(),
                            width: width / 2 - 15,
                          ),
                          DisplayDetails(
                            head: 'Weight',
                            value: user.weight.toString(),
                            width: width / 2 - 15,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          DisplayDetails(
                            head: 'Blood Group',
                            value: user.bloodGroup,
                            width: width / 2 - 15,
                          ),
                          DisplayDetails(
                            head: 'Blood Pressure',
                            value: user.bloodPressure.toString(),
                            width: width / 2 - 15,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          DisplayDetails(
                            head: 'Breathing',
                            value: user.breathing.toString(),
                            width: width / 2 - 15,
                          ),
                          DisplayDetails(
                            head: 'Pulse',
                            value: user.pulse.toString(),
                            width: width / 2 - 15,
                          ),
                        ],
                      ),
                      DisplayDetails(
                        head: 'Temperature',
                        value: user.temperature.toString(),
                        width: width,
                      ),
                      DisplayDetails(
                        head: 'Impatient Relative Risk',
                        value: user.impatientRelativeRisk.toString(),
                        width: width,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _backAction() {
    Navigator.pop(_context);
  }
}
