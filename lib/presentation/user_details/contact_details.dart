import 'package:flutter/material.dart';
import 'package:osler_health/models/mixins/unsubscribe.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/presentation/user_details/contact_details_form.dart';
import 'package:osler_health/presentation/user_details/display_details.dart';
import 'package:osler_health/services/broadcaster_service.dart';
import 'package:osler_health/services/error_service.dart';
import 'package:osler_health/utils/styles.dart';

class ContactDetails extends StatefulWidget {
  final User user;
  final OnSave onSave;
  final bool isSaving;

  ContactDetails({
    @required this.user,
    @required this.onSave,
    @required this.isSaving,
  });


  final ScrollController controller = new ScrollController();

  @override
  _ContactDetailsState createState() => _ContactDetailsState();
}

class _ContactDetailsState extends State<ContactDetails> with UnsubscribeMixin {
  var isEditing = false;
  var hasErrorOccurred = false;
  BuildContext _context;

  @override
  void initState() {
    super.initState();
    ErrorService.getInstance()
        .on(ErrorCode.updateUser)
        .takeUntil(destroy$)
        .listen((ErrorEvent event) {
      print("**********************");
      print("Error" + event.error.message);
      print("***********************");
      setState(() {
        hasErrorOccurred = true;
      });
      showSnackBar(context, "Please enter valid data");
    });
  }

  @override
  void didUpdateWidget(ContactDetails oldWidget) {
    if (oldWidget.isSaving && !widget.isSaving && !hasErrorOccurred) {
      setState(() {
        isEditing = false;
      });
    }

    super.didUpdateWidget(oldWidget);
  }

  ContactDetailsForm _form;

  @override
  Widget build(BuildContext context) {
    _form = ContactDetailsForm(
        user: widget.user,
        onSave: widget.onSave,
        isSaving: widget.isSaving);
    _context = context;
    double width = MediaQuery
        .of(context)
        .size
        .width - 50;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: GestureDetector(
          onTap: (){
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Column(
            children: <Widget>[
              CustomAppbar(
                isEditing: isEditing,
                topLeftCTAIcon: Image.asset('assets/back_arrow_icon.png'),
                bottomRightCTAIcon: (!isEditing) ? Image.asset(
                    'assets/edit_icon.png') : Icon(
                  Icons.save, color: Colors.white,),
                navTitle: widget.user.getFullName(),
                pageTitle: "Contact Details",
                pageImage: Image.asset('assets/contact_image.png'),
                onTopLeftIconTap: (!isEditing) ? _backAction : _editModeOff,
                onBottomRightIconTap: (!isEditing) ? _editModeOn : onMyTap,
                isSaving: widget.isSaving,
                height: MediaQuery
                    .of(context)
                    .size
                    .height,
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                isShrinked: false,
              ),
              Expanded(
                child: isEditing
                    ? _form
                    : SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  controller: widget.controller,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      DisplayDetails(
                      head: "CELL PHONE",
                      value: widget.user.cellPhone,
                      width: width,
                      ),
                      DisplayDetails(
                      head: "WORK PHONE",
                      value: widget.user.workPhone,
                        width: width,
                      ),
                      DisplayDetails(
                      head: "HOME PHONE",
                      value: widget.user.homePhone,
                      width: width,
                      ),
                      DisplayDetails(
                        head: "PERSONAL EMAIL",
                        value: widget.user.personalEmail,
                        width: width,
                      ),
                      DisplayDetails(
                        head: "WORK EMAIL",
                        value: widget.user.workEmail,
                        width: width,
                      ),
                      DisplayDetails(
                        head: "CITY",
                        value: widget.user.city,
                        width: width,
                      ),
                      DisplayDetails(
                        head: "STATE",
                        value: widget.user.state,
                        width: width,
                      ),
                      DisplayDetails(
                        head: "ZIP CODE",
                        value: widget.user.zipCode,
                        width: width,
                      ),
                      DisplayDetails(
                        head: "ADDRESS LINE 1",
                        value: widget.user.address1,
                        width: width,
                      ),
                      DisplayDetails(
                        head: "ADDRESS LINE 2",
                        value: widget.user.address2,
                        width: width,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _backAction() {
    Navigator.pop(_context);
  }

  _editModeOn() {
    if (!isEditing) {
      setState(() {
        isEditing = true;
      });
    }
  }

  _editModeOff() {
    if (isEditing) {
      setState(() {
        isEditing = false;
      });
    }
  }

  onMyTap() {
    BroadcasterService().broadcast(type: BroadcasterEventType.onSavedTap);
    setState(() {
      hasErrorOccurred = false;
    });
  }

}
