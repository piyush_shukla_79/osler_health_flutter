import 'package:flutter/material.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/utils/string.dart';
import 'package:osler_health/utils/styles.dart';

import 'chronic_disease_body.dart';
import 'display_details.dart';

class ChronicDiseasePage extends StatelessWidget {
  final User user;

  ChronicDiseasePage({this.user});

  ScrollController controller = new ScrollController();

  BuildContext _context;
  double width;
  double height;


  @override
  Widget build(BuildContext context) {

    _context = context;
    width = MediaQuery
        .of(context)
        .size
        .width;
    height = MediaQuery
        .of(context)
        .size
        .height;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: Column(
          children: <Widget>[
            CustomAppbar(
              topLeftCTAIcon: Image.asset('assets/back_arrow_icon.png'),
              navTitle: user.getFullName(),
              pageTitle: CHRONIC_DISEASE,
              pageImage: Image.asset('assets/chronic_diseases_image.png'),
              onTopLeftIconTap: _backAction,
              height: MediaQuery
                  .of(context)
                  .size
                  .height,
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              isShrinked: false,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                DisplayDetails(
                  head: "Count",
                  value: (user.chronicDiseases==null)?"0":user.chronicDiseases.length.toString(),
                  width: width * 0.4,
                  align: CrossAxisAlignment.center,
                ),
              ],
            ),
            Expanded(
              child: (user.chronicDiseases != null) ? ChronicDiseaseBody(
                controller: controller,
                width: width,
                height: height,
                chronicDiseases: user.chronicDiseases.toList(),) : Center(
                child: Text("No Chronic Diseases"),),
            ),
          ],
        ),
      ),
    );
  }

  _backAction() {
    Navigator.pop(_context);
  }
}
