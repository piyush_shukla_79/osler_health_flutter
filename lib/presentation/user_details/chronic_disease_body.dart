import 'package:flutter/material.dart';
import 'package:osler_health/models/chronic_diseases.dart';
import 'package:osler_health/presentation/user_details/chronic_disease_row.dart';
import 'package:osler_health/services/notifcation_listener_service.dart';

class ChronicDiseaseBody extends StatefulWidget {
  List<ChronicDiseases> chronicDiseases;
  double height;
  double width;
  ScrollController controller;

  ChronicDiseaseBody(
      {this.controller, this.chronicDiseases, this.height, this.width});

  @override
  State<StatefulWidget> createState() {
    return _ChronicDiseaseBody();
  }
}

class _ChronicDiseaseBody extends State<ChronicDiseaseBody> {
  OnTap onTap;
  int tapId;

  @override
  Widget build(BuildContext context) {
    return NotificationListener<OnTap>(
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        controller: widget.controller,
        shrinkWrap: true,
        itemCount: widget.chronicDiseases.length,
        itemBuilder: (BuildContext context, int index) {
          ChronicDiseases item = widget.chronicDiseases[index];
          return ChronicDiseaseRow(
            height: widget.height,
            width: widget.width,
            isExpanded:
            (tapId == null) ? false : (tapId == index) ? true : false,
            chronicDisease: item,
            controller: widget.controller,
            index: index,
            contextParent: context,
          );
        },
      ),
      onNotification: (onTap) {
        setState(() {
          tapId = (tapId == onTap.tapId) ? null : onTap.tapId;
        });
      },
    );
  }
}
