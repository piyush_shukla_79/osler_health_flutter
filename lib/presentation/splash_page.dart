import 'package:flutter/material.dart';
import 'package:onesignal/onesignal.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/containers/login_container.dart';
import 'package:osler_health/main.dart';
import 'package:osler_health/presentation/home_page.dart';
import 'package:osler_health/services/preferences_service.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      this._getHomeScreen(context);
    });

    return Container(
      child: Center(child: CircularProgressIndicator()),
    );
  }

  _getHomeScreen(BuildContext context) async {
    final hasToken = await PreferencesService().hasAuthToken();

    if (!hasToken) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => LogInContainer()));
    } else {
      final user = await PreferencesService().getAuthUser();
      OneSignal.shared.sendTag("email", user.getOneSignalEmail());
      OslerHealthApp.store.dispatch(LogInComplete(currentUser: user));
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    }
  }
}
