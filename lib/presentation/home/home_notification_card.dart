import 'package:flutter/material.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';

typedef HomeNotificationCardTapped = void Function(AppNotification notif);

class HomeNotificationCard extends StatelessWidget {
  final double height;
  final double width;
  final AppNotification notification;
  final HomeNotificationCardTapped onNotificationTapped;

  HomeNotificationCard(
      {this.height, this.width, this.notification, this.onNotificationTapped});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(height * 0.1),
            color: Colors.white),
        padding: EdgeInsets.all(height * 0.06),
        child: notification == null
            ? Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                        height: height * 0.3,
                        width: width * 0.3,
                        child: Image.asset(
                          "assets/all_notification.png",
                        )),
                    SizedBox(
                      width: 5.0,
                    ),
                    HeaderText(
                      data: "See all",
                      style:
                          TextStyle(color: Styles.accentColor, fontSize: 20.0),
                    )
                  ],
                ),
              )
            : Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                          height: height * 0.3,
                          width: width * 0.3,
                          child: Image.asset("assets/home_notification.png")),
                      Container(
                        width: width * .5,
                        child: Text(
                          notification.title,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Styles.primaryColor,
                            fontWeight: FontWeight.w300,
                            fontSize: 18.0,
                            fontFamily: 'Righteous',
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
      ),
      onTap: _notificationTapped,
    );
  }

  _notificationTapped() {
    onNotificationTapped(notification);
  }
}
