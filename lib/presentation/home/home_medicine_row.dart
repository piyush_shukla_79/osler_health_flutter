import 'package:flutter/material.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';

import 'home_medicine_card.dart';

class HomeMedicineRow extends StatefulWidget {
  final List<Medicine> medicines;
  final List<Reminder> reminders;

  HomeMedicineRow({this.medicines, this.reminders});

  @override
  State<StatefulWidget> createState() {
    return _HomeMedicineRow();
  }
}

class _HomeMedicineRow extends State<HomeMedicineRow> {
  int index = 0;
  ScrollController controller;

  _HomeMedicineRow();

  @override
  void initState() {
    controller = ScrollController();
    controller.addListener(scrollListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (widget.medicines.length != 0)
        ? Column(
            children: <Widget>[
              Expanded(
                child: Container(),
                flex: 1,
              ),
              Expanded(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 74,
                      child: ListView.builder(
                        controller: controller,
                        physics: BouncingScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: widget.medicines.length > 3
                            ? 3
                            : widget.medicines.length,
                        itemBuilder: (context, index) {
                          final item = widget.medicines[index];
                          final filteredReminders = widget.reminders.where(
                              (r) => r.entityId == item.medicinePatientId);
                          return Row(
                            children: <Widget>[
                              (index == 0)
                                  ? SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          0.25,
                                    )
                                  : SizedBox(
                                      width: 5.0,
                                    ),
                              HomeMedicineCard(
                                  width:
                                      MediaQuery.of(context).size.width * 0.5,
                                  medicine: item,
                                  reminder: filteredReminders.length > 0
                                      ? filteredReminders.first
                                      : null),
                              SizedBox(
                                width: 20.0,
                              ),
                              (index == 2 ||
                                      widget.medicines.length < 3 &&
                                          (index ==
                                              widget.medicines.length - 1))
                                  ? SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                              0.25 -
                                          20,
                                    )
                                  : SizedBox(
                                      width: 5.0,
                                    ),
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                ),
                flex: 20,
              ),
              Expanded(
                child: Container(),
                flex: 1,
              ),
              Expanded(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(),
                      flex: 40,
                    ),
                    Expanded(
                      child: Container(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Expanded(
                              child: (widget.medicines.length >= 1)
                                  ? Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: index == 0
                                              ? Styles.accentColor
                                              : Styles.primaryColor),
                                    )
                                  : Container(),
                              flex: 1,
                            ),
                            Expanded(
                              child: (widget.medicines.length >= 2)
                                  ? Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: index == 1
                                              ? Styles.accentColor
                                              : Styles.primaryColor),
                                    )
                                  : Container(),
                              flex: 1,
                            ),
                            Expanded(
                              child: (widget.medicines.length >= 3)
                                  ? Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: index == 2
                                              ? Styles.accentColor
                                              : Styles.primaryColor),
                                    )
                                  : Container(),
                              flex: 1,
                            ),
                          ],
                        ),
                      ),
                      flex: 10,
                    ),
                    Expanded(
                      child: Container(),
                      flex: 40,
                    ),
                  ],
                ),
                flex: 1,
              ),
            ],
          )
        : Container(
            width: MediaQuery.of(context).size.width * 0.5,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12.0),
                color: Styles.accentColor),
            child: Align(
              alignment: Alignment.center,
              child: HeaderText(
                data: "No Medicine",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                    fontSize: 20.0),
              ),
            ),
          );
  }

  scrollListener() {
    if (controller.offset >= 0 && controller.offset <= 130)
      setState(() {
        index = 0;
      });
    else if (controller.offset >= 130 && controller.offset <= 300)
      setState(() {
        index = 1;
      });
    else if (controller.offset >= 300)
      setState(() {
        index = 2;
      });
  }
}
