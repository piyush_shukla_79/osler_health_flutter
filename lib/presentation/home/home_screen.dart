import 'package:flutter/material.dart';
import 'package:osler_health/actions/notification_action.dart';
import 'package:osler_health/main.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/notifications/notification_form_page.dart';
import 'package:osler_health/presentation/notifications/notification_web_view.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';
import 'package:url_launcher/url_launcher.dart';

import '../home_page.dart';
import 'home_medicine_row.dart';
import 'home_notification_card.dart';

class HomeScreen extends StatelessWidget {
  final User user;
  final List<Medicine> medicines;
  final List<Reminder> reminders;
  final List<AppNotification> notifications;
  final Function toggleSidebar;
  BuildContext _context;

  int index = 0;

  final ScrollController controller = new ScrollController();
  String url;

  _launchURL(String phone) async {
    url = 'tel:' + phone;
    if (await canLaunch(url)) {
      launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  HomeScreen(
      {this.user,
      this.medicines,
      this.reminders,
      this.notifications,
      this.toggleSidebar});

  @override
  Widget build(BuildContext context) {
    _context = context;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: (user != null)
            ? Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Expanded(
                  child: Container(),
                  flex: 4,
                ),
                Expanded(
                  flex: 11,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            'assets/home_page.png',
                          ),
                          fit: BoxFit.fill),
                    ),
                  ),
                ),
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              color: Styles.primaryColor,
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(20),
                                bottomLeft: Radius.circular(20),
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Align(
                                    alignment: Alignment.centerLeft,
                                    child: IconButton(
                                      icon: Image.asset(
                                          'assets/sidebar_icon.png'),
                                      onPressed: toggleSidebar,
                                      iconSize: MediaQuery.of(context)
                                          .size
                                          .height *
                                          0.03,
                                    )),
                                InkWell(
                                  child: Column(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        width: MediaQuery.of(context)
                                            .size
                                            .width *
                                            0.5,
                                        child: HeaderText(
                                          data: user.getFullName(),
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 24,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 2,
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            height: 25,
                                            width: 25,
                                            child: Image.asset(
                                              "assets/star.png",
                                              color: Styles.accentColor,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          (user.points == null)
                                              ? HeaderText(
                                            data: " 0",
                                            style: TextStyle(
                                              color: Styles
                                                  .accentColor,
                                              fontSize: 20,
                                            ),
                                          )
                                              : HeaderText(
                                            data: " " +
                                                user.points
                                                    .toString(),
                                            style: TextStyle(
                                              color: Styles
                                                  .accentColor,
                                              fontSize: 20,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  onTap: _navigateToProfile,
                                )
                              ],
                            ),
                          ),
                          flex: 75,
                        ),
                        Expanded(
                          flex: 25,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                                child: IconButton(
                                  icon: Image.asset(
                                    'assets/notifications_icon.png',
                                    color: Styles.primaryColor,
                                  ),
                                  onPressed: _navigateToNotificationPage,
                                  iconSize:
                                  MediaQuery.of(context).size.height *
                                      0.05,
                                )),
                          ),
                        )
                      ],
                    ),
                  ),
                  flex: 14,
                ),
                Expanded(
                  child: Container(),
                  flex: 3,
                ),
                Expanded(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width*0.02,
                      ),
                      (user.practice != null &&
                          user.practice.data.phone != null)
                          ? InkWell(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment:
                          MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 52,
                              child: CircleAvatar(
                                radius: MediaQuery.of(context)
                                    .size
                                    .height *
                                    0.06,
                                backgroundColor:
                                Styles.iconBackColor,
                                child: Container(
                                  height: MediaQuery.of(context)
                                      .size
                                      .height *
                                      0.038,
                                  width: MediaQuery.of(context)
                                      .size
                                      .height *
                                      0.03,
                                  child: Image.asset(
                                    'assets/call.png',
                                    fit: BoxFit.contain,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 48,
                              child: Container(
                                width: MediaQuery.of(context)
                                    .size
                                    .width *
                                    0.40,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    HeaderText(
                                      data: "Call your physician!",
                                      style: TextStyle(
                                          fontWeight:
                                          FontWeight.w800,
                                          color:
                                          Styles.primaryColor),
                                      overflow:
                                      TextOverflow.ellipsis,
                                      maxLines: 1,
                                    ),
                                    HeaderText(
                                      data: user.practice.data
                                          .label ??
                                          " ",
                                      style: TextStyle(
                                          fontWeight:
                                          FontWeight.w200,
                                          color:
                                          Styles.primaryColor),
                                      overflow:
                                      TextOverflow.ellipsis,
                                      maxLines: 1,
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        onTap: () =>
                            _launchURL(user.practice.data.phone),
                      )
                          : Container(),
                      InkWell(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 52,
                              child: CircleAvatar(
                                radius:
                                MediaQuery.of(context).size.height *
                                    0.06,
                                backgroundColor: Styles.iconBackColor,
                                child: Container(
                                  height:
                                  MediaQuery.of(context).size.height *
                                      0.038,
                                  width:
                                  MediaQuery.of(context).size.height *
                                      0.03,
                                  child: Image.asset(
                                    'assets/clock.png',
                                    fit: BoxFit.contain,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 48,
                              child: Container(
                                width: MediaQuery.of(context).size.width *
                                    0.40,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    HeaderText(
                                      data: "Medicine Time!",
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w800,
                                          color: Styles.primaryColor),
                                    ),
                                    HeaderText(
                                      data: "View your reminders",
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w200,
                                          color: Styles.primaryColor),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        onTap: _navigateToMedicines,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width*0.1,
                      ),
                    ],
                  ),
                  flex: 13,
                ),
                Expanded(
                  child: Container(),
                  flex: 2,
                ),
                Expanded(
                  child: Container(
                    child: HomeMedicineRow(
                      reminders: reminders,
                      medicines: medicines,
                    ),
                  ),
                  flex: 20,
                ),
                Expanded(
                  child: Container(),
                  flex: 3,
                ),
                Expanded(
                  child: Container(
                    child: ListView.builder(
                      controller: controller,
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemCount: notifications.length > 2
                          ? 3
                          : notifications.length + 1,
                      itemBuilder: (context, index) {
                        return Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                (index == 0)
                                    ? SizedBox(
                                  width: MediaQuery.of(context)
                                      .size
                                      .width *
                                      0.20,
                                )
                                    : SizedBox(
                                  width: MediaQuery.of(context)
                                      .size
                                      .width *
                                      .01,
                                ),
                                _getNotificationCard(index, context),
                                SizedBox(
                                  width:
                                  MediaQuery.of(context).size.width *
                                      0.05,
                                ),
                                (index == 2) ||
                                    (notifications.length < 2 &&
                                        index == notifications.length)
                                    ? SizedBox(
                                  width: MediaQuery.of(context)
                                      .size
                                      .width *
                                      0.25 -
                                      20,
                                )
                                    : SizedBox(
                                  width: MediaQuery.of(context)
                                      .size
                                      .width *
                                      .01,
                                ),
                              ],
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  flex: 22,
                ),
                Expanded(
                  child: Container(),
                  flex: 4,
                ),
              ],
            ),
          ],
        )
            : Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  _getNotificationCard(index, BuildContext context) {
    if (index < notifications.length && index < 2) {
      final item = notifications[index];

      return HomeNotificationCard(
        width: MediaQuery.of(context).size.width * 0.6,
        height: MediaQuery.of(context).size.height * .2475,
        notification: item,
        onNotificationTapped: _notifications,
      );
    }

    return HomeNotificationCard(
      width: MediaQuery.of(context).size.width * 0.6,
      height: MediaQuery.of(context).size.height * .2475,
      onNotificationTapped: _notifications,
    );
  }

  _notifications(AppNotification notif) {
    if (notif == null) {
      _navigateToNotificationPage();
    } else {
      if (notif.type == "form") {
        Navigator.push(
          _context,
          MaterialPageRoute(
            builder: (context) => NotificationFormPage(
                  notification: notif,
                  notificationRead: () {
                    OslerHealthApp.store
                        .dispatch(UpdateNotification(notification: notif));
                  },
                ),
          ),
        );
      } else {
        OslerHealthApp.store.dispatch(UpdateNotification(notification: notif));
        Navigator.push(
          _context,
          MaterialPageRoute(
            builder: (context) => NotificationWebView(
                  notification: notif,
                ),
          ),
        );
      }
    }
  }

  _navigateToMedicines() {
    Navigator.pushReplacement(
      _context,
      MaterialPageRoute(
        builder: (context) => HomePage(
              currentIndex: HomePageIndex.medicines,
            ),
      ),
    );
  }

  _navigateToNotificationPage() {
    Navigator.pushReplacement(
      _context,
      MaterialPageRoute(
        builder: (context) => HomePage(
              currentIndex: HomePageIndex.notifications,
            ),
      ),
    );
  }

  _navigateToProfile() {
    Navigator.pushReplacement(
      _context,
      MaterialPageRoute(
        builder: (context) => HomePage(
              currentIndex: HomePageIndex.profile,
            ),
      ),
    );
  }
}
