import 'package:flutter/material.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/presentation/stepper_medicine.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';

class HomeMedicineCard extends StatelessWidget {
  final Medicine medicine;
  final Reminder reminder;
  final double width;
  final double height;

  HomeMedicineCard(
      {@required this.medicine, this.reminder, this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0), color: Styles.accentColor),
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          HeaderText(
            data: medicine.name,
            style:
            TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
          HeaderText(
            data: "Dose: " + medicine.dosage.toString(),
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontFamily: 'JosefinSans'),
          ),
          StepperMedicine(
            times: (medicine.frequency == "daily")
                ? medicine.occurrence
                : 1,
            reminder: reminder,
            color: Colors.white,
            frequency: medicine.frequency,
          ),
        ],
      ),
    );
  }
}
