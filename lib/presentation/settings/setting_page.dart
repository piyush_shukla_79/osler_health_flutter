import 'package:flutter/material.dart';
import 'package:osler_health/app_config.dart';
import 'package:osler_health/models/setting.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/presentation/settings/policy_web_view.dart';
import 'package:osler_health/utils/string.dart';
import 'package:osler_health/utils/styles.dart';

import '../home_page.dart';

class SettingPage extends StatefulWidget {
  final Setting setting;
  final Function settingUpdate;
  final Function toggleSidebar;

  SettingPage({this.setting, this.settingUpdate, this.toggleSidebar});

  @override
  State<StatefulWidget> createState() {
    return _SettingPage(setting);
  }
}

class _SettingPage extends State<SettingPage> {
  Setting setting;

  _SettingPage(this.setting);

  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Column(
      children: <Widget>[
        CustomAppbar(
          height: MediaQuery
              .of(context)
              .size
              .height,
          width: MediaQuery
              .of(context)
              .size
              .width,
          isShrinked: false,
          topLeftCTAIcon: Image.asset('assets/sidebar_icon.png'),
          onTopLeftIconTap: widget.toggleSidebar,
          topRightCTAIcon: Image.asset('assets/notifications_icon.png'),
          onTopRightIconTap: _notifications,
          navTitle: SETTINGS_TITLE,
          pageImage: Image.asset('assets/setting_image.png'),
          pageTitle: "",
        ),
        Expanded(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Subscribe",
                    style: TextStyle(
                        color: Styles.iconBackColor,
                        fontSize: 25,
                        fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 50.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.white),
                    child: SwitchListTile(
                      activeColor: Styles.accentColor,
                      value: setting.email,
                      onChanged: (bool value) {
                        final newSettings =
                        setting.rebuild((b) => b..email = value);
                        setState(() {
                          setting = newSettings;
                        });
                        settingChanges(newSettings);
                      },
                      title: Text(
                        'Email',
                        style: Styles.detailsResponseText,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 50.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.white),
                    child: SwitchListTile(
                      activeColor: Styles.accentColor,
                      value: setting.notification,
                      onChanged: (bool value) {
                        final newSettings =
                        setting.rebuild((b) => b..notification = value);
                        setState(() {
                          setting = newSettings;
                        });
                        settingChanges(newSettings);
                      },
                      title: Text(
                        'Notifications',
                        style: Styles.detailsResponseText,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 50.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.white),
                    child: SwitchListTile(
                      activeColor: Styles.accentColor,
                      value: setting.messages,
                      onChanged: (bool value) {
                        final newSettings =
                        setting.rebuild((b) => b..messages = value);
                        setState(() {
                          setting = newSettings;
                        });
                        settingChanges(newSettings);
                      },
                      title: Text(
                        'Messages',
                        style: Styles.detailsResponseText,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Legal",
                    style: TextStyle(
                        color: Styles.iconBackColor,
                        fontSize: 25,
                        fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  InkWell(
                    onTap: _tcTapped,
                    child: Container(
                      height: 50.0,
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Colors.white),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Terms and Conditions',
                        style: Styles.detailsResponseText,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  InkWell(
                    onTap: _privacyPolicyTapped,
                    child: Container(
                      height: 50.0,
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Colors.white),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Privacy Policy',
                        style: Styles.detailsResponseText,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  settingChanges(Setting settings) {
    widget.settingUpdate(settings);
  }

  _tcTapped() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (_context) =>
                PolicyWebView(AppConfig.termsAndConditionUrl)));
  }

  _privacyPolicyTapped() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (_context) => PolicyWebView(AppConfig.privacyPolicyUrl)));
  }

  _notifications() {
    Navigator.pushReplacement(
        _context,
        MaterialPageRoute(
            builder: (context) =>
                HomePage(
                  currentIndex: HomePageIndex.notifications,
                )));
  }

  _navigateToHomePage(){
    Navigator.popUntil(context, (route)=> route.isFirst);
  }
}
