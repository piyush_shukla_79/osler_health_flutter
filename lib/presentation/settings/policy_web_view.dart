import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/utils/styles.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PolicyWebView extends StatelessWidget {
  final String url;

  PolicyWebView(this.url);
  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context=context;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: Column(
          children: <Widget>[
            CustomAppbar(
              navTitle: "",
              topLeftCTAIcon: Image.asset('assets/back_arrow_icon.png'),
              pageImage: Image.asset('assets/setting_image.png'),
              pageTitle: "",
              onTopLeftIconTap: _backAction,
              height: MediaQuery
                  .of(context)
                  .size
                  .height,
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              isShrinked: true,
              backGroundColor: Colors.white,
            ),
            Expanded(
              child: WebView(
                initialUrl: url,
              ),
            ),
          ],
        ),
      ),
    );
  }
  _backAction() {
    Navigator.pop(_context);
  }
}
