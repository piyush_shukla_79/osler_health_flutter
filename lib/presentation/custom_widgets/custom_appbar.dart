import 'package:flutter/material.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';

typedef CTACallback = void Function();

class CustomAppbar extends StatelessWidget {
  final Image topLeftCTAIcon;
  final Image topRightCTAIcon;
  final  bottomLeftCTAIcon;
  final  bottomRightCTAIcon;

  final pageImage;

  final String pageTitle;
  final String navTitle;

  final CTACallback onTopLeftIconTap;
  final CTACallback onTopRightIconTap;
  final CTACallback onBottomLeftIconTap;
  final CTACallback onBottomRightIconTap;

  final double height;
  final double width;

  final Color backGroundColor;

  bool isSaving;
  bool isEditing;
  bool isShrinked;

  CustomAppbar({
    @required this.navTitle,
    @required this.pageTitle,
    @required this.height,
    @required this.topLeftCTAIcon,
    @required this.pageImage,
    @required this.onTopLeftIconTap,
    this.width,
    this.isShrinked,
    this.bottomLeftCTAIcon,
    this.bottomRightCTAIcon,
    this.topRightCTAIcon,
    this.onBottomLeftIconTap,
    this.onBottomRightIconTap,
    this.onTopRightIconTap,
    this.isSaving,
    this.isEditing,
    this.backGroundColor,
  })
      : assert((topRightCTAIcon == null && onTopRightIconTap == null) ||
      (topRightCTAIcon != null && onTopRightIconTap != null)),
        assert((bottomLeftCTAIcon == null && onBottomLeftIconTap == null) ||
            (bottomLeftCTAIcon != null && onBottomLeftIconTap != null)),
        assert((bottomRightCTAIcon == null && onBottomRightIconTap == null) ||
            (bottomRightCTAIcon != null && onBottomRightIconTap != null));

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backGroundColor ?? Styles.appColor,
      height: (isShrinked) ? height * 0.274 : height * 0.35,
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20)),
              color: Styles.accentColor,
            ),
            height: (isShrinked) ? height * 0.129 : height * 0.215,
          ),
          Positioned(
            top: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: topLeftCTAIcon,
                    onPressed: onTopLeftIconTap,
                    iconSize: height * 0.03,
                  ),
                  Container(
                    alignment: Alignment.center,
                    width: width * 0.6,
                    child: HeaderText(
                      data: navTitle,
                      style: TextStyle(color: Colors.white, fontSize: 24.0),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  (topRightCTAIcon != null)
                      ? IconButton(
                    icon: topRightCTAIcon,
                    onPressed: onTopRightIconTap,
                    iconSize: height * 0.03,

                  )
                      : Container(
                    width: height * 0.056,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: height * 0.10,
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  (bottomLeftCTAIcon != null)
                      ? InkWell(
                    onTap: onBottomLeftIconTap,
                    child: Container(
                        height: height * .09,
                        width: height * 0.09,
                        padding: EdgeInsets.all(height * 0.023),
                        decoration: BoxDecoration(
                            color: Styles.iconBackColor,
                            shape: BoxShape.circle),
                        child: isSaving
                            ? CircularProgressIndicator()
                            : bottomLeftCTAIcon
                    ),
                  )

                      : Container(
                    width: height * 0.09,
                    height: height * 0.09,
                  ),
                  (bottomRightCTAIcon != null)
                      ? GestureDetector(
                      child: Container(width: height * 0.09,
                          height: height * 0.09,
                          padding: EdgeInsets.all(height * 0.023),
                          decoration: BoxDecoration(
                              color: !isEditing ? Styles.primaryColor : Styles
                                  .iconBackColor,
                              shape: BoxShape.circle),
                          child: isSaving
                              ? CircularProgressIndicator()
                              : bottomRightCTAIcon
                      ),
                      onTap: onBottomRightIconTap)
                      : Container(
                    width: height * 0.09,
                    height: height * 0.09,
                  )
                ],
              ),
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: height * 0.005,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(height * 0.04),
                  height: height * 0.17,
                  width: width * 0.40,
                  child: pageImage,
                  decoration: BoxDecoration(
                      color: Styles.accentColor,
                      border: Border.all(color: Colors.white, width: 5.0),
                      shape: BoxShape.circle),
                ),
                SizedBox(
                  height: height * 0.013,
                ),
                Container(
                  width: width*0.65,
                  child: HeaderText(
                    data: pageTitle,
                    style: TextStyle(
                        color: Styles.primaryColor,
                        fontSize: 22.0,
                        fontFamily: 'Righteous',
                        fontWeight: FontWeight.w500),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
