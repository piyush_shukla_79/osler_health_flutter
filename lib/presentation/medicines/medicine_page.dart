import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:osler_health/containers/add_medicine_container.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/presentation/medicines/medicine_row.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';

import '../home_page.dart';

class MedicinePage extends StatefulWidget {
  final List<Medicine> medicines;
  final List<Reminder> reminders;
  final bool isLoading;
  final Function toggleSidebar;
  final User user;

  MedicinePage(
      {this.medicines,
      this.isLoading,
      @required this.reminders,
      this.toggleSidebar,
      this.user});

  @override
  State<StatefulWidget> createState() {
    return MedicinePageState(
      toggleSidebar: toggleSidebar,
    );
  }
}

class MedicinePageState extends State<MedicinePage>
    with TickerProviderStateMixin {
  final Function toggleSidebar;

  MedicinePageState({this.toggleSidebar}) {}

  BuildContext _context;
  double width;
  double height;
  bool isExpanded = true;
  bool isFirst = true;

  ScrollController controller = new ScrollController();

  AnimationController _animationController;
  AnimationController _fadeAnimationController;

  @override
  void initState() {
    super.initState();

    controller.addListener(() {
      if (controller.positions.first.userScrollDirection ==
          ScrollDirection.forward) {
        if (!isExpanded &&
            controller.offset <= controller.position.minScrollExtent) {
          setState(() {
            isExpanded = true;
          });
        }
      } else {
        if (isExpanded) {
          setState(() {
            isExpanded = false;
          });
        }
      }
    });
    _animationController = new AnimationController(
        lowerBound: 0.325, vsync: this, duration: Duration(milliseconds: 400));
    _fadeAnimationController = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 100));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!isFirst && isExpanded) {
      controller.animateTo(controller.positions.first.minScrollExtent + 1,
          duration: Duration(microseconds: 10), curve: ElasticInCurve());
    }
    if (isExpanded) {
      _animationController.forward();
      _fadeAnimationController.forward();
    } else {
      if (isFirst) {
        isFirst = false;
      }
      _animationController.reverse();
      _fadeAnimationController.reverse();
    }
    _context = context;

    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).push(
              CupertinoPageRoute(
                fullscreenDialog: true,
                builder: (context) => Scaffold(
                  body: AddMedicineContainer(),
                ),
              ),
            );
          },
        ),
        body: GestureDetector(
          onVerticalDragUpdate: (dragUp) {
            if (dragUp.delta.dy < 0) {
              controller.animateTo(
                  controller.positions.first.minScrollExtent + 5,
                  duration: Duration(microseconds: 10),
                  curve: ElasticInCurve());
              setState(() {
                isExpanded = false;
              });
            }
            if (dragUp.delta.dy >= 0 &&
                controller.offset <= controller.position.minScrollExtent) {
              setState(() {
                isExpanded = true;
              });
            }
          },
          child: Stack(
            children: <Widget>[
              AnimatedBuilder(
                animation: _animationController,
                builder: (BuildContext context, Widget child) {
                  return Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20),
                        ),
                        color: Styles.accentColor),
                    width: width,
                    height: isExpanded
                        ? height * 0.37 * _animationController.value
                        : height * 0.32 * _animationController.value,
                  );
                },
              ),
              Column(
                children: <Widget>[
                  AnimatedBuilder(
                    animation: _animationController,
                    builder: (BuildContext context, Widget child) {
                      return Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        width: width,
                        height: isExpanded
                            ? height * 0.32 * _animationController.value
                            : height * 0.32 * _animationController.value,
                      );
                    },
                  ),
                  (!widget.isLoading)
                      ? (widget.medicines.length > 0)
                      ? Expanded(
                    child: getBodyChild(),
                  )
                      : Column(children: <Widget>[
                    SizedBox(
                      height: height * 0.1,
                    ),
                    Text("No Medicines", style: Styles.titleText)
                  ])
                      : Column(children: <Widget>[
                    SizedBox(
                      height: height * 0.1,
                    ),
                    Container(child: CircularProgressIndicator())
                  ]),
                ],
              ),
              _expandedContainer(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _expandedContainer() {
    return Stack(
      children: <Widget>[
        Positioned(
          top: 0,
          child: Container(
            width: width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FadeTransition(
                  opacity: CurvedAnimation(
                      parent: _fadeAnimationController, curve: Curves.easeIn),
                  child: IconButton(
                    icon: Image.asset('assets/sidebar_icon.png'),
                    onPressed: toggleSidebar,
                    iconSize: height * 0.03,
                  ),
                ),
                IconButton(
                  icon: Image.asset('assets/notifications_icon.png'),
                  onPressed: _notifications,
                  iconSize: MediaQuery.of(context).size.height * 0.04,
                ),
              ],
            ),
          ),
        ),
        AnimatedPositioned(
          duration: Duration(milliseconds: 400),
          top: isExpanded ? height * 0.07 : height * 0.01,
          left: isExpanded ? 0.0 : width * 0.01,
          right: isExpanded ? 0.0 : width * 0.8,
          child: GestureDetector(
            onTap: _navigateToProfile,
            child: Hero(
              tag: 'profileImage',
              child: Center(
                child: AnimatedContainer(
                  height: isExpanded ? height * 0.17 : height * .08,
                  width: isExpanded ? height * 0.17 : height * .08,
                  duration: Duration(milliseconds: 400),
                  child: CircleAvatar(
                    radius: 50,
                    backgroundImage:
                    CachedNetworkImageProvider(widget.user.getAvatarUrl()),
                  ),
                ),
              ),
            ),
          ),
        ),
        AnimatedPositioned(
          child: Container(
            width: width * 0.25,
            child: HeaderText(
              data: widget.user.getFullName(),
              style: TextStyle(fontSize: 16.0, color: Colors.white),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              textAlign: TextAlign.center,
            ),
          ),
          duration: Duration(
            milliseconds: 400,
          ),
          top: isExpanded ? height * 0.25 : height * 0.04,
          left: isExpanded ? width * 0.25 : width * 0.2,
          right: isExpanded ? width * 0.25 : width * 0.4,
        ),
        Positioned(
          top: height * 0.27,
          left: width * 0.1,
          right: width * 0.1,
          child: AnimatedOpacity(
            opacity: isExpanded ? 1 : 0.0,
            duration: Duration(milliseconds: isExpanded ? 1200 : 200),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: height * 0.05,
                  width: width * 0.05,
                  child: Image.asset(
                    'assets/star.png',
                    color: Styles.primaryColor,
                  ),
                ),
                Container(
                    child: HeaderText(
                  data: widget.user.points?.toString() ?? "0",
                  style: TextStyle(color: Styles.primaryColor, fontSize: 13),
                ))
              ],
            ),
          ),
        ),
      ],
    );
  }

  getBodyChild() {
    return ListView.builder(
      physics: _scrollPhysics(),
      controller: controller,
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: widget.medicines.length,
      itemBuilder: (context, index) {
        final item = widget.medicines[index];
        final filteredReminders =
            widget.reminders.where((r) => r.entityId == item.medicinePatientId);

        return Column(
          children: <Widget>[
            MedicineRow(
                medicine: item,
                reminder: filteredReminders.length > 0
                    ? filteredReminders.first
                    : null),
            SizedBox(
              height: 20.0,
            ),
          ],
        );
      },
    );
  }

  _notifications() {
    Navigator.pushReplacement(
      _context,
      MaterialPageRoute(
        builder: (context) => HomePage(
              currentIndex: HomePageIndex.notifications,
            ),
      ),
    );
  }

  ScrollPhysics _scrollPhysics() {
    if (isExpanded == true) {
      return NeverScrollableScrollPhysics();
    }
  }

  _navigateToProfile() {
    Navigator.pushReplacement(
      _context,
      MaterialPageRoute(
        builder: (context) => HomePage(
              currentIndex: HomePageIndex.profile,
            ),
      ),
    );
  }
}
