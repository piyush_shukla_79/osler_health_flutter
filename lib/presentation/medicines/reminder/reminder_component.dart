import 'package:flutter/material.dart';
import 'package:osler_health/models/reminder_notification.dart';
import 'package:osler_health/utils/styles.dart';

class ReminderComponent extends StatelessWidget {
  final ReminderNotification notification;
  final Function notificationUpdated;
  Widget day;
  final int index;

  ReminderComponent(
      {this.notification, this.notificationUpdated, this.day, this.index});

  double width;
  double height;



  @override
  Widget build(BuildContext context) {
    width = MediaQuery
        .of(context)
        .size
        .width;
    height = MediaQuery
        .of(context)
        .size
        .height;

    TimeOfDay tm =
    TimeOfDay(hour: notification.hour, minute: notification.minute);

    return Column(
      children: <Widget>[
        Container(child: (day != null) ? day : SizedBox()),
        Container(
          margin: EdgeInsets.symmetric(
              vertical: 5.0, horizontal: 10.0),
          padding: EdgeInsets.symmetric(
              vertical: 3.0, horizontal: 2.0),
          decoration: BoxDecoration(
              color: notification.enabled ?
              Colors.white : Styles.opacityColor,
              borderRadius: BorderRadius.circular(10.0)

          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: width * 0.5,
                    child: InkWell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          CircleAvatar(
                            radius: MediaQuery
                                .of(context)
                                .size
                                .height * 0.04,
                            backgroundColor: Styles.primaryColor,
                            child: Container(
                              height: MediaQuery
                                  .of(context)
                                  .size
                                  .height * 0.035,
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .height * 0.035,
                              child: Image.asset(
                                'assets/clock.png',
                                fit: BoxFit.contain,),
                            ),
                          ),
                          Text(
                            tm.format(context),
                            style: Styles.detailsResponseText,
                          )
                        ],
                      ),
                      onTap: () {
                        _editTime(time: notification, context: context);
                      },
                    ),
                  ),
                  Container(
                    width: width * 0.3,
                    child: SwitchListTile(
                      value: notification.enabled,
                      onChanged: (value) {
                        enableDisable(value);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  _editTime(
      {@required ReminderNotification time, @required BuildContext context}) {
    TimeOfDay initialTime = new TimeOfDay(hour: time.hour, minute: time.minute);
    showTimePicker(context: context, initialTime: initialTime).then(
      (TimeOfDay time) {
        if (index == null) {
          notificationUpdated(notification.rebuild((b) =>
          b
            ..hour = time.hour
            ..minute = time.minute));
        }
        else {
          notificationUpdated(notification.rebuild((b) =>
          b
            ..hour = time.hour
            ..minute = time.minute), index);
        }
      },
    ).catchError(
      (error) => print(error),
    );
  }

  enableDisable(bool value) {
    if (index == null) {
      notificationUpdated(notification.rebuild((b) => b..enabled = value));
    }
    else {
      notificationUpdated(notification.rebuild((b) =>
      b
        ..enabled = value), index);
    }
  }

}
