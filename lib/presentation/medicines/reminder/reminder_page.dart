import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:osler_health/models/broadcaster_event.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/mixins/unsubscribe.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/reminder_notification.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/presentation/medicines/reminder/reminder_component.dart';
import 'package:osler_health/services/broadcaster_service.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';

class ReminderPage extends StatefulWidget {
  final Reminder reminder;
  final Medicine medicine;
  final Function reminderUpdated;
  bool isSaving;

  ReminderPage(
      {this.reminder, this.medicine, this.reminderUpdated, this.isSaving})
      : super(key: Key(medicine.id.toString())) {
  }

  @override
  State<StatefulWidget> createState() {
    return _ReminderPage(reminder);
  }
}

class _ReminderPage extends State<ReminderPage> with UnsubscribeMixin {
  Reminder reminder;
  BuildContext _context;
  var event;
  int reminderIndex;

  _ReminderPage(this.reminder);

  @override
  void dispose() {
    event?.cancel();
    super.dispose();
  }

  @override
  initState() {
    super.initState();

    if (this.reminder == null) {
      this.reminder = _getDefaultReminder();
    }

    event = BroadcasterService()
        .on(BroadcasterEventType.reminderSaved)
        .takeUntil(destroy$)
        .listen((BroadcasterEvent event) {
      Navigator.pop(_context);
      setState(() {
        reminder = reminder.rebuild((b) => b..id = event.data["id"]);
      });
    });
  }

  ScrollController controller = new ScrollController();

  @override
  Widget build(BuildContext context) {
    reminderIndex = -1;
    _context = context;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: Column(
          children: <Widget>[
            CustomAppbar(
              topLeftCTAIcon: Image.asset('assets/back_arrow_icon.png'),
              onTopLeftIconTap: _backAction,
              bottomRightCTAIcon: Icon(
                Icons.save,
                color: Colors.white,
              ),
              isEditing: true,
              onBottomRightIconTap: _onSave,
              navTitle: "",
              pageImage: Image.asset('assets/reminder_image.png'),
              pageTitle: reminder.title,
              height: MediaQuery
                  .of(context)
                  .size
                  .height,
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              isShrinked: false,
              isSaving: widget.isSaving,
            ),
            Text(
              "Dose: " + widget.medicine.dosage.toString(),
              style: Styles.detailsResponseText,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20.0,
            ),
            Expanded(
              child: SingleChildScrollView(
                controller: controller,
                child: Column(
                  children: <Widget>[
                    (widget.medicine.frequency == "daily") ?
                    ListView.builder(
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      controller: controller,
                      itemCount: reminder == null ? 0 : reminder.notifications
                          .length,
                      itemBuilder: (BuildContext context, int index) {
                        ReminderNotification item =
                        reminder?.notifications[index];
                        return ReminderComponent(
                          notification: item,
                          notificationUpdated:
                              (ReminderNotification notification) {
                            var notifs = reminder.notifications.toList();
                            notifs[index] = notification;
                            final updatedReminder = reminder.rebuild((b) =>
                            b
                              ..notifications =
                              BuiltList<ReminderNotification>.from(notifs)
                                  .toBuilder());
                            setState(() {
                              reminder = updatedReminder;
                            });
                          },
                        );
                      },
                    ) : ListView.builder(
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      controller: controller,
                      itemCount: widget.medicine.value.length,
                      itemBuilder: (BuildContext context, int index) {
                        Widget text;
                        if (widget.medicine.value[index]) {
                          reminderIndex++;
                          if (widget.medicine.frequency == "weekly") {
                            text = HeaderText(
                              textAlign: TextAlign.center,
                              data: _getDayOfWeek(index),
                              style: TextStyle(color: Styles.iconBackColor),);
                          } else {
                            text = HeaderText(
                              textAlign: TextAlign.center,
                              data: (index + 1).toString() + " of month",
                              style: TextStyle(color: Styles.iconBackColor),);
                          }
                          ReminderNotification item =
                          reminder?.notifications[reminderIndex];
                          return ReminderComponent(
                            index: reminderIndex,
                            day: text,
                            notification: item,
                            notificationUpdated:
                                (ReminderNotification notification,int remIndex) {
                              var notifs = reminder.notifications.toList();
                              notifs[remIndex] = notification;
                              final updatedReminder = reminder.rebuild((b) =>
                              b
                                ..notifications =
                                BuiltList<ReminderNotification>.from(notifs)
                                    .toBuilder());
                              setState(() {
                                reminder = updatedReminder;
                              });
                            },
                          );
                        }
                        else {
                          return SizedBox();
                        }
                      },
                    )
                    ,
                    SizedBox(
                      height: 20.0,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Reminder _getDefaultReminder() {
    List<ReminderNotification> notifications = [];

    if (widget.medicine.frequency == "daily") {
      for (var j = 0; j < widget.medicine.occurrence; j++) {
        notifications.add(ReminderNotification((b) => b
          ..enabled = false
          ..hour = (6 + (j * _getOccurrenceFactor(widget.medicine.occurrence)))
              .toInt()
          ..minute = 0));
      }
    }

    if (widget.medicine.frequency == "weekly") {
      widget.medicine.value.asMap().forEach((int key, bool value) {
        if (value) {
          notifications.add(ReminderNotification((b) => b
            ..enabled = false
            ..hour = 6
            ..minute = 0
            ..date = key));
        }
      });
    }

    if (widget.medicine.frequency == "monthly") {
      widget.medicine.value.asMap().forEach((int key, bool value) {
        if (value) {
          notifications.add(ReminderNotification((b) => b
            ..enabled = false
            ..hour = 6
            ..minute = 0
            ..date = key));
        }
      });
    }

    return Reminder((b) => b
      ..title = widget.medicine.name
      ..entityId = widget.medicine.medicinePatientId
      ..entityType = "medication"
      ..endDate = widget.medicine.endDate
      ..notifications =
          BuiltList<ReminderNotification>.from(notifications).toBuilder());
  }

  _getOccurrenceFactor(int occurrences) {
    return 16 / occurrences;
  }

  @override
  void onDispose() {
    onDispose();
    super.onDispose();
  }

  _backAction() {
    Navigator.pop(_context);
  }

  _onSave() {
    widget.reminderUpdated(reminder);

//    if (!widget.isSaving)
//      _backAction();
  }


  _getDayOfWeek(int index) {
    switch (index) {
      case 0:
        return "Monday";
        break;
      case 1:
        return "Tuesday";
        break;
      case 2:
        return "Wednesday";
        break;
      case 3:
        return "Thursday";
        break;
      case 4:
        return "Friday";
        break;
      case 5:
        return "Saturday";
        break;
      case 6:
        return "Sunday";
        break;
    }
  }
}
