import 'package:flutter/material.dart';
import 'package:osler_health/containers/tabs/reminder_container.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/presentation/stepper_medicine.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';

class MedicineRow extends StatelessWidget {
  final Medicine medicine;
  final Reminder reminder;

  MedicineRow({@required this.medicine, this.reminder});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _reminderContainer(context);
      },
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: Colors.white),
        padding: EdgeInsets.all(15.0),
        margin: EdgeInsets.symmetric(horizontal: 27.0),
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: HeaderText(data: medicine.name,
                style: Styles.titleText,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,),
            ),
            SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "Dose: " + medicine.dosage.toString(),
                    style: Styles.detailsResponseText,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 21.0,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: StepperMedicine(
                times: (medicine.frequency == "daily")
                    ? medicine.occurrence
                    : 1,
                reminder: reminder,
                color: Styles.accentColor,
                frequency: medicine.frequency,
              ),
            ),
            SizedBox(
              height: 10.0,
            )
          ],
        ),
      ),
    );
  }

  _reminderContainer(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                ReminderContainer(medicationId: medicine.medicinePatientId)));
  }
}
