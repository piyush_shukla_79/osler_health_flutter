import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:osler_health/models/mixins/unsubscribe.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/services/broadcaster_service.dart';
import 'package:osler_health/services/error_service.dart';
import 'package:osler_health/utils/helpers.dart';
import 'package:osler_health/utils/styles.dart';

class MedicineForm extends StatefulWidget {
  Function onSave;

  MedicineForm({this.onSave});

  @override
  _MedicineFormState createState() => _MedicineFormState();
}

class _MedicineFormState extends State<MedicineForm> with UnsubscribeMixin {
  Map<String, dynamic> medicine;
  bool isSaving = false;
  List<bool> weekValues = [];
  List<bool> monthValues = [];

  final _formKey = GlobalKey<FormState>();

  final ScrollController _scrollController = ScrollController();

//  final _occurrenceController = TextEditingController(text: "1");

  @override
  void initState() {
    super.initState();

    medicine = {"interval": 1, "frequency": "daily", "occurrence": "1"};

    _resetWeekValues();
    _resetMonthValues();

//    _occurrenceController.addListener(() {
//      _updateMedicine("occurrence", _occurrenceController.text);
//      if (_occurrenceController.text != medicine["occurrence"]) {
//        _resetMonthValues();
//        _resetWeekValues();
//      }
//    });

    _scrollController.addListener(() {
      if (_scrollController.positions.first.userScrollDirection ==
          ScrollDirection.forward ||
          _scrollController.positions.first.userScrollDirection ==
              ScrollDirection.reverse) {
        FocusScope.of(context).requestFocus(FocusNode());
      }
    });
    BroadcasterService()
        .on(BroadcasterEventType.medicineSaved)
        .takeUntil(destroy$)
        .listen((data) {
      Navigator.of(context).pop();
    });

    ErrorService.getInstance()
        .on(ErrorCode.saveMedicine)
        .takeUntil(destroy$)
        .listen((data) {
      setState(() {
        isSaving = false;
      });

      showSnackBar(context, data.error.message);
    });
  }

  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Column(
            children: <Widget>[
              CustomAppbar(
                topLeftCTAIcon: Image.asset('assets/back_arrow_icon.png'),
                onTopLeftIconTap: _backAction,
                bottomRightCTAIcon: Icon(
                  Icons.save,
                  color: Colors.white,
                ),
                onBottomRightIconTap: _onSave,
                isEditing: true,
                navTitle: "Add Medicine",
                pageTitle: "",
                pageImage: Image.asset('assets/reminder_image.png'),
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                isShrinked: false,
                isSaving: isSaving,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  controller: _scrollController,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 27.0),
                    padding: EdgeInsets.all(10.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 15.0),
                            padding: EdgeInsets.symmetric(
                                vertical: 6.0, horizontal: 10.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 4.0,
                                ),
                                Text(
                                  'Name:',
                                  style: Styles.detailsTitleText,
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                TextFormField(
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Enter Name",
                                  ),
                                  style: Styles.detailsResponseText,
                                  onFieldSubmitted: _removeFocus,
                                  onSaved: (String value) {
                                    _updateMedicine("name", value);
                                  },
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return "Required";
                                    }
                                  },
                                  onEditingComplete: () {
                                    _validateForm();
                                  },
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 15.0),
                            padding: EdgeInsets.symmetric(
                                vertical: 6.0, horizontal: 10.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 4.0,
                                ),
                                Text(
                                  'Company Name:',
                                  style: Styles.detailsTitleText,
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                TextFormField(
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: "Enter company Name"),
                                  style: Styles.detailsResponseText,
                                  onFieldSubmitted: _removeFocus,
                                  onSaved: (String value) {
                                    _updateMedicine("company_name", value);
                                  },
                                  onEditingComplete: () {
                                    _validateForm();
                                  },
                                ),
                                SizedBox(
                                  height: 4.0,
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 15.0),
                            padding: EdgeInsets.symmetric(
                                vertical: 6.0, horizontal: 10.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 4.0,
                                ),
                                Text(
                                  'Dosage:',
                                  style: Styles.detailsTitleText,
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                TextFormField(
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Enter dosage",
                                  ),
                                  onFieldSubmitted: _removeFocus,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return "Required";
                                    }
                                  },
                                  onSaved: (String value) {
                                    _updateMedicine("dosage", value);
                                  },
                                  style: Styles.detailsResponseText,
                                  onEditingComplete: () {
                                    _validateForm();
                                  },
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 15.0),
                            padding: EdgeInsets.symmetric(
                                vertical: 6.0, horizontal: 10.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 4.0,
                                ),
                                Text(
                                  'Frequency:',
                                  style: Styles.detailsTitleText,
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                DropdownButtonFormField<String>(
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: "Select Frequency",
                                      focusedBorder: OutlineInputBorder()),
                                  value: medicine["frequency"],
                                  onChanged: (String value) {
                                    _updateMedicine("frequency", value);
                                  },
                                  validator: (String value) {
                                    if (value == null || value.isEmpty) {
                                      return "Required";
                                    }
                                  },
                                  items: [
                                    DropdownMenuItem<String>(
                                        child: Text("Daily"), value: "daily"),
                                    DropdownMenuItem<String>(
                                        child: Text("Weekly"), value: "weekly"),
                                    DropdownMenuItem<String>(
                                        child: Text("Monthly"),
                                        value: "monthly"),
                                  ],
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                _getFrequencySelector(),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                width:(medicine["frequency"] == "daily")? MediaQuery
                                    .of(context)
                                    .size
                                    .width * 0.36:MediaQuery.of(context).size.width-84,
                                padding: EdgeInsets.symmetric(
                                    vertical: 6.0, horizontal: 10.0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                    BorderRadius.circular(10.0)),
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 4.0,
                                    ),
                                    Text(
                                      'Interval:',
                                      style: Styles.detailsTitleText,
                                    ),
                                    SizedBox(
                                      height: 4.0,
                                    ),
                                    TextFormField(
                                      initialValue:
                                      medicine["interval"].toString(),
                                      decoration: InputDecoration(
                                          border: InputBorder.none),
                                      keyboardType: TextInputType.number,
                                      style: Styles.detailsResponseText,
                                      validator: (String value) {
                                        if (value.isEmpty) {
                                          return "Required";
                                        }
                                      },
                                      onFieldSubmitted: _removeFocus,
                                      onSaved: (String value) {
                                        _updateMedicine("interval", value);
                                      },
                                      onEditingComplete: () {
                                        _validateForm();
                                      },
                                    ),
                                    SizedBox(
                                      height: 4.0,
                                    ),
                                  ],
                                ),
                              ),
                              (medicine["frequency"] == "daily")
                                  ? Container(
                                  width:
                                  MediaQuery
                                      .of(context)
                                      .size
                                      .width *
                                      0.36,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 6.0, horizontal: 10.0),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                      BorderRadius.circular(10.0)),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 4.0,
                                      ),
                                      Text(
                                        'Occurrences:',
                                        style: Styles.detailsTitleText,
                                      ),
                                      SizedBox(
                                        height: 4.0,
                                      ),
                                      DropdownButtonFormField<String>(
                                        decoration: InputDecoration(
                                            border: InputBorder.none,
                                            focusedBorder: OutlineInputBorder()),
                                        value: medicine["occurrence"],
                                        onChanged: (value) {
                                          _updateMedicine(
                                              "occurrence", value);
                                        },
                                        validator: (value) {
                                          if (value == null) {
                                            return "Required";
                                          }
                                        },
                                        items: [
                                          DropdownMenuItem<String>(
                                              child: Text("1"),
                                              value: "1"),
                                          DropdownMenuItem<String>(
                                              child: Text("2"),
                                              value: "2"),
                                          DropdownMenuItem<String>(
                                              child: Text("3"),
                                              value: "3"),
                                          DropdownMenuItem<String>(
                                              child: Text("4"),
                                              value: "4"),
                                          DropdownMenuItem<String>(
                                              child: Text("5"),
                                              value: "5"),
                                          DropdownMenuItem<String>(
                                              child: Text("6"),
                                              value: "6"),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 4.0,
                                      ),
                                    ],
                                  ))
                                  : SizedBox()
                            ],
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  _selectStartDate();
                                },
                                child: Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width *
                                      0.36,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 6.0, horizontal: 10.0),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                      BorderRadius.circular(10.0)),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 4.0,
                                      ),
                                      Text(
                                        'Start Date:',
                                        style: Styles.detailsTitleText,
                                      ),
                                      SizedBox(
                                        height: 4.0,
                                      ),
                                      IgnorePointer(
                                        child: TextFormField(
                                          validator: (String value) {
                                            if (value.isEmpty) {
                                              return "Required";
                                            }
                                          },
                                          decoration: InputDecoration(
                                              border: InputBorder.none),
                                          controller: TextEditingController(
                                              text: medicine["start_date"] ??
                                                  ""),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 4.0,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  if (medicine["start_date"] != null) {
                                    _selectEndDate();
                                  }
                                },
                                child: Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width *
                                      0.36,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 6.0, horizontal: 10.0),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                      BorderRadius.circular(10.0)),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 4.0,
                                      ),
                                      Text(
                                        'End Date:',
                                        style: Styles.detailsTitleText,
                                      ),
                                      SizedBox(
                                        height: 4.0,
                                      ),
                                      IgnorePointer(
                                        child: TextFormField(
                                          enabled:
                                          medicine["start_date"] != null,
                                          validator: (String value) {
                                            if (value.isEmpty) {
                                              return "Required";
                                            }
                                          },
                                          decoration: InputDecoration(
                                              border: InputBorder.none),
                                          controller: TextEditingController(
                                              text:
                                              medicine["end_date"] ?? ""),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 4.0,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _updateMedicine(String key, String value) {
    if (value != null && value.isNotEmpty) {
      setState(() {
        medicine = medicine..addAll({key: value});
      });
    }
  }

  _selectStartDate() async {
    final picked = await _getPickedDate();
    if (picked != null) {
      setState(() {
        medicine.addAll({"start_date": yyyyMdddd(picked)});
      });
    }
  }

  _selectEndDate() async {
    final picked =
        await _getPickedDate(firstDate: DateTime.parse(medicine["start_date"]));
    if (picked != null) {
      setState(() {
        medicine.addAll({"end_date": yyyyMdddd(picked)});
      });
    }
  }

  Future<DateTime> _getPickedDate(
      {DateTime initialDate, DateTime firstDate, DateTime lastDate}) async {
    final now = DateTime.now();
    return await showDatePicker(
      context: context,
      initialDate: initialDate ?? firstDate ?? now,
      firstDate: firstDate ?? initialDate ?? now,
      lastDate: lastDate ?? DateTime(DateTime.now().year + 100),
    );
  }

  _toggleWeekValues(int index) {
    setState(() {
      weekValues[index] = !weekValues[index];
    });
  }

  _toggleMonthValue(int index) {
    setState(() {
      monthValues[index] = !monthValues[index];
    });
  }

  _getFrequencySelector() {
    switch (medicine["frequency"]) {
      case "weekly":
        _resetMonthValues();
        return _getWeekSelector();
      case "monthly":
        _resetWeekValues();
        return _getMonthSelector();
      default:
        return SizedBox();
    }
  }

  _getMonthSelector() {
    return Container(
      height: 350.0,
      child: GridView.count(
        crossAxisCount: 5,
        controller: _scrollController,
        shrinkWrap: true,
        children: List.generate(30, (index) {
          return Center(
            child: InkWell(
              onTap: () {
                _toggleMonthValue(index);
              },
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  "${index + 1}",
                  style: TextStyle(
                      color: monthValues[index]
                          ? Colors.white
                          : Styles.primaryColor),
                ),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: monthValues[index]
                        ? Styles.primaryColor
                        : Colors.transparent),
              ),
            ),
          );
        }),
      ),
    );
  }

  _getWeekSelector() {
    return Container(
      height: 100.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Column(children: <Widget>[
            Checkbox(
                value: weekValues[0],
                onChanged: (bool value) {
                  _toggleWeekValues(0);
                }),
            Text("Mon")
          ]),
          Column(
            children: <Widget>[
              Checkbox(
                  value: weekValues[1],
                  onChanged: (bool value) {
                    _toggleWeekValues(1);
                  }),
              Text("Tue")
            ],
          ),
          Column(
            children: <Widget>[
              Checkbox(
                  value: weekValues[2],
                  onChanged: (bool value) {
                    _toggleWeekValues(2);
                  }),
              Text("Wed")
            ],
          ),
          Column(
            children: <Widget>[
              Checkbox(
                  value: weekValues[3],
                  onChanged: (bool value) {
                    _toggleWeekValues(3);
                  }),
              Text("Thu")
            ],
          ),
          Column(
            children: <Widget>[
              Checkbox(
                  value: weekValues[4],
                  onChanged: (bool value) {
                    _toggleWeekValues(4);
                  }),
              Text("Fri")
            ],
          ),
          Column(
            children: <Widget>[
              Checkbox(
                  value: weekValues[5],
                  onChanged: (bool value) {
                    _toggleWeekValues(5);
                  }),
              Text("Sat")
            ],
          ),
          Column(
            children: <Widget>[
              Checkbox(
                  value: weekValues[6],
                  onChanged: (bool value) {
                    _toggleWeekValues(6);
                  }),
              Text("Sun")
            ],
          ),
        ],
      ),
    );
  }

  bool _validateForm() {
    return _formKey.currentState?.validate();
  }

  _resetMonthValues() {
    for (var i = 0; i < 30; i++) {
      monthValues.add(false);
    }
  }

  _resetWeekValues() {
    weekValues = [false, false, false, false, false, false, false];
  }

  _backAction() {
    Navigator.pop(_context);
  }

  _onSave() {
    if (_formKey.currentState.validate()) {
      try {
        _formKey.currentState.save();
        final frequency = medicine["frequency"];
        if (frequency != "daily") {
          medicine.removeWhere((key, value) => key == "occurrence");
          if (frequency == "weekly") {
            medicine["value"] = weekValues;
          } else {
            medicine["value"] = monthValues;
          }
        } else {
          medicine.removeWhere((key, value) => key == "value");
        }
      } catch (e) {
        print(e);
      }
      widget.onSave(medicine);
      setState(() {
        isSaving = true;
      });
      // If the form is valid, display a snackbar. In the real world, you'd
      // often want to call a server or save the information in a database
    }
  }
  void _removeFocus(value){
    FocusScope.of(context).requestFocus(FocusNode());
  }
}
