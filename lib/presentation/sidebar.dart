import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:osler_health/actions/actions.dart';
import 'package:osler_health/containers/login_container.dart';
import 'package:osler_health/main.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/utils/header_text.dart';
import 'package:osler_health/utils/styles.dart';
import 'package:redux/redux.dart';

import 'home_page.dart';

typedef SidebarOptionTapped = void Function(HomePageIndex index);

class Sidebar extends StatelessWidget {
  final HomePageIndex currentIndex;
  final SidebarOptionTapped onTabTapped;
  final String userName;
  final String imageURL;

  BuildContext _context;

  Sidebar({this.onTabTapped, this.imageURL, this.userName, this.currentIndex});

  double height, width;

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    _context = context;
    return StoreConnector<AppState, _ViewModel>(
      converter: _ViewModel.fromStore,
      builder: (BuildContext _context, _ViewModel model) => Container(
            height: height,
            width: width * 0.83,
            color: Colors.white,
        child: (model.user != null) ? SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 20.0),
                margin: EdgeInsets.all(20.0),
                child: Image.asset('assets/app_logo.png'),
              ),
              Padding(
                padding:
                EdgeInsets.symmetric(horizontal: width * 0.14),
                child: InkWell(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: Styles.accentColor,
                          radius: 40.0,
                          backgroundImage: CachedNetworkImageProvider(
                              model.user.getAvatarUrl()),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: width * 0.65,
                          child: HeaderText(
                            data: model.user.getFullName(),
                            style: TextStyle(
                                fontSize: 22,
                                color: Styles.accentColor),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ],
                    ),
                    onTap: () {
                      onTabTapped(HomePageIndex.profile);
                    }),
              ),
              SizedBox(
                height: 10.0,
              ),
              FlatButton(
                  child: _sidebarRow('assets/home.png', 'Home',HomePageIndex.home),
                  onPressed: () {
                    onTabTapped(HomePageIndex.home);
                  }),
              FlatButton(
                  child: _sidebarRow('assets/profile.png', 'Profile',HomePageIndex.profile),
                  onPressed: () {
                    onTabTapped(HomePageIndex.profile);
                  }),
              FlatButton(
                child:
                _sidebarRow('assets/medicines.png', 'Medicines',HomePageIndex.medicines),
                onPressed: () {
                  onTabTapped(HomePageIndex.medicines);
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                height: 2.0,
                color: Styles.accentColor,
              ),
              SizedBox(
                height: 10.0,
              ),
              FlatButton(
                  child: _sidebarRow(
                      'assets/notifications.png', 'Notifications',HomePageIndex.notifications),
                  onPressed: () {
                    onTabTapped(HomePageIndex.notifications);
                  }),
              SizedBox(
                height: 10.0,
              ),
              FlatButton(
                child: _sidebarRow('assets/settings.png', 'Settings',HomePageIndex.settings),
                onPressed: () {
                  onTabTapped(HomePageIndex.settings);
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                height: 2.0,
                color: Styles.accentColor,
              ),
              SizedBox(
                height: 10.0,
              ),
              FlatButton(
                child: _sidebarRow('assets/logout.png', 'Logout',5),
                onPressed: _logoutTapped,
              ),
              SizedBox(
                height: 10.0,
              )
            ],
          ),
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          ),
    );
  }

  Widget _sidebarRow(sidebarImage, tab,index) {
    return Container(
      color: index == currentIndex ? Styles.opacityColor : null,
      height: height * 0.0625,
      padding: EdgeInsets.all(2.5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 30,
            width: 30,
            alignment: Alignment.center,
            child: Image.asset(sidebarImage),
          ),
          SizedBox(
            width: 10.0,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: HeaderText(
              data: tab,
              style: Styles.pageTitleText,
              textAlign: TextAlign.start,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
          )
        ],
      ),
    );
  }

  _logoutTapped() async {
    OslerHealthApp.store.dispatch(Logout());
    Navigator.pushReplacement(
        _context, MaterialPageRoute(builder: (c) => LogInContainer()));
  }
}

class _ViewModel {
  User user;

  _ViewModel(this.user);

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(store.state.user.profile);
  }
}
