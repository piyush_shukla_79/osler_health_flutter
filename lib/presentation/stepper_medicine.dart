import 'package:flutter/material.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/utils/styles.dart';

class StepperMedicine extends StatelessWidget {
  final int times;
  final Reminder reminder;
  final Color color;
  final String frequency;

  StepperMedicine(
      {this.times, @required this.reminder, this.color, this.frequency});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 15.0,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Container(
              height: 2.0,
              color: color,
            ),
          ),
          Align(
            child: getDoses(),
          )
        ],
      ),
    );
  }

  Widget _medicineDose(bool enabled) {
    return Container(
      height: 15.0,
      width: 15.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: color, width: 2.0),
          color: enabled
              ? color == Colors.white ? Colors.white : Styles.accentColor
              : color == Colors.white ? Styles.accentColor : Colors.white),
    );
  }

  Widget getDoses() {
    if (reminder == null) {
      if (times != null) {
        List<Widget> list = new List<Widget>();
        for (var i = 0; i < times; i++) {
          list.add(_medicineDose(false));
        }
        return Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: list);
      }

      return null;
    }
    if (frequency != "daily") {
      List<Widget> list = new List<Widget>();
      for (var i = 0; i < times; i++) {
        list.add(_medicineDose(false));
      }
      return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: list);
    }

    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: reminder.notifications
            .map<Widget>((n) => _medicineDose(n.enabled))
            .toList());
  }
}
