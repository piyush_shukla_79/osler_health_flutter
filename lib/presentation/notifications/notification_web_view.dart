import 'package:flutter/material.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/utils/styles.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NotificationWebView extends StatelessWidget {
  final AppNotification notification;

  NotificationWebView({@required this.notification});

  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: Column(
          children: <Widget>[
            CustomAppbar(
              navTitle: "",
              topLeftCTAIcon: Image.asset('assets/back_arrow_icon.png'),
              pageImage: Image.asset('assets/notification_image.png'),
              pageTitle: "",
              onTopLeftIconTap: _backAction,
              height: MediaQuery
                  .of(context)
                  .size
                  .height,
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              isShrinked: true,
              backGroundColor: Colors.white,
            ),
            Expanded(
              child: WebView(
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (WebViewController controller) {
                  controller.evaluateJavascript("""
                           document.write(`
                           <html>
                            <head>
                              <meta name="viewport" content="width=device-width, initial-scale=1">
                            </head>
                            <body>
                              ${notification.body}
                            </body>
                           </html>
                           `);
                          """).then((value) {
                    print("js result");
                    print(value);
                  }).catchError((err) {
                    print("js error");
                    debugPrint(err.toString());
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  _backAction() {
    Navigator.pop(_context);
  }
}
