import 'package:flutter/material.dart';
import 'package:osler_health/models/mixins/unsubscribe.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/presentation/notifications/notification_form_page.dart';
import 'package:osler_health/presentation/notifications/notification_row.dart';
import 'package:osler_health/presentation/notifications/notification_web_view.dart';
import 'package:osler_health/services/broadcaster_service.dart';
import 'package:osler_health/utils/string.dart';
import 'package:osler_health/utils/styles.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class NotificationsPage extends StatefulWidget {
  final List<AppNotification> notifications;
  final bool isLoading;
  final Function notificationTapped;
  final Function onRefresh;
  final Function toggleSidebar;

  NotificationsPage(
      {@required this.notifications,
      @required this.isLoading,
      @required this.notificationTapped,
      @required this.onRefresh,
      @required this.toggleSidebar});

  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage>
    with UnsubscribeMixin {
  final RefreshController _refreshController = RefreshController();

  @override
  initState() {
    super.initState();

    BroadcasterService()
        .on(BroadcasterEventType.notificationsLoaded)
        .takeUntil(destroy$)
        .listen((data) {
      _refreshController.refreshCompleted();
    });
  }

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CustomAppbar(
          navTitle: NOTIFICATIONS_TITLE,
          topLeftCTAIcon: Image.asset('assets/sidebar_icon.png'),
          pageImage: Image.asset('assets/notification_image.png'),
          pageTitle: "",
          onTopLeftIconTap: widget.toggleSidebar,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          isShrinked: false,
        ),
        Container(
          height: 0.4,
          color: Styles.primaryColor,
          width: MediaQuery.of(context).size.width * 0.90,
        ),
        Expanded(
          child: widget.notifications.length > 0
              ? SmartRefresher(
                  controller: _refreshController,
                  onRefresh: _refreshStarted,
                  child: ListView.builder(
                    itemCount: widget.notifications.length,
                    itemBuilder: (context, index) {
                      final item = widget.notifications[index];

                      return Column(
                        children: <Widget>[
                          NotificationRow(item, rowTapped),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.90,

                            height: 0.5,
                            color: Styles.primaryColor,
                          )
                        ],
                      );
                    },
                  ),
                )
              : Text(
                  "No Notifications",
                  style: Styles.titleText,
                ),
        )
      ],
    );
  }

  rowTapped(AppNotification item, BuildContext context) {
    if (item.type != "form") {
      if (!item.read) {
        widget.notificationTapped(item.rebuild((b) => b..read = true));
      }

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => NotificationWebView(notification: item)));
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_context) => NotificationFormPage(
                notification: item,
                notificationRead: () {
                  showSnackBar(context, "Form submitted succesfully");
                  Navigator.pop(context);
                  widget
                      .notificationTapped(item.rebuild((b) => b..read = true));
                },
              ),
        ),
      );
    }
  }

  _refreshStarted() {
    widget.onRefresh();
  }

  void onTopRightIconTab() {}

  @override
  void dispose() {
    onDispose();
    _refreshController.dispose();
    super.dispose();
  }
}
