import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/utils/styles.dart';

class NotificationRow extends StatelessWidget {
  final AppNotification notification;
  final Function onTap;

  NotificationRow(this.notification, this.onTap);

  double width;
  double height;
  double textWidth;

  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    return InkWell(
        onTap: () => onTap(notification, context), child: _notificationRow());
  }

  Widget _notificationRow() {
    height = MediaQuery.of(_context).size.height;
    width = MediaQuery.of(_context).size.width;
    textWidth = width * 0.58;

    return Container(
      height: 80,
      width: MediaQuery.of(_context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 7.0, vertical: 7.5),
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 10.0,
            alignment: Alignment.center,
            child: !notification.read ? CircleAvatar(
              backgroundColor: Styles.accentColor,
              radius: 5.0,
            ) : Container(),
          ),
          Container(
            width: textWidth,
            child: Container(
              child: Text(
                notification.title,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: !notification.read ? Styles.accentColor : Styles
                        .primaryColor),
              ),
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      notification.getYMMDCreatedAt(),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      notification.getHmCreatedAt(),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

// DateFormat.yMMMMd("en_US")
