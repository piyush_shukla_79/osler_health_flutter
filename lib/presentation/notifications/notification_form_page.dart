import 'package:flutter/material.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/presentation/custom_widgets/custom_appbar.dart';
import 'package:osler_health/utils/styles.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NotificationFormPage extends StatelessWidget {
  final AppNotification notification;
  final Function notificationRead;

  NotificationFormPage(
      {@required this.notification, @required this.notificationRead});

  BuildContext _context;
  @override
  Widget build(BuildContext context) {
    _context = context;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Styles.appColor,
        body: Column(
          children: <Widget>[CustomAppbar(
            navTitle: "",
            topLeftCTAIcon: Image.asset('assets/back_arrow_icon.png'),
            pageImage: Image.asset('assets/notification_image.png'),
            pageTitle: "Please fill the form",
            onTopLeftIconTap: _backAction,
            height: MediaQuery
                .of(context)
                .size
                .height,
            width: MediaQuery
                .of(context)
                .size
                .width,
            isShrinked: false,
            backGroundColor: Colors.white,
          ),
            Expanded(
              child: WebView(
                initialUrl: notification.body,
                javascriptMode: JavascriptMode.unrestricted,
                javascriptChannels: Set.from(
                  [
                    JavascriptChannel(
                      name: "appClient",
                      onMessageReceived: (JavascriptMessage message) {
                        notificationRead();
                      },
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _backAction() {
    Navigator.pop(_context);
  }
}

