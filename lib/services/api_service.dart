import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:osler_health/actions/actions.dart';
import 'package:osler_health/app_config.dart';
import 'package:osler_health/main.dart';
import 'package:osler_health/models/responses/error.dart';
import 'package:osler_health/serializers/serializers.dart';
import 'package:osler_health/services/preferences_service.dart';

abstract class APIService {
  Future<Map<String, dynamic>> get(String url,
      {Map<String, String> params, bool useAuthHeaders = true}) async {
    return this._getResponse(await http.get(
        this._getUrlWithParams(url, params: params),
        headers: (await this._getHeaders(useAuthHeaders: useAuthHeaders))));
  }

  Future<Map<String, dynamic>> post(String url,
      {@required body, bool useAuthHeaders = true}) async {
    final response = await http.post(this._getUrl(url),
        headers: (await this._getHeaders(useAuthHeaders: useAuthHeaders)),
        body: json.encode(body));

    return this._getResponse(response);
  }

  Future<Map<String, dynamic>> patch(String url,
      {@required body, bool useAuthHeaders = true}) async {
    final response = await http.patch(this._getUrl(url),
        headers: (await this._getHeaders(useAuthHeaders: useAuthHeaders)),
        body: json.encode(body));

    return this._getResponse(response);
  }

  // Helper Methods for API Services
  String _getUrl(String url) {
    return AppConfig.baseUrl + url;
  }

  Future<Map<String, String>> _getHeaders({useAuthHeaders = true}) async {
    final map = Map<String, String>.from(
        {"client-key": "phone_app", "Content-Type": "application/json"});

    if (useAuthHeaders) {
      map["Authorization"] =
          "bearer ${await PreferencesService().getAuthToken()}";
    }

    return map;
  }

  String _getUrlWithParams(url, {Map<String, String> params}) {
    var absUrl = this._getUrl(url);

    if (params != null) {
      var paramsString = "";
      params.forEach((key, value) {
        paramsString += "&$key=$value";
      });

      return absUrl + "?" + paramsString.substring(1);
    }

    return absUrl;
  }

  Map<String, dynamic> _getResponse(http.Response response) {
    final body = json.decode(response.body);
    if (response.statusCode == 401) {
      print("*****Logout Called******");
      OslerHealthApp.store.dispatch(Logout());
    }

    if (response.statusCode < 200 || response.statusCode >= 300) {
      throw serializers.deserializeWith(NetworkError.serializer, body);
    }

    return body;
  }
}
