import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:osler_health/middleware/reminder_middleware.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/reminder_notification.dart';
import 'package:osler_health/serializers/serializers.dart';
import 'package:osler_health/serializers/types.dart';
import 'package:osler_health/services/alarm_service.dart';
import 'package:osler_health/services/api_service.dart';
import 'package:osler_health/utils/helpers.dart';

class ReminderService extends APIService {
  ReminderService._();

  static final ReminderService _instance = ReminderService._();

  factory ReminderService.getInstance() => _instance;

  Future<List<Reminder>> listReminders() async {
    final response = await this.get("/patient/me/reminders");

    return (serializers.deserialize(response["data"],
            specifiedType: reminderList) as BuiltList<Reminder>)
        .toList();
  }

  Future<Reminder> create(Reminder reminder) async {
    final response =
        await this.post("/patient/me/reminders", body: reminder.toMap());

    return serializers.deserializeWith(Reminder.serializer, response["data"]);
  }

  Future<Reminder> update(Reminder reminder) async {
    final response = await this
        .patch("/patient/me/reminders/${reminder.id}", body: reminder.toMap());

    return serializers.deserializeWith(Reminder.serializer, response["data"]);
  }

  setReminder(Reminder reminder, Medicine med) {
    if (med.frequency == "daily") {
      if (med.hasEndDate()) {
        final startDate = DateTime.parse(med.startDate);
        final endDate = DateTime.parse(med.endDate);

        var date = startDate;

        while (date.compareTo(endDate) <= 0) {
          reminder.notifications
              .where((r) => r.enabled)
              .forEach((ReminderNotification reminderNotif) {
            final notificationInfo = NotificationInfo(
                id: randomNumber(),
                title: med.name,
                dateTime: date.add(Duration(
                    hours: reminderNotif.hour, minutes: reminderNotif.minute)));

            AlarmService.getInstance().schedule(notificationInfo);
          });

          date = date.add(Duration(days: 1));
        }
      } else {
        final filteredReminders =
            reminder.notifications.where((r) => r.enabled).toList();

        for (var i = 0; i < filteredReminders.length; i++) {
          ReminderNotification reminderNotif = filteredReminders[i];

          final notificationInfo = NotificationInfo(
            id: randomNumber(),
            title: med.name,
            hours: reminderNotif.hour,
            minutes: reminderNotif.minute,
          );

          AlarmService.getInstance().showDaily(notificationInfo);
        }
      }
    }

    if (med.frequency == "weekly") {
      if (med.hasEndDate()) {
        final startDate = DateTime.parse(med.startDate);
        final endDate = DateTime.parse(med.endDate);

        var date = startDate;

        while (date.compareTo(endDate) <= 0) {
          reminder.notifications
              .where((r) => r.enabled)
              .forEach((ReminderNotification reminderNotif) {
            if (med.value[date.weekday - 1]) {
              final notificationInfo = NotificationInfo(
                  id: randomNumber(),
                  title: med.name,
                  dateTime: date.add(Duration(
                      hours: reminderNotif.hour,
                      minutes: reminderNotif.minute)));

              AlarmService.getInstance().schedule(notificationInfo);
            }
          });

          date = date.add(Duration(days: 1));
        }
      } else {
        final filteredReminders =
            reminder.notifications.where((r) => r.enabled).toList();

        for (var i = 0; i < filteredReminders.length; i++) {
          ReminderNotification reminderNotif = filteredReminders[i];

          final notificationInfo = NotificationInfo(
            id: randomNumber(),
            title: med.name,
            hours: reminderNotif.hour,
            minutes: reminderNotif.minute,
          );

          med.value.asMap().forEach((int index, bool value) {
            if (value) {
              AlarmService.getInstance()
                  .showWeekly(notificationInfo, dartToNotif[index + 1]);
            }
          });
        }
      }
    }

    if (med.frequency == "weekly") {
      if (med.hasEndDate()) {
        final startDate = DateTime.parse(med.startDate);
        final endDate = DateTime.parse(med.endDate);

        var date = startDate;

        while (date.compareTo(endDate) <= 0) {
          reminder.notifications
              .where((r) => r.date == date.weekday - 1 && r.enabled)
              .forEach((ReminderNotification reminderNotif) {
            final notificationInfo = NotificationInfo(
                id: randomNumber(),
                title: med.name,
                dateTime: date.add(Duration(
                    hours: reminderNotif.hour, minutes: reminderNotif.minute)));

            AlarmService.getInstance().schedule(notificationInfo);
          });

          date = date.add(Duration(days: 1));
        }
      } else {
        final filteredReminders =
            reminder.notifications.where((r) => r.enabled).toList();

        for (var i = 0; i < filteredReminders.length; i++) {
          ReminderNotification reminderNotif = filteredReminders[i];

          final notificationInfo = NotificationInfo(
            id: randomNumber(),
            title: med.name,
            hours: reminderNotif.hour,
            minutes: reminderNotif.minute,
          );

          AlarmService.getInstance().showWeekly(
              notificationInfo, dartToNotif[reminderNotif.date + 1]);
        }
      }
    }

    if (med.frequency == "monthly") {
      if (med.hasEndDate()) {
        final startDate = DateTime.parse(med.startDate);
        final endDate = DateTime.parse(med.endDate);

        var date = startDate;

        while (date.compareTo(endDate) <= 0) {
          reminder.notifications
              .where((r) => r.date == date.day - 1 && r.enabled)
              .forEach((ReminderNotification reminderNotif) {
            final notificationInfo = NotificationInfo(
                id: randomNumber(),
                title: med.name,
                dateTime: date.add(Duration(
                    hours: reminderNotif.hour, minutes: reminderNotif.minute)));

            AlarmService.getInstance().schedule(notificationInfo);
          });

          date = date.add(Duration(days: 1));
        }
      } else {
        // If end date in monthly freq is not set then we set the reminder for 6 months
        for (var j = 0; j < 181; j++) {
          final filteredReminders =
              reminder.notifications.where((r) => r.enabled).toList();

          for (var i = 0; i < filteredReminders.length; i++) {
            ReminderNotification reminderNotif = filteredReminders[i];
            var startDate = DateTime.parse(med.startDate);

            final notificationInfo = NotificationInfo(
                id: randomNumber(),
                title: med.name,
                dateTime: startDate.add(Duration(
                    days: i,
                    hours: reminderNotif.hour,
                    minutes: reminderNotif.minute)));

            AlarmService.getInstance().schedule(notificationInfo);
          }
        }
      }
    }
  }
}
