import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:meta/meta.dart';

//import 'dart:typed_data';
//import 'package:flutter/material.dart';
class AlarmService {
  AlarmService._() {
    this._initState();
  }

  static final AlarmService _instance = AlarmService._();

  factory AlarmService.getInstance() => _instance;

  FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  AndroidNotificationDetails _androidNotificationDetails =
      AndroidNotificationDetails('repeatDailyAtTime channel id',
          'repeatDailyAtTime channel name', 'repeatDailyAtTime description');
  IOSNotificationDetails _iosNotificationDetails = IOSNotificationDetails();

  NotificationDetails _notificationDetails;

  _initState() {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('app_logo');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    this._flutterLocalNotificationsPlugin.initialize(initializationSettings);
    this._notificationDetails = NotificationDetails(
        this._androidNotificationDetails, this._iosNotificationDetails);
  }

  showNow() async {
    await _flutterLocalNotificationsPlugin.show(
        0, "Test title", "Test Description", _notificationDetails);
  }

  testSchedule() async {
    var scheduledNotificationDateTime =
        new DateTime.now().add(new Duration(seconds: 5));
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your other channel id',
        'your other channel name',
        'your other channel description');
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    NotificationDetails platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await _flutterLocalNotificationsPlugin.schedule(
        1,
        'scheduled title',
        'scheduled body',
        scheduledNotificationDateTime,
        platformChannelSpecifics);
  }

  showDaily(NotificationInfo info) async {
    await _flutterLocalNotificationsPlugin.showDailyAtTime(
        info.id,
        info.title,
        info.description ?? "Take your medicine",
        info.getTime(),
        _notificationDetails);
  }

  showWeekly(NotificationInfo info, int day) async {
    await _flutterLocalNotificationsPlugin.showWeeklyAtDayAndTime(
        info.id,
        info.title,
        info.description ?? "Take your medicine",
        Day(day),
        info.getTime(),
        _notificationDetails);
  }

  schedule(NotificationInfo info) async {
    await _flutterLocalNotificationsPlugin.schedule(
        info.id,
        info.title,
        info.description ?? "Take your medicine",
        info.dateTime,
        _notificationDetails);
  }

  cancelNotification(int id) async {
    await _flutterLocalNotificationsPlugin.cancel(id);
  }

  cancelAll() async {
    await _flutterLocalNotificationsPlugin.cancelAll();
  }
}

class NotificationInfo {
  int id;
  String title;
  String description;
  int hours;
  int minutes;

  DateTime dateTime;

  NotificationInfo(
      {@required this.id,
      @required this.title,
      this.description,
      this.hours,
      this.minutes,
      this.dateTime});

  Time getTime() {
    return Time(hours, minutes, 0);
  }
}
