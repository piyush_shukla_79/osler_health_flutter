import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/serializers/serializers.dart';
import 'package:osler_health/serializers/types.dart';
import 'package:osler_health/services/api_service.dart';

class NotificationService extends APIService {
  NotificationService._();

  static final NotificationService _instance = new NotificationService._();

  factory NotificationService() => _instance;

  Future<List<AppNotification>> list() async {
    final response = await this.get("/patient/me/notifications");

    return (serializers.deserialize(response["data"],
            specifiedType: notificationList) as BuiltList<AppNotification>)
        .toList();
  }

  Future<void> markRead(int id) {
    return this.post("/patient/notifications/$id/mark-read", body: {"read": 1});
  }
}
