import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/responses/error.dart';
import 'package:osler_health/models/transaction.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/serializers/serializers.dart';
import 'package:osler_health/serializers/types.dart';
import 'package:osler_health/services/api_service.dart';
import 'package:osler_health/services/error_service.dart';

class UserService extends APIService {
  UserService._();

  static final UserService _instance = UserService._();

  factory UserService.getInstance() => _instance;

  Future<User> getDetails() async {
    final response = await this.get("/patient/me");

    return serializers.deserializeWith(User.serializer, response["data"]);
  }

  Future<User> updateMe(User user) async {
    try {
      final response = await this.patch("/patient/me", body: user.toMap());
      print(response.toString());
      return serializers.deserializeWith(User.serializer, response["data"]);
    } on NetworkError catch (error) {
      ErrorService.getInstance().broadcast(
          type: ErrorCode.updateUser, data: error);
      return null;
    }
  }

  Future<List<Reminder>> getReminders() async {
    final response = await this.get("/patient/me/reminders");

    return (serializers.deserialize(response["data"],
            specifiedType: reminderList) as BuiltList<Reminder>)
        .toList();
  }

  Future<Medicine> saveMedicine(Map<String, dynamic> medicine) async {
    try {
      final response =
      await this.post("/patient/me/medications", body: medicine);

      return serializers.deserializeWith(Medicine.serializer, response["data"]);
    } on NetworkError catch (error) {
      ErrorService.getInstance()
          .broadcast(type: ErrorCode.saveMedicine, data: error);
      return null;
    }
  }

  Future<List<Transaction>> getTransactions() async {
    try {
      final response = await this.get("/patient/me/points-transactions");

      return (serializers.deserialize(response["data"],
              specifiedType: transactionsList) as BuiltList<Transaction>)
          .toList();
    } on NetworkError catch (err) {
      ErrorService.getInstance()
          .broadcast(type: ErrorCode.transactionsLoadError, data: err);
      return null;
    }
  }
}
