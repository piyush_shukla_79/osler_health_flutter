import 'dart:async';

import 'package:osler_health/models/responses/error.dart';
import 'package:osler_health/models/responses/login_response.dart';
import 'package:osler_health/models/user.dart';
import 'package:osler_health/serializers/serializers.dart';
import 'package:osler_health/services/api_service.dart';
import 'package:osler_health/services/error_service.dart';

class AuthService extends APIService {
  AuthService._();

  static final AuthService _instance = AuthService._();

  factory AuthService.getInstance() => _instance;

  Future<LoginResponse> login(String email, String password) async {
    try {
      final response = await this.get("/login",
          params: {"email": email, "password": password},
          useAuthHeaders: false);
      if (response["data"]["patient"] == null) {
        ErrorService.getInstance().broadcast(
          type: ErrorCode.noUser,);
      }
      else
      return LoginResponse(
          response["token"],
          serializers.deserializeWith(
              User.serializer, response["data"]["patient"]["data"]));
    } on NetworkError catch (error) {
      ErrorService.getInstance().broadcast(type: ErrorCode.login, data: error);
    }
  }
}
