import 'package:meta/meta.dart';
import 'package:osler_health/models/broadcaster_event.dart';
import 'package:rxdart/rxdart.dart';

class BroadcasterService {
  BroadcasterService._();

  static final BroadcasterService _instance = new BroadcasterService._();

  factory BroadcasterService() => _instance;

  PublishSubject<BroadcasterEvent> _eventBus =
      new PublishSubject<BroadcasterEvent>();

  void broadcast(
      {@required BroadcasterEventType type, Map<String, dynamic> data}) {
    this._eventBus.add(BroadcasterEvent(type, data));
  }

  Observable<BroadcasterEvent> on(BroadcasterEventType event) {
    return (new Observable(this._eventBus.stream)).where((BroadcasterEvent e) {
      return e.event == event;
    });
  }

  Observable<BroadcasterEvent> includes(List<BroadcasterEventType> events) {
    return (new Observable(this._eventBus.stream)).where((BroadcasterEvent e) {
      return events.indexWhere((BroadcasterEventType innerEvent) {
            return innerEvent == e.event;
          }) >=
          0;
    });
  }

  Observable<BroadcasterEvent> excludes(List<BroadcasterEventType> events) {
    return (new Observable(this._eventBus.stream)).where((BroadcasterEvent e) {
      return events.indexWhere((BroadcasterEventType innerEvent) {
            return innerEvent == e.event;
          }) <
          0;
    });
  }
}

// Added Broadcaster events here
enum BroadcasterEventType {
  loginComplete,
  medicineSaved,
  reminderSaved,
  logout,
  notificationsLoaded,
  onSavedTap
}
