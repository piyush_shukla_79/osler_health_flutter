import 'package:meta/meta.dart';
import 'package:osler_health/models/responses/error.dart';
import 'package:rxdart/rxdart.dart';

class ErrorService {
  ErrorService._();

  static final ErrorService _instance = new ErrorService._();

  factory ErrorService.getInstance() => _instance;

  PublishSubject<ErrorEvent> _eventBus = new PublishSubject<ErrorEvent>();

  void broadcast({@required ErrorCode type, NetworkError data}) {
    this._eventBus.add(ErrorEvent(type, data));
  }

  Observable<ErrorEvent> on(ErrorCode event) {
    return (new Observable(this._eventBus.stream)).where((ErrorEvent e) {
      return e.type == event;
    });
  }

  Observable<ErrorEvent> includes(List<ErrorCode> events) {
    return (new Observable(this._eventBus.stream)).where((ErrorEvent e) {
      return events.indexWhere((ErrorCode innerEvent) {
            return innerEvent == e.type;
          }) >=
          0;
    });
  }

  Observable<ErrorEvent> excludes(List<ErrorCode> events) {
    return (new Observable(this._eventBus.stream)).where((ErrorEvent e) {
      return events.indexWhere((ErrorCode innerEvent) {
            return innerEvent == e.type;
          }) <
          0;
    });
  }
}

class ErrorEvent {
  ErrorCode type;
  NetworkError error;

  ErrorEvent(this.type, this.error);
}

enum ErrorCode {
  login,
  saveMedicine,
  transactionsLoadError,
  noUser,
  updateUser
}
