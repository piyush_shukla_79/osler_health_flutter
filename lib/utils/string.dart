const REMINDERS_TITLE = "Medications";
const SET_REMINDER_TITLE = "Set Reminder";
const PATIENT_DETAILS_TITLE = "Patient Details";
const EDIT_DETAILS_TITLE = "Edit Details";
const SETTINGS_TITLE = "Settings";
const NOTIFICATIONS_TITLE = "Notifications";

const LOG_IN_BUTTON = "LOG IN";
const LOG_OUT_BUTTON = "LOG OUT";
const SEND_BUTTON = "SEND";
const SAVE_BUTTON = "SAVE";

const GENERAL_DETAILS = "General Details";
const CONTACT_DETAILS = "Contact Details";
const HEALTH_DETAILS = "Health Details";
const CHRONIC_DISEASE = "Chronic Disease";
const REWARD_HISTORY = "Reward History";
const QUICK_NOTES = "Quick Notes";

const LOGIN_WARNING = "Please Enter Valid Email-Id and Password";
