import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class Styles {

  static const double textFieldHeight = 50.0;

  static const Color primaryColor = Color.fromRGBO(16, 58, 94, 1);
  static const Color accentColor = Color.fromRGBO(53, 173, 74, 1);
  static const Color buttonTextColor = Colors.white;
  static const Color hintText = Color.fromRGBO(224, 224, 224, 1.0);
  static const Color backgroundColor = Color.fromRGBO(247, 248, 252, 1);
  static const Color opacityColor=Color.fromRGBO(14, 51, 17, 0.15);
  static const Color iconBackColor = Color.fromRGBO(203, 142, 43, 1);
  static const Color appColor = Color.fromRGBO(247, 248, 252, 1);

  static const TextStyle pageTitleText = TextStyle(
    color: primaryColor,
    fontSize: 18.0,
    letterSpacing: 1.5,
    fontWeight: FontWeight.w700,
    fontFamily: 'JosefinSans',);
  static const TextStyle titleText = TextStyle(
      color: primaryColor,
      fontSize: 22.0,
      fontFamily: 'JosefinSans',
      letterSpacing: 1.0,
      fontWeight: FontWeight.w600);
  static const TextStyle buttonText = TextStyle(
      color: buttonTextColor,
      fontFamily: 'JosefinSans',
      fontSize: 18.0, letterSpacing: 1.5);
  static const TextStyle inputTextStyle = TextStyle(
    fontFamily: 'JosefinSans',
    color: Colors.black,
    fontSize: 20.0,);
  static const TextStyle subtitleText = TextStyle(
    color: Styles.primaryColor, fontSize: 12.0,
    fontFamily: 'JosefinSans',);
  static const TextStyle detailsTitleText = TextStyle(
      color: accentColor,
      fontSize: 14.0,
      fontWeight: FontWeight.w400,
      fontFamily: 'Righteous');
  static const TextStyle detailsResponseText = TextStyle(
      color: primaryColor,
      fontSize: 18,
      letterSpacing: 1.5,
      fontWeight: FontWeight.w600,
      fontFamily: 'JosefinSans');

  static final BoxDecoration textFieldDecoration = BoxDecoration(
      borderRadius: BorderRadius.circular(8.0), color: Colors.white);
}


showSnackBar(BuildContext context, String message) {
  Scaffold.of(context).showSnackBar(
    new SnackBar(
      content: Text(
        message,
      ),
    ),
  );
}
