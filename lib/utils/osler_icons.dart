import 'package:flutter/material.dart';

abstract class OslerIcons {
  static const IconData settings = IconData(0xe902, fontFamily: "osler-icons");
  static const IconData alarm = IconData(0xe901, fontFamily: "osler-icons");
  static const IconData person_outline =
      IconData(0xe903, fontFamily: "osler-icons");
  static const IconData notifications_none =
      IconData(0xe900, fontFamily: "osler-icons");
}
