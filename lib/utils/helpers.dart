import 'dart:math';

import 'package:intl/intl.dart';

String twoDigits(int a) {
  return a.toString().padLeft(2, "0");
}

String yMMMd(DateTime date) {
  return DateFormat.yMMMd().format(date);
}

String yyyyMdddd(DateTime date) {
  return DateFormat("yyyy-MM-dd").format(date);
}

DateTime getDateTime(String dateTime) {
  return DateTime.parse(dateTime);
}

date(DateTime time) {
  return DateFormat.jm().format(time);
}

String medicationDate(DateTime date) {
  return DateFormat.yMd().format(date);
}

String transactionDate(DateTime date) {
  return DateFormat.yMd().format(date);
}

var medicineDate = new DateTime.now();
var formatter = new DateFormat().add_yMd();
String formatted = formatter.format(medicineDate);

int randomNumber([int length = 6]) {
  var rng = new Random();
  var result = 1;
  for (var i = 1; i <= length; i++) {
    result = (result * 10) + rng.nextInt(10);
  }

  return result;
}
