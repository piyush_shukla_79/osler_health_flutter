import 'package:flutter/material.dart';

class HeaderText extends Text {
  HeaderText({
    String data,
    TextStyle style,
    TextAlign textAlign,
    TextOverflow overflow,
    int maxLines,
  }) : super(
          data,
          style: TextStyle(
            fontFamily: style.fontFamily ?? 'Righteous',
            color: style.color,
            backgroundColor: style.backgroundColor,
            fontSize: style.fontSize,
            fontWeight: style.fontWeight,
            fontStyle: style.fontStyle,
            letterSpacing: style.letterSpacing,
            wordSpacing: style.wordSpacing,
            textBaseline: style.textBaseline,
            height: style.height,
            locale: style.locale,
            foreground: style.foreground,
            background: style.background,
            shadows: style.shadows,
            decoration: style.decoration,
            decorationColor: style.decorationColor,
            decorationStyle: style.decorationStyle,
            decorationThickness: style.decorationThickness,
            debugLabel: style.debugLabel,
          ),
          textAlign: textAlign,
          maxLines: maxLines,
          overflow: overflow,
        );
}
