import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:onesignal/onesignal.dart';
import 'package:osler_health/app_config.dart';
import 'package:osler_health/middleware/app_middleware.dart';
import 'package:osler_health/middleware/notification_middleware.dart';
import 'package:osler_health/middleware/reminder_middleware.dart';
import 'package:osler_health/middleware/user_middleware.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/presentation/splash_page.dart';
import 'package:osler_health/reducers/app_state_reducer.dart';
import 'package:osler_health/utils/styles.dart';
import 'package:redux/redux.dart';

class OslerHealthApp extends StatelessWidget {
  static final store = Store<AppState>(appReducer,
      initialState: AppState(),
      middleware: [
        appMiddleware,
        notificationMiddleware,
        reminderMiddleware,
        userMiddleware
      ]);

  @override
  Widget build(BuildContext context) {
    OneSignal.shared.init(AppConfig.oneSignalAppId, iOSSettings: {
      OSiOSSettings.autoPrompt: true,
      OSiOSSettings.inAppLaunchUrl: true
    });
    OneSignal.shared
        .setInFocusDisplayType(OSNotificationDisplayType.notification);

    return StoreProvider(
      store: OslerHealthApp.store,
      child: MaterialApp(
        theme: ThemeData(
            backgroundColor: Styles.appColor,
            primaryColor: Styles.primaryColor,
            accentColor: Styles.accentColor,
            iconTheme: IconThemeData(color: Styles.primaryColor),
            scaffoldBackgroundColor: Styles.backgroundColor,
            fontFamily: 'JosefinSans'),
        debugShowCheckedModeBanner: false,
        title: 'Osler Health',
        home: SplashPage(),
      ),
    );
  }
}
