import 'dart:async';
import 'dart:io' as io;

import 'package:osler_health/models/medicine_record.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  DatabaseHelper._();

  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    print("initDB");
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    _db = theDb;
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    print("created database");
    try {
      await db.execute(
          "CREATE TABLE Record(medicineName Text, recordId number PRIMARY KEY,active number)");
      await db.execute("CREATE TABLE Alarm(hour number, minute number)");
//      await db.execute(
//          "CREATE TABLE User(full_name Text, username Text, phone_number Text, profile_pic TEXT, id number PRIMARY KEY, age number, gender TEXT, code TEXT, about TEXT, country TEXT, token TEXT, coins number, req_coins number)");
//      await db.execute(
//          "CREATE TABLE Video(id number PRIMARY KEY, owner_id number, video TEXT, timeStamp int, channel_id number, cached_at TEXT)");
    } catch (e) {}
  }

//  User related Code

//  Future<int> insertUser(User user, String token) async {
//    if (_db == null) {
//      await initDb();
//    }
//    try {
//      print("insert user");
//      Map<String, dynamic> data = new Map();
//      data['token'] = token;
//      Map<String, dynamic> data2 = Map<String, dynamic>.from(
//          userSerializers.serializeWith(User.serializer, user));
//      data.addAll(data2);
//      int id = await _db.insert("User", data);
//      return id;
//    } catch (e) {
//      print(e);
//    }
//  }
//
//  Future<void> updateUser(int user_id, Map<String, dynamic> data) async {
//    if (_db == null) {
//      await initDb();
//    }
//
//    try {
//      await _db.update("User", data, where: "id = ?", whereArgs: [
//        user_id.toString(),
//      ]);
//      print("user updated");
//    } catch (e) {
//      print("log " + e.toString());
//    }
//  }
//

  Future<int> insertRecord(MedicineRecord record, String token) async {
    if (_db == null) await initDb();
    try {
      print("Medicine Record");
      print(record.toString());
    } catch (e) {
      print("Error " + e);
    }
  }

  Future<void> updateRecord(int recordId, Map<String, dynamic> record) {
    if (_db == null) initDb();
    try {
      _db.update("Record", record, where: "id=?", whereArgs: [recordId]);
    } catch (e) {
      print("Error " + e.toString());
    }
  }
}
