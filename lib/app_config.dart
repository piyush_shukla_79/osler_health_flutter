abstract class AppConfig {
  static String baseUrl;
  static String oneSignalAppId;
  static String dummyImageUrl;
  static String termsAndConditionUrl;
  static String privacyPolicyUrl;

  static setupEnv(Environment env) {
    switch (env) {
      case Environment.dev:
        {
          AppConfig.baseUrl = "https://staging-api.osler4health.com/api";
          AppConfig.oneSignalAppId = "f13f33fa-7668-41cb-98d9-823251aa0a8d";
          AppConfig.dummyImageUrl =
              "https://dummyimage.com/300x300/f2eef0/ba78a8.png&text=";
          AppConfig.termsAndConditionUrl =
              "https://staging-dash.osler4health.com/assets/terms-and-conditions.html";
          AppConfig.privacyPolicyUrl =
              "https://staging-dash.osler4health.com/assets/privacy-policy.html";

          break;
        }

      case Environment.stage:
        {
          AppConfig.baseUrl = "https://staging-api.osler4health.com/api";
          AppConfig.oneSignalAppId = "f13f33fa-7668-41cb-98d9-823251aa0a8d";
          AppConfig.dummyImageUrl =
              "https://dummyimage.com/300x300/f2eef0/ba78a8.png&text=";
          AppConfig.termsAndConditionUrl =
              "https://staging-dash.osler4health.com/assets/terms-and-conditions.html";
          AppConfig.privacyPolicyUrl =
              "https://staging-dash.osler4health.com/assets/privacy-policy.html";

          break;
        }

      case Environment.prod:
        {
          AppConfig.baseUrl = "https://api.osler4health.com/api";
          AppConfig.oneSignalAppId = "f13f33fa-7668-41cb-98d9-823251aa0a8d";
          AppConfig.dummyImageUrl =
              "https://dummyimage.com/300x300/f2eef0/ba78a8.png&text=";
          AppConfig.termsAndConditionUrl =
              "https://dashboard.osler4health.com/assets/terms-and-conditions.html";
          AppConfig.privacyPolicyUrl =
              "https://dashboard.osler4health.com/assets/privacy-policy.html";
        }
    }
  }
}

enum Environment { dev, stage, prod }
