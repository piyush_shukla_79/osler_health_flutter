import 'dart:async';

import 'package:osler_health/actions/medicine_action.dart';
import 'package:osler_health/actions/reminder_action.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/services/broadcaster_service.dart';
import 'package:osler_health/services/user_service.dart';
import 'package:redux/redux.dart';

Future<Null> userMiddleware(
    Store<AppState> store, action, NextDispatcher next) async {
  switch (action.runtimeType) {
    case FetchLoggedInUserDetails:
      if (!store.state.user.isLoading && !store.state.user.isLoaded) {
        next(action);

        final user = await UserService.getInstance().getDetails();

        store.dispatch(ListMedicinesComplete(user.medicines.data.toList()));
        store.dispatch(ListRemindersComplete(user.reminders.data.toList()));
        store.dispatch(UpdateUserInfo(user));
      }
      break;

    case UpdateLoggedInUser:
      next(action);
      final user = await UserService.getInstance().updateMe(action.user);
      if (user != null)
        store.dispatch(UpdateUserInfo(user));
      else
        store.dispatch(StopLoading());

      break;

    case CreateMedicine:
      next(action);

      final medicine =
          await UserService.getInstance().saveMedicine(action.medicine);

      if (medicine != null) {
        store.dispatch(CreateMedicineComplete(medicine));
        BroadcasterService()
            .broadcast(type: BroadcasterEventType.medicineSaved);
      }

      break;

    case LoadTransactions:
      next(action);
      final transactions = await UserService.getInstance().getTransactions();

      if (transactions != null) {
        store.dispatch(LoadTransactionsComplete(transactions));
      }
      break;
    case StopLoading:
      next(action);
      break;
    default:
      next(action);
  }
}
