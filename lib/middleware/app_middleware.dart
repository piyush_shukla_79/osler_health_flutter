import 'dart:async';

import 'package:onesignal/onesignal.dart';
import 'package:osler_health/actions/actions.dart';
import 'package:osler_health/actions/user_action.dart';
import 'package:osler_health/database/database_helper.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/responses/login_response.dart';
import 'package:osler_health/services/auth_service.dart';
import 'package:osler_health/services/broadcaster_service.dart';
import 'package:osler_health/services/preferences_service.dart';
import 'package:redux/redux.dart';

AuthService authService = AuthService.getInstance();
BroadcasterService broadcasterService = BroadcasterService();

DatabaseHelper databaseHelper = new DatabaseHelper();

Future<Null> appMiddleware(
    Store<AppState> store, action, NextDispatcher next) async {
  switch (action.runtimeType) {
    case CheckSession:
      break;

    case LogIn:
      next(action);

      final LoginResponse data =
          await authService.login(action.email, action.password);

      if (data != null) {
        await PreferencesService().setAuthToken(data.token);
        await PreferencesService().setAuthUser(data.user);

        OneSignal.shared.sendTag("email", data.user.getOneSignalEmail());

        store.dispatch(LogInComplete(currentUser: data.user));
        BroadcasterService()
            .broadcast(type: BroadcasterEventType.loginComplete);
      }

      break;
    case LogInComplete:
      next(action);
      break;

    case UpdateUserInfo:
      next(action);
      try {
        await PreferencesService().setAuthUser(action.user);
      } catch (e) {
        print("Update User Info error");
        print(e);
      }
      break;

    case Logout:
      next(action);

      await PreferencesService().logout();
      OneSignal.shared.deleteTag("email");

      break;

    default:
      next(action);
  }
}
