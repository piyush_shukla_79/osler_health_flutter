import 'dart:async';

import 'package:osler_health/actions/reminder_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/services/alarm_service.dart';
import 'package:osler_health/services/broadcaster_service.dart';
import 'package:osler_health/services/reminder_service.dart';
import 'package:redux/redux.dart';

Future<Null> reminderMiddleware(
    Store<AppState> store, action, NextDispatcher next) async {
  switch (action.runtimeType) {
    case ListReminders:
      next(action);
      final reminders = await ReminderService.getInstance().listReminders();
      store.dispatch(ListRemindersComplete(reminders));
      break;
    case UpdateReminder:
      next(action);
      final updatedReminder =
          await ReminderService.getInstance().update(action.reminder);
      final medicinesList = store.state.medicines.getAll();
      if (updatedReminder != null) {
        BroadcasterService().broadcast(
            type: BroadcasterEventType.reminderSaved,
            data: updatedReminder.toMap());

        store.dispatch(UpdateReminderComplete(updatedReminder));
        await AlarmService.getInstance().cancelAll();

        medicinesList.forEach((Medicine med) {
          final reminder =
          store.state.reminder.getEntities()[med.medicinePatientId];

          if (reminder != null) {
            ReminderService.getInstance().setReminder(reminder, med);
          }
        });
      }

      break;

    case CreateReminder:
      next(action);

      final updatedReminder =
          await ReminderService.getInstance().create(action.reminder);
      final medicinesList = store.state.medicines.getAll();

      if (updatedReminder != null) {
        store.dispatch(UpdateReminderComplete(updatedReminder));

        await AlarmService.getInstance().cancelAll();

        medicinesList.forEach((Medicine med) {
          final reminder =
              store.state.reminder.getEntities()[med.medicinePatientId];

          if (reminder != null) {
            ReminderService.getInstance().setReminder(reminder, med);
          }
        });

        BroadcasterService().broadcast(
            type: BroadcasterEventType.reminderSaved,
            data: updatedReminder.toMap());
      }

      break;
    default:
      next(action);
  }
}

const dartToNotif = {1: 2, 2: 3, 3: 4, 4: 5, 5: 6, 6: 7, 7: 1};
