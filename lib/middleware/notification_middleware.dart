import 'dart:async';

import 'package:osler_health/actions/notification_action.dart';
import 'package:osler_health/models/app_state.dart';
import 'package:osler_health/services/broadcaster_service.dart';
import 'package:osler_health/services/notification_service.dart';
import 'package:redux/redux.dart';

Future<Null> notificationMiddleware(
    Store<AppState> store, action, NextDispatcher next) async {
  switch (action.runtimeType) {
    case ListNotification:
      next(action);
      final notifications = await NotificationService().list();
      store.dispatch(ListNotificationComplete(notifications));
      BroadcasterService()
          .broadcast(type: BroadcasterEventType.notificationsLoaded);
      break;
    case ListNotificationComplete:
      next(action);
      break;
    case UpdateNotification:
      try {
        await NotificationService().markRead(action.notification.id);
        next(action);
      } catch (e) {
        print(e);
      }
      break;
    default:
      next(action);
  }
}
