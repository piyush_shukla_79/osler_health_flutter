import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:osler_health/models/mixins/base_model.dart';
import 'package:osler_health/models/reminder_notification.dart';

part 'reminder.g.dart';

abstract class Reminder
    with BaseModel
    implements Built<Reminder, ReminderBuilder> {
  @nullable
  int get id;

  @nullable
  String get title;

  BuiltList<ReminderNotification> get notifications;

  @BuiltValueField(wireName: "entity_type")
  String get entityType;

  @BuiltValueField(wireName: "entity_id")
  int get entityId;

  @nullable
  @BuiltValueField(wireName: "end_date")
  String get endDate;

  @nullable
  @BuiltValueField(wireName: "created_at")
  String get createdAt;

  @nullable
  @BuiltValueField(wireName: "updated_at")
  String get updatedAt;

  @nullable
  @BuiltValueField(wireName: "start_date")
  String get startDate;

  Reminder._();

  factory Reminder([updates(ReminderBuilder b)]) = _$Reminder;

  static Serializer<Reminder> get serializer => _$reminderSerializer;

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "title": title,
      "entity_type": entityType,
      "entity_id": entityId,
      "end_date": endDate,
      "start_date": startDate,
      "notifications": notifications.map((n) => n.toMap()).toList()
    };
  }
}
