// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AppState> _$appStateSerializer = new _$AppStateSerializer();
Serializer<UserState> _$userStateSerializer = new _$UserStateSerializer();
Serializer<AppNotificationState> _$appNotificationStateSerializer =
    new _$AppNotificationStateSerializer();
Serializer<ReminderState> _$reminderStateSerializer =
    new _$ReminderStateSerializer();
Serializer<MedicineState> _$medicineStateSerializer =
    new _$MedicineStateSerializer();

class _$AppStateSerializer implements StructuredSerializer<AppState> {
  @override
  final Iterable<Type> types = const [AppState, _$AppState];
  @override
  final String wireName = 'AppState';

  @override
  Iterable serialize(Serializers serializers, AppState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.user != null) {
      result
        ..add('user')
        ..add(serializers.serialize(object.user,
            specifiedType: const FullType(UserState)));
    }
    if (object.appNotification != null) {
      result
        ..add('appNotification')
        ..add(serializers.serialize(object.appNotification,
            specifiedType: const FullType(AppNotificationState)));
    }
    if (object.reminder != null) {
      result
        ..add('reminder')
        ..add(serializers.serialize(object.reminder,
            specifiedType: const FullType(ReminderState)));
    }
    if (object.medicines != null) {
      result
        ..add('medicines')
        ..add(serializers.serialize(object.medicines,
            specifiedType: const FullType(MedicineState)));
    }

    return result;
  }

  @override
  AppState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AppStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(UserState)) as UserState);
          break;
        case 'appNotification':
          result.appNotification.replace(serializers.deserialize(value,
                  specifiedType: const FullType(AppNotificationState))
              as AppNotificationState);
          break;
        case 'reminder':
          result.reminder.replace(serializers.deserialize(value,
              specifiedType: const FullType(ReminderState)) as ReminderState);
          break;
        case 'medicines':
          result.medicines.replace(serializers.deserialize(value,
              specifiedType: const FullType(MedicineState)) as MedicineState);
          break;
      }
    }

    return result.build();
  }
}

class _$UserStateSerializer implements StructuredSerializer<UserState> {
  @override
  final Iterable<Type> types = const [UserState, _$UserState];
  @override
  final String wireName = 'UserState';

  @override
  Iterable serialize(Serializers serializers, UserState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.isLoading != null) {
      result
        ..add('isLoading')
        ..add(serializers.serialize(object.isLoading,
            specifiedType: const FullType(bool)));
    }
    if (object.isLoaded != null) {
      result
        ..add('isLoaded')
        ..add(serializers.serialize(object.isLoaded,
            specifiedType: const FullType(bool)));
    }
    if (object.isLoggedIn != null) {
      result
        ..add('isLoggedIn')
        ..add(serializers.serialize(object.isLoggedIn,
            specifiedType: const FullType(bool)));
    }
    if (object.profile != null) {
      result
        ..add('profile')
        ..add(serializers.serialize(object.profile,
            specifiedType: const FullType(User)));
    }
    if (object.transactions != null) {
      result
        ..add('transactions')
        ..add(serializers.serialize(object.transactions,
            specifiedType: const FullType(
                BuiltList, const [const FullType(Transaction)])));
    }

    return result;
  }

  @override
  UserState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'isLoading':
          result.isLoading = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'isLoaded':
          result.isLoaded = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'isLoggedIn':
          result.isLoggedIn = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'profile':
          result.profile.replace(serializers.deserialize(value,
              specifiedType: const FullType(User)) as User);
          break;
        case 'transactions':
          result.transactions.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Transaction)]))
              as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$AppNotificationStateSerializer
    implements StructuredSerializer<AppNotificationState> {
  @override
  final Iterable<Type> types = const [
    AppNotificationState,
    _$AppNotificationState
  ];
  @override
  final String wireName = 'AppNotificationState';

  @override
  Iterable serialize(Serializers serializers, AppNotificationState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'isLoading',
      serializers.serialize(object.isLoading,
          specifiedType: const FullType(bool)),
      'isLoaded',
      serializers.serialize(object.isLoaded,
          specifiedType: const FullType(bool)),
      'ids',
      serializers.serialize(object.ids,
          specifiedType:
              const FullType(BuiltList, const [const FullType(int)])),
      'entities',
      serializers.serialize(object.entities,
          specifiedType: const FullType(BuiltMap,
              const [const FullType(int), const FullType(AppNotification)])),
    ];

    return result;
  }

  @override
  AppNotificationState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AppNotificationStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'isLoading':
          result.isLoading = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'isLoaded':
          result.isLoaded = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'ids':
          result.ids.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(int)]))
              as BuiltList);
          break;
        case 'entities':
          result.entities.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap, const [
                const FullType(int),
                const FullType(AppNotification)
              ])) as BuiltMap);
          break;
      }
    }

    return result.build();
  }
}

class _$ReminderStateSerializer implements StructuredSerializer<ReminderState> {
  @override
  final Iterable<Type> types = const [ReminderState, _$ReminderState];
  @override
  final String wireName = 'ReminderState';

  @override
  Iterable serialize(Serializers serializers, ReminderState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ids',
      serializers.serialize(object.ids,
          specifiedType:
              const FullType(BuiltList, const [const FullType(int)])),
      'entities',
      serializers.serialize(object.entities,
          specifiedType: const FullType(
              BuiltMap, const [const FullType(int), const FullType(Reminder)])),
      'isLoading',
      serializers.serialize(object.isLoading,
          specifiedType: const FullType(bool)),
    ];

    return result;
  }

  @override
  ReminderState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ReminderStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ids':
          result.ids.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(int)]))
              as BuiltList);
          break;
        case 'entities':
          result.entities.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap, const [
                const FullType(int),
                const FullType(Reminder)
              ])) as BuiltMap);
          break;
        case 'isLoading':
          result.isLoading = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$MedicineStateSerializer implements StructuredSerializer<MedicineState> {
  @override
  final Iterable<Type> types = const [MedicineState, _$MedicineState];
  @override
  final String wireName = 'MedicineState';

  @override
  Iterable serialize(Serializers serializers, MedicineState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ids',
      serializers.serialize(object.ids,
          specifiedType:
              const FullType(BuiltList, const [const FullType(int)])),
      'entities',
      serializers.serialize(object.entities,
          specifiedType: const FullType(
              BuiltMap, const [const FullType(int), const FullType(Medicine)])),
      'isLoading',
      serializers.serialize(object.isLoading,
          specifiedType: const FullType(bool)),
    ];

    return result;
  }

  @override
  MedicineState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MedicineStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ids':
          result.ids.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(int)]))
              as BuiltList);
          break;
        case 'entities':
          result.entities.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap, const [
                const FullType(int),
                const FullType(Medicine)
              ])) as BuiltMap);
          break;
        case 'isLoading':
          result.isLoading = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$AppState extends AppState {
  @override
  final UserState user;
  @override
  final AppNotificationState appNotification;
  @override
  final ReminderState reminder;
  @override
  final MedicineState medicines;

  factory _$AppState([void updates(AppStateBuilder b)]) =>
      (new AppStateBuilder()..update(updates)).build();

  _$AppState._({this.user, this.appNotification, this.reminder, this.medicines})
      : super._();

  @override
  AppState rebuild(void updates(AppStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  AppStateBuilder toBuilder() => new AppStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AppState &&
        user == other.user &&
        appNotification == other.appNotification &&
        reminder == other.reminder &&
        medicines == other.medicines;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, user.hashCode), appNotification.hashCode),
            reminder.hashCode),
        medicines.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AppState')
          ..add('user', user)
          ..add('appNotification', appNotification)
          ..add('reminder', reminder)
          ..add('medicines', medicines))
        .toString();
  }
}

class AppStateBuilder implements Builder<AppState, AppStateBuilder> {
  _$AppState _$v;

  UserStateBuilder _user;
  UserStateBuilder get user => _$this._user ??= new UserStateBuilder();
  set user(UserStateBuilder user) => _$this._user = user;

  AppNotificationStateBuilder _appNotification;
  AppNotificationStateBuilder get appNotification =>
      _$this._appNotification ??= new AppNotificationStateBuilder();
  set appNotification(AppNotificationStateBuilder appNotification) =>
      _$this._appNotification = appNotification;

  ReminderStateBuilder _reminder;
  ReminderStateBuilder get reminder =>
      _$this._reminder ??= new ReminderStateBuilder();
  set reminder(ReminderStateBuilder reminder) => _$this._reminder = reminder;

  MedicineStateBuilder _medicines;
  MedicineStateBuilder get medicines =>
      _$this._medicines ??= new MedicineStateBuilder();
  set medicines(MedicineStateBuilder medicines) =>
      _$this._medicines = medicines;

  AppStateBuilder();

  AppStateBuilder get _$this {
    if (_$v != null) {
      _user = _$v.user?.toBuilder();
      _appNotification = _$v.appNotification?.toBuilder();
      _reminder = _$v.reminder?.toBuilder();
      _medicines = _$v.medicines?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AppState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AppState;
  }

  @override
  void update(void updates(AppStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$AppState build() {
    _$AppState _$result;
    try {
      _$result = _$v ??
          new _$AppState._(
              user: _user?.build(),
              appNotification: _appNotification?.build(),
              reminder: _reminder?.build(),
              medicines: _medicines?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        _user?.build();
        _$failedField = 'appNotification';
        _appNotification?.build();
        _$failedField = 'reminder';
        _reminder?.build();
        _$failedField = 'medicines';
        _medicines?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AppState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$UserState extends UserState {
  @override
  final bool isLoading;
  @override
  final bool isLoaded;
  @override
  final bool isLoggedIn;
  @override
  final User profile;
  @override
  final BuiltList<Transaction> transactions;

  factory _$UserState([void updates(UserStateBuilder b)]) =>
      (new UserStateBuilder()..update(updates)).build();

  _$UserState._(
      {this.isLoading,
      this.isLoaded,
      this.isLoggedIn,
      this.profile,
      this.transactions})
      : super._();

  @override
  UserState rebuild(void updates(UserStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserStateBuilder toBuilder() => new UserStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserState &&
        isLoading == other.isLoading &&
        isLoaded == other.isLoaded &&
        isLoggedIn == other.isLoggedIn &&
        profile == other.profile &&
        transactions == other.transactions;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, isLoading.hashCode), isLoaded.hashCode),
                isLoggedIn.hashCode),
            profile.hashCode),
        transactions.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserState')
          ..add('isLoading', isLoading)
          ..add('isLoaded', isLoaded)
          ..add('isLoggedIn', isLoggedIn)
          ..add('profile', profile)
          ..add('transactions', transactions))
        .toString();
  }
}

class UserStateBuilder implements Builder<UserState, UserStateBuilder> {
  _$UserState _$v;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _isLoaded;
  bool get isLoaded => _$this._isLoaded;
  set isLoaded(bool isLoaded) => _$this._isLoaded = isLoaded;

  bool _isLoggedIn;
  bool get isLoggedIn => _$this._isLoggedIn;
  set isLoggedIn(bool isLoggedIn) => _$this._isLoggedIn = isLoggedIn;

  UserBuilder _profile;
  UserBuilder get profile => _$this._profile ??= new UserBuilder();
  set profile(UserBuilder profile) => _$this._profile = profile;

  ListBuilder<Transaction> _transactions;
  ListBuilder<Transaction> get transactions =>
      _$this._transactions ??= new ListBuilder<Transaction>();
  set transactions(ListBuilder<Transaction> transactions) =>
      _$this._transactions = transactions;

  UserStateBuilder();

  UserStateBuilder get _$this {
    if (_$v != null) {
      _isLoading = _$v.isLoading;
      _isLoaded = _$v.isLoaded;
      _isLoggedIn = _$v.isLoggedIn;
      _profile = _$v.profile?.toBuilder();
      _transactions = _$v.transactions?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserState;
  }

  @override
  void update(void updates(UserStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserState build() {
    _$UserState _$result;
    try {
      _$result = _$v ??
          new _$UserState._(
              isLoading: isLoading,
              isLoaded: isLoaded,
              isLoggedIn: isLoggedIn,
              profile: _profile?.build(),
              transactions: _transactions?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'profile';
        _profile?.build();
        _$failedField = 'transactions';
        _transactions?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$AppNotificationState extends AppNotificationState {
  @override
  final bool isLoading;
  @override
  final bool isLoaded;
  @override
  final BuiltList<int> ids;
  @override
  final BuiltMap<int, AppNotification> entities;

  factory _$AppNotificationState(
          [void updates(AppNotificationStateBuilder b)]) =>
      (new AppNotificationStateBuilder()..update(updates)).build();

  _$AppNotificationState._(
      {this.isLoading, this.isLoaded, this.ids, this.entities})
      : super._() {
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('AppNotificationState', 'isLoading');
    }
    if (isLoaded == null) {
      throw new BuiltValueNullFieldError('AppNotificationState', 'isLoaded');
    }
    if (ids == null) {
      throw new BuiltValueNullFieldError('AppNotificationState', 'ids');
    }
    if (entities == null) {
      throw new BuiltValueNullFieldError('AppNotificationState', 'entities');
    }
  }

  @override
  AppNotificationState rebuild(void updates(AppNotificationStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  AppNotificationStateBuilder toBuilder() =>
      new AppNotificationStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AppNotificationState &&
        isLoading == other.isLoading &&
        isLoaded == other.isLoaded &&
        ids == other.ids &&
        entities == other.entities;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, isLoading.hashCode), isLoaded.hashCode), ids.hashCode),
        entities.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AppNotificationState')
          ..add('isLoading', isLoading)
          ..add('isLoaded', isLoaded)
          ..add('ids', ids)
          ..add('entities', entities))
        .toString();
  }
}

class AppNotificationStateBuilder
    implements Builder<AppNotificationState, AppNotificationStateBuilder> {
  _$AppNotificationState _$v;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _isLoaded;
  bool get isLoaded => _$this._isLoaded;
  set isLoaded(bool isLoaded) => _$this._isLoaded = isLoaded;

  ListBuilder<int> _ids;
  ListBuilder<int> get ids => _$this._ids ??= new ListBuilder<int>();
  set ids(ListBuilder<int> ids) => _$this._ids = ids;

  MapBuilder<int, AppNotification> _entities;
  MapBuilder<int, AppNotification> get entities =>
      _$this._entities ??= new MapBuilder<int, AppNotification>();
  set entities(MapBuilder<int, AppNotification> entities) =>
      _$this._entities = entities;

  AppNotificationStateBuilder();

  AppNotificationStateBuilder get _$this {
    if (_$v != null) {
      _isLoading = _$v.isLoading;
      _isLoaded = _$v.isLoaded;
      _ids = _$v.ids?.toBuilder();
      _entities = _$v.entities?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AppNotificationState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AppNotificationState;
  }

  @override
  void update(void updates(AppNotificationStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$AppNotificationState build() {
    _$AppNotificationState _$result;
    try {
      _$result = _$v ??
          new _$AppNotificationState._(
              isLoading: isLoading,
              isLoaded: isLoaded,
              ids: ids.build(),
              entities: entities.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'ids';
        ids.build();
        _$failedField = 'entities';
        entities.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AppNotificationState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$ReminderState extends ReminderState {
  @override
  final BuiltList<int> ids;
  @override
  final BuiltMap<int, Reminder> entities;
  @override
  final bool isLoading;

  factory _$ReminderState([void updates(ReminderStateBuilder b)]) =>
      (new ReminderStateBuilder()..update(updates)).build();

  _$ReminderState._({this.ids, this.entities, this.isLoading}) : super._() {
    if (ids == null) {
      throw new BuiltValueNullFieldError('ReminderState', 'ids');
    }
    if (entities == null) {
      throw new BuiltValueNullFieldError('ReminderState', 'entities');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('ReminderState', 'isLoading');
    }
  }

  @override
  ReminderState rebuild(void updates(ReminderStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  ReminderStateBuilder toBuilder() => new ReminderStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ReminderState &&
        ids == other.ids &&
        entities == other.entities &&
        isLoading == other.isLoading;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, ids.hashCode), entities.hashCode), isLoading.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ReminderState')
          ..add('ids', ids)
          ..add('entities', entities)
          ..add('isLoading', isLoading))
        .toString();
  }
}

class ReminderStateBuilder
    implements Builder<ReminderState, ReminderStateBuilder> {
  _$ReminderState _$v;

  ListBuilder<int> _ids;
  ListBuilder<int> get ids => _$this._ids ??= new ListBuilder<int>();
  set ids(ListBuilder<int> ids) => _$this._ids = ids;

  MapBuilder<int, Reminder> _entities;
  MapBuilder<int, Reminder> get entities =>
      _$this._entities ??= new MapBuilder<int, Reminder>();
  set entities(MapBuilder<int, Reminder> entities) =>
      _$this._entities = entities;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  ReminderStateBuilder();

  ReminderStateBuilder get _$this {
    if (_$v != null) {
      _ids = _$v.ids?.toBuilder();
      _entities = _$v.entities?.toBuilder();
      _isLoading = _$v.isLoading;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ReminderState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ReminderState;
  }

  @override
  void update(void updates(ReminderStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$ReminderState build() {
    _$ReminderState _$result;
    try {
      _$result = _$v ??
          new _$ReminderState._(
              ids: ids.build(),
              entities: entities.build(),
              isLoading: isLoading);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'ids';
        ids.build();
        _$failedField = 'entities';
        entities.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ReminderState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$MedicineState extends MedicineState {
  @override
  final BuiltList<int> ids;
  @override
  final BuiltMap<int, Medicine> entities;
  @override
  final bool isLoading;

  factory _$MedicineState([void updates(MedicineStateBuilder b)]) =>
      (new MedicineStateBuilder()..update(updates)).build();

  _$MedicineState._({this.ids, this.entities, this.isLoading}) : super._() {
    if (ids == null) {
      throw new BuiltValueNullFieldError('MedicineState', 'ids');
    }
    if (entities == null) {
      throw new BuiltValueNullFieldError('MedicineState', 'entities');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('MedicineState', 'isLoading');
    }
  }

  @override
  MedicineState rebuild(void updates(MedicineStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  MedicineStateBuilder toBuilder() => new MedicineStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MedicineState &&
        ids == other.ids &&
        entities == other.entities &&
        isLoading == other.isLoading;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, ids.hashCode), entities.hashCode), isLoading.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MedicineState')
          ..add('ids', ids)
          ..add('entities', entities)
          ..add('isLoading', isLoading))
        .toString();
  }
}

class MedicineStateBuilder
    implements Builder<MedicineState, MedicineStateBuilder> {
  _$MedicineState _$v;

  ListBuilder<int> _ids;
  ListBuilder<int> get ids => _$this._ids ??= new ListBuilder<int>();
  set ids(ListBuilder<int> ids) => _$this._ids = ids;

  MapBuilder<int, Medicine> _entities;
  MapBuilder<int, Medicine> get entities =>
      _$this._entities ??= new MapBuilder<int, Medicine>();
  set entities(MapBuilder<int, Medicine> entities) =>
      _$this._entities = entities;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  MedicineStateBuilder();

  MedicineStateBuilder get _$this {
    if (_$v != null) {
      _ids = _$v.ids?.toBuilder();
      _entities = _$v.entities?.toBuilder();
      _isLoading = _$v.isLoading;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MedicineState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MedicineState;
  }

  @override
  void update(void updates(MedicineStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$MedicineState build() {
    _$MedicineState _$result;
    try {
      _$result = _$v ??
          new _$MedicineState._(
              ids: ids.build(),
              entities: entities.build(),
              isLoading: isLoading);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'ids';
        ids.build();
        _$failedField = 'entities';
        entities.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'MedicineState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
