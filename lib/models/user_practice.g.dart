// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_practice.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserPractice> _$userPracticeSerializer =
    new _$UserPracticeSerializer();

class _$UserPracticeSerializer implements StructuredSerializer<UserPractice> {
  @override
  final Iterable<Type> types = const [UserPractice, _$UserPractice];
  @override
  final String wireName = 'UserPractice';

  @override
  Iterable serialize(Serializers serializers, UserPractice object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'label',
      serializers.serialize(object.label,
          specifiedType: const FullType(String)),
    ];
    if (object.address1 != null) {
      result
        ..add('address1')
        ..add(serializers.serialize(object.address1,
            specifiedType: const FullType(String)));
    }
    if (object.address2 != null) {
      result
        ..add('address2')
        ..add(serializers.serialize(object.address2,
            specifiedType: const FullType(String)));
    }
    if (object.city != null) {
      result
        ..add('city')
        ..add(serializers.serialize(object.city,
            specifiedType: const FullType(String)));
    }
    if (object.state != null) {
      result
        ..add('state')
        ..add(serializers.serialize(object.state,
            specifiedType: const FullType(String)));
    }
    if (object.zipCode != null) {
      result
        ..add('zip_code')
        ..add(serializers.serialize(object.zipCode,
            specifiedType: const FullType(String)));
    }
    if (object.officeHours != null) {
      result
        ..add('office_hours')
        ..add(serializers.serialize(object.officeHours,
            specifiedType: const FullType(String)));
    }
    if (object.phone != null) {
      result
        ..add('phone')
        ..add(serializers.serialize(object.phone,
            specifiedType: const FullType(String)));
    }
    if (object.emrPlatform != null) {
      result
        ..add('emr_platform')
        ..add(serializers.serialize(object.emrPlatform,
            specifiedType: const FullType(String)));
    }
    if (object.website != null) {
      result
        ..add('website')
        ..add(serializers.serialize(object.website,
            specifiedType: const FullType(String)));
    }
    if (object.tinNumber != null) {
      result
        ..add('tin_number')
        ..add(serializers.serialize(object.tinNumber,
            specifiedType: const FullType(String)));
    }
    if (object.updatedAt != null) {
      result
        ..add('updated_at')
        ..add(serializers.serialize(object.updatedAt,
            specifiedType: const FullType(String)));
    }
    if (object.createdAt != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(String)));
    }
    if (object.latitude != null) {
      result
        ..add('latitude')
        ..add(serializers.serialize(object.latitude,
            specifiedType: const FullType(String)));
    }
    if (object.longitude != null) {
      result
        ..add('longitude')
        ..add(serializers.serialize(object.longitude,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  UserPractice deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserPracticeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'label':
          result.label = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'address1':
          result.address1 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'address2':
          result.address2 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'city':
          result.city = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'state':
          result.state = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'zip_code':
          result.zipCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'office_hours':
          result.officeHours = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'phone':
          result.phone = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'emr_platform':
          result.emrPlatform = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'website':
          result.website = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'tin_number':
          result.tinNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updated_at':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserPractice extends UserPractice {
  @override
  final int id;
  @override
  final String label;
  @override
  final String address1;
  @override
  final String address2;
  @override
  final String city;
  @override
  final String state;
  @override
  final String zipCode;
  @override
  final String officeHours;
  @override
  final String phone;
  @override
  final String emrPlatform;
  @override
  final String website;
  @override
  final String tinNumber;
  @override
  final String updatedAt;
  @override
  final String createdAt;
  @override
  final String latitude;
  @override
  final String longitude;

  factory _$UserPractice([void Function(UserPracticeBuilder) updates]) =>
      (new UserPracticeBuilder()..update(updates)).build();

  _$UserPractice._(
      {this.id,
      this.label,
      this.address1,
      this.address2,
      this.city,
      this.state,
      this.zipCode,
      this.officeHours,
      this.phone,
      this.emrPlatform,
      this.website,
      this.tinNumber,
      this.updatedAt,
      this.createdAt,
      this.latitude,
      this.longitude})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('UserPractice', 'id');
    }
    if (label == null) {
      throw new BuiltValueNullFieldError('UserPractice', 'label');
    }
  }

  @override
  UserPractice rebuild(void Function(UserPracticeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserPracticeBuilder toBuilder() => new UserPracticeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserPractice &&
        id == other.id &&
        label == other.label &&
        address1 == other.address1 &&
        address2 == other.address2 &&
        city == other.city &&
        state == other.state &&
        zipCode == other.zipCode &&
        officeHours == other.officeHours &&
        phone == other.phone &&
        emrPlatform == other.emrPlatform &&
        website == other.website &&
        tinNumber == other.tinNumber &&
        updatedAt == other.updatedAt &&
        createdAt == other.createdAt &&
        latitude == other.latitude &&
        longitude == other.longitude;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(0,
                                                                    id.hashCode),
                                                                label.hashCode),
                                                            address1.hashCode),
                                                        address2.hashCode),
                                                    city.hashCode),
                                                state.hashCode),
                                            zipCode.hashCode),
                                        officeHours.hashCode),
                                    phone.hashCode),
                                emrPlatform.hashCode),
                            website.hashCode),
                        tinNumber.hashCode),
                    updatedAt.hashCode),
                createdAt.hashCode),
            latitude.hashCode),
        longitude.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserPractice')
          ..add('id', id)
          ..add('label', label)
          ..add('address1', address1)
          ..add('address2', address2)
          ..add('city', city)
          ..add('state', state)
          ..add('zipCode', zipCode)
          ..add('officeHours', officeHours)
          ..add('phone', phone)
          ..add('emrPlatform', emrPlatform)
          ..add('website', website)
          ..add('tinNumber', tinNumber)
          ..add('updatedAt', updatedAt)
          ..add('createdAt', createdAt)
          ..add('latitude', latitude)
          ..add('longitude', longitude))
        .toString();
  }
}

class UserPracticeBuilder
    implements Builder<UserPractice, UserPracticeBuilder> {
  _$UserPractice _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _label;
  String get label => _$this._label;
  set label(String label) => _$this._label = label;

  String _address1;
  String get address1 => _$this._address1;
  set address1(String address1) => _$this._address1 = address1;

  String _address2;
  String get address2 => _$this._address2;
  set address2(String address2) => _$this._address2 = address2;

  String _city;
  String get city => _$this._city;
  set city(String city) => _$this._city = city;

  String _state;
  String get state => _$this._state;
  set state(String state) => _$this._state = state;

  String _zipCode;
  String get zipCode => _$this._zipCode;
  set zipCode(String zipCode) => _$this._zipCode = zipCode;

  String _officeHours;
  String get officeHours => _$this._officeHours;
  set officeHours(String officeHours) => _$this._officeHours = officeHours;

  String _phone;
  String get phone => _$this._phone;
  set phone(String phone) => _$this._phone = phone;

  String _emrPlatform;
  String get emrPlatform => _$this._emrPlatform;
  set emrPlatform(String emrPlatform) => _$this._emrPlatform = emrPlatform;

  String _website;
  String get website => _$this._website;
  set website(String website) => _$this._website = website;

  String _tinNumber;
  String get tinNumber => _$this._tinNumber;
  set tinNumber(String tinNumber) => _$this._tinNumber = tinNumber;

  String _updatedAt;
  String get updatedAt => _$this._updatedAt;
  set updatedAt(String updatedAt) => _$this._updatedAt = updatedAt;

  String _createdAt;
  String get createdAt => _$this._createdAt;
  set createdAt(String createdAt) => _$this._createdAt = createdAt;

  String _latitude;
  String get latitude => _$this._latitude;
  set latitude(String latitude) => _$this._latitude = latitude;

  String _longitude;
  String get longitude => _$this._longitude;
  set longitude(String longitude) => _$this._longitude = longitude;

  UserPracticeBuilder();

  UserPracticeBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _label = _$v.label;
      _address1 = _$v.address1;
      _address2 = _$v.address2;
      _city = _$v.city;
      _state = _$v.state;
      _zipCode = _$v.zipCode;
      _officeHours = _$v.officeHours;
      _phone = _$v.phone;
      _emrPlatform = _$v.emrPlatform;
      _website = _$v.website;
      _tinNumber = _$v.tinNumber;
      _updatedAt = _$v.updatedAt;
      _createdAt = _$v.createdAt;
      _latitude = _$v.latitude;
      _longitude = _$v.longitude;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserPractice other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserPractice;
  }

  @override
  void update(void Function(UserPracticeBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserPractice build() {
    final _$result = _$v ??
        new _$UserPractice._(
            id: id,
            label: label,
            address1: address1,
            address2: address2,
            city: city,
            state: state,
            zipCode: zipCode,
            officeHours: officeHours,
            phone: phone,
            emrPlatform: emrPlatform,
            website: website,
            tinNumber: tinNumber,
            updatedAt: updatedAt,
            createdAt: createdAt,
            latitude: latitude,
            longitude: longitude);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
