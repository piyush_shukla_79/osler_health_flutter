import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'reminder_notification.g.dart';

abstract class ReminderNotification
    implements Built<ReminderNotification, ReminderNotificationBuilder> {
  int get hour;

  int get minute;

  bool get enabled;

  @nullable
  int get date;

  ReminderNotification._();

  factory ReminderNotification([updates(ReminderNotificationBuilder b)]) =
      _$ReminderNotification;

  static Serializer<ReminderNotification> get serializer =>
      _$reminderNotificationSerializer;

  Map<String, dynamic> toMap() {
    return {"hour": hour, "minute": minute, "enabled": enabled, "date": date};
  }
}
