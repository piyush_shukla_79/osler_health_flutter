// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_doctor.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserDoctor> _$userDoctorSerializer = new _$UserDoctorSerializer();

class _$UserDoctorSerializer implements StructuredSerializer<UserDoctor> {
  @override
  final Iterable<Type> types = const [UserDoctor, _$UserDoctor];
  @override
  final String wireName = 'UserDoctor';

  @override
  Iterable serialize(Serializers serializers, UserDoctor object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.firstName != null) {
      result
        ..add('first_name')
        ..add(serializers.serialize(object.firstName,
            specifiedType: const FullType(String)));
    }
    if (object.lastName != null) {
      result
        ..add('last_name')
        ..add(serializers.serialize(object.lastName,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.phone != null) {
      result
        ..add('phone')
        ..add(serializers.serialize(object.phone,
            specifiedType: const FullType(String)));
    }
    if (object.address != null) {
      result
        ..add('address')
        ..add(serializers.serialize(object.address,
            specifiedType: const FullType(String)));
    }
    if (object.specialtyId != null) {
      result
        ..add('specialty_id')
        ..add(serializers.serialize(object.specialtyId,
            specifiedType: const FullType(int)));
    }
    if (object.practiceId != null) {
      result
        ..add('practice_id')
        ..add(serializers.serialize(object.practiceId,
            specifiedType: const FullType(int)));
    }
    if (object.dob != null) {
      result
        ..add('date_of_birth')
        ..add(serializers.serialize(object.dob,
            specifiedType: const FullType(String)));
    }
    if (object.updatedAt != null) {
      result
        ..add('updated_at')
        ..add(serializers.serialize(object.updatedAt,
            specifiedType: const FullType(String)));
    }
    if (object.createdAt != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  UserDoctor deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserDoctorBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'first_name':
          result.firstName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'last_name':
          result.lastName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'phone':
          result.phone = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'address':
          result.address = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'specialty_id':
          result.specialtyId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'practice_id':
          result.practiceId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'date_of_birth':
          result.dob = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updated_at':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserDoctor extends UserDoctor {
  @override
  final int id;
  @override
  final String firstName;
  @override
  final String lastName;
  @override
  final String email;
  @override
  final String phone;
  @override
  final String address;
  @override
  final int specialtyId;
  @override
  final int practiceId;
  @override
  final String dob;
  @override
  final String updatedAt;
  @override
  final String createdAt;

  factory _$UserDoctor([void updates(UserDoctorBuilder b)]) =>
      (new UserDoctorBuilder()..update(updates)).build();

  _$UserDoctor._(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.phone,
      this.address,
      this.specialtyId,
      this.practiceId,
      this.dob,
      this.updatedAt,
      this.createdAt})
      : super._();

  @override
  UserDoctor rebuild(void updates(UserDoctorBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserDoctorBuilder toBuilder() => new UserDoctorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserDoctor &&
        id == other.id &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        email == other.email &&
        phone == other.phone &&
        address == other.address &&
        specialtyId == other.specialtyId &&
        practiceId == other.practiceId &&
        dob == other.dob &&
        updatedAt == other.updatedAt &&
        createdAt == other.createdAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc($jc(0, id.hashCode),
                                            firstName.hashCode),
                                        lastName.hashCode),
                                    email.hashCode),
                                phone.hashCode),
                            address.hashCode),
                        specialtyId.hashCode),
                    practiceId.hashCode),
                dob.hashCode),
            updatedAt.hashCode),
        createdAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserDoctor')
          ..add('id', id)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('email', email)
          ..add('phone', phone)
          ..add('address', address)
          ..add('specialtyId', specialtyId)
          ..add('practiceId', practiceId)
          ..add('dob', dob)
          ..add('updatedAt', updatedAt)
          ..add('createdAt', createdAt))
        .toString();
  }
}

class UserDoctorBuilder implements Builder<UserDoctor, UserDoctorBuilder> {
  _$UserDoctor _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _firstName;
  String get firstName => _$this._firstName;
  set firstName(String firstName) => _$this._firstName = firstName;

  String _lastName;
  String get lastName => _$this._lastName;
  set lastName(String lastName) => _$this._lastName = lastName;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _phone;
  String get phone => _$this._phone;
  set phone(String phone) => _$this._phone = phone;

  String _address;
  String get address => _$this._address;
  set address(String address) => _$this._address = address;

  int _specialtyId;
  int get specialtyId => _$this._specialtyId;
  set specialtyId(int specialtyId) => _$this._specialtyId = specialtyId;

  int _practiceId;
  int get practiceId => _$this._practiceId;
  set practiceId(int practiceId) => _$this._practiceId = practiceId;

  String _dob;
  String get dob => _$this._dob;
  set dob(String dob) => _$this._dob = dob;

  String _updatedAt;
  String get updatedAt => _$this._updatedAt;
  set updatedAt(String updatedAt) => _$this._updatedAt = updatedAt;

  String _createdAt;
  String get createdAt => _$this._createdAt;
  set createdAt(String createdAt) => _$this._createdAt = createdAt;

  UserDoctorBuilder();

  UserDoctorBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _firstName = _$v.firstName;
      _lastName = _$v.lastName;
      _email = _$v.email;
      _phone = _$v.phone;
      _address = _$v.address;
      _specialtyId = _$v.specialtyId;
      _practiceId = _$v.practiceId;
      _dob = _$v.dob;
      _updatedAt = _$v.updatedAt;
      _createdAt = _$v.createdAt;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserDoctor other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserDoctor;
  }

  @override
  void update(void updates(UserDoctorBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserDoctor build() {
    final _$result = _$v ??
        new _$UserDoctor._(
            id: id,
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone: phone,
            address: address,
            specialtyId: specialtyId,
            practiceId: practiceId,
            dob: dob,
            updatedAt: updatedAt,
            createdAt: createdAt);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
