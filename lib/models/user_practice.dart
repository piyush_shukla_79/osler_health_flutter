import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_practice.g.dart';

abstract class UserPractice
    implements Built<UserPractice, UserPracticeBuilder> {
  int get id;

  String get label;

  @nullable
  String get address1;

  @nullable
  String get address2;

  @nullable
  String get city;

  @nullable
  String get state;

  @nullable
  @BuiltValueField(wireName: "zip_code")
  String get zipCode;

  @nullable
  @BuiltValueField(wireName: "office_hours")
  String get officeHours;

  @nullable
  String get phone;

  @nullable
  @BuiltValueField(wireName: "emr_platform")
  String get emrPlatform;

  @nullable
  String get website;

  @nullable
  @BuiltValueField(wireName: "tin_number")
  String get tinNumber;

  @nullable
  @BuiltValueField(wireName: "updated_at")
  String get updatedAt;

  @nullable
  @BuiltValueField(wireName: "created_at")
  String get createdAt;

  @nullable
  String get latitude;

  @nullable
  String get longitude;

  UserPractice._();

  factory UserPractice([updates(UserPracticeBuilder b)]) = _$UserPractice;

  static Serializer<UserPractice> get serializer => _$userPracticeSerializer;
}
