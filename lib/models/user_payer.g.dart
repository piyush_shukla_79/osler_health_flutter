// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_payer.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserPayer> _$userPayerSerializer = new _$UserPayerSerializer();

class _$UserPayerSerializer implements StructuredSerializer<UserPayer> {
  @override
  final Iterable<Type> types = const [UserPayer, _$UserPayer];
  @override
  final String wireName = 'UserPayer';

  @override
  Iterable serialize(Serializers serializers, UserPayer object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.label != null) {
      result
        ..add('label')
        ..add(serializers.serialize(object.label,
            specifiedType: const FullType(String)));
    }
    if (object.logo != null) {
      result
        ..add('logo')
        ..add(serializers.serialize(object.logo,
            specifiedType: const FullType(String)));
    }
    if (object.levels != null) {
      result
        ..add('levels')
        ..add(serializers.serialize(object.levels,
            specifiedType:
                const FullType(BuiltList, const [const FullType(int)])));
    }
    if (object.updatedAt != null) {
      result
        ..add('updated_at')
        ..add(serializers.serialize(object.updatedAt,
            specifiedType: const FullType(String)));
    }
    if (object.createdAt != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  UserPayer deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserPayerBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'label':
          result.label = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'logo':
          result.logo = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'levels':
          result.levels.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(int)]))
              as BuiltList);
          break;
        case 'updated_at':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserPayer extends UserPayer {
  @override
  final int id;
  @override
  final String label;
  @override
  final String logo;
  @override
  final BuiltList<int> levels;
  @override
  final String updatedAt;
  @override
  final String createdAt;

  factory _$UserPayer([void updates(UserPayerBuilder b)]) =>
      (new UserPayerBuilder()..update(updates)).build();

  _$UserPayer._(
      {this.id,
      this.label,
      this.logo,
      this.levels,
      this.updatedAt,
      this.createdAt})
      : super._();

  @override
  UserPayer rebuild(void updates(UserPayerBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserPayerBuilder toBuilder() => new UserPayerBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserPayer &&
        id == other.id &&
        label == other.label &&
        logo == other.logo &&
        levels == other.levels &&
        updatedAt == other.updatedAt &&
        createdAt == other.createdAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc($jc(0, id.hashCode), label.hashCode), logo.hashCode),
                levels.hashCode),
            updatedAt.hashCode),
        createdAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserPayer')
          ..add('id', id)
          ..add('label', label)
          ..add('logo', logo)
          ..add('levels', levels)
          ..add('updatedAt', updatedAt)
          ..add('createdAt', createdAt))
        .toString();
  }
}

class UserPayerBuilder implements Builder<UserPayer, UserPayerBuilder> {
  _$UserPayer _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _label;
  String get label => _$this._label;
  set label(String label) => _$this._label = label;

  String _logo;
  String get logo => _$this._logo;
  set logo(String logo) => _$this._logo = logo;

  ListBuilder<int> _levels;
  ListBuilder<int> get levels => _$this._levels ??= new ListBuilder<int>();
  set levels(ListBuilder<int> levels) => _$this._levels = levels;

  String _updatedAt;
  String get updatedAt => _$this._updatedAt;
  set updatedAt(String updatedAt) => _$this._updatedAt = updatedAt;

  String _createdAt;
  String get createdAt => _$this._createdAt;
  set createdAt(String createdAt) => _$this._createdAt = createdAt;

  UserPayerBuilder();

  UserPayerBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _label = _$v.label;
      _logo = _$v.logo;
      _levels = _$v.levels?.toBuilder();
      _updatedAt = _$v.updatedAt;
      _createdAt = _$v.createdAt;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserPayer other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserPayer;
  }

  @override
  void update(void updates(UserPayerBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserPayer build() {
    _$UserPayer _$result;
    try {
      _$result = _$v ??
          new _$UserPayer._(
              id: id,
              label: label,
              logo: logo,
              levels: _levels?.build(),
              updatedAt: updatedAt,
              createdAt: createdAt);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'levels';
        _levels?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserPayer', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
