// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_notes.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserNotes> _$userNotesSerializer = new _$UserNotesSerializer();

class _$UserNotesSerializer implements StructuredSerializer<UserNotes> {
  @override
  final Iterable<Type> types = const [UserNotes, _$UserNotes];
  @override
  final String wireName = 'UserNotes';

  @override
  Iterable serialize(Serializers serializers, UserNotes object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.patientId != null) {
      result
        ..add('patient_id')
        ..add(serializers.serialize(object.patientId,
            specifiedType: const FullType(int)));
    }
    if (object.userId != null) {
      result
        ..add('user_id')
        ..add(serializers.serialize(object.userId,
            specifiedType: const FullType(int)));
    }
    if (object.notes != null) {
      result
        ..add('notes')
        ..add(serializers.serialize(object.notes,
            specifiedType: const FullType(String)));
    }
    if (object.date != null) {
      result
        ..add('date')
        ..add(serializers.serialize(object.date,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  UserNotes deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserNotesBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'patient_id':
          result.patientId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'user_id':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'notes':
          result.notes = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'date':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserNotes extends UserNotes {
  @override
  final int id;
  @override
  final int patientId;
  @override
  final int userId;
  @override
  final String notes;
  @override
  final String date;

  factory _$UserNotes([void updates(UserNotesBuilder b)]) =>
      (new UserNotesBuilder()..update(updates)).build();

  _$UserNotes._({this.id, this.patientId, this.userId, this.notes, this.date})
      : super._();

  @override
  UserNotes rebuild(void updates(UserNotesBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserNotesBuilder toBuilder() => new UserNotesBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserNotes &&
        id == other.id &&
        patientId == other.patientId &&
        userId == other.userId &&
        notes == other.notes &&
        date == other.date;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), patientId.hashCode), userId.hashCode),
            notes.hashCode),
        date.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserNotes')
          ..add('id', id)
          ..add('patientId', patientId)
          ..add('userId', userId)
          ..add('notes', notes)
          ..add('date', date))
        .toString();
  }
}

class UserNotesBuilder implements Builder<UserNotes, UserNotesBuilder> {
  _$UserNotes _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  int _patientId;
  int get patientId => _$this._patientId;
  set patientId(int patientId) => _$this._patientId = patientId;

  int _userId;
  int get userId => _$this._userId;
  set userId(int userId) => _$this._userId = userId;

  String _notes;
  String get notes => _$this._notes;
  set notes(String notes) => _$this._notes = notes;

  String _date;
  String get date => _$this._date;
  set date(String date) => _$this._date = date;

  UserNotesBuilder();

  UserNotesBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _patientId = _$v.patientId;
      _userId = _$v.userId;
      _notes = _$v.notes;
      _date = _$v.date;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserNotes other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserNotes;
  }

  @override
  void update(void updates(UserNotesBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserNotes build() {
    final _$result = _$v ??
        new _$UserNotes._(
            id: id,
            patientId: patientId,
            userId: userId,
            notes: notes,
            date: date);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
