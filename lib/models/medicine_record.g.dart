// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medicine_record.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MedicineRecord> _$medicineRecordSerializer =
    new _$MedicineRecordSerializer();

class _$MedicineRecordSerializer
    implements StructuredSerializer<MedicineRecord> {
  @override
  final Iterable<Type> types = const [MedicineRecord, _$MedicineRecord];
  @override
  final String wireName = 'MedicineRecord';

  @override
  Iterable serialize(Serializers serializers, MedicineRecord object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.medicineName != null) {
      result
        ..add('medicineName')
        ..add(serializers.serialize(object.medicineName,
            specifiedType: const FullType(String)));
    }
    if (object.active != null) {
      result
        ..add('active')
        ..add(serializers.serialize(object.active,
            specifiedType: const FullType(bool)));
    }

    return result;
  }

  @override
  MedicineRecord deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MedicineRecordBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'medicineName':
          result.medicineName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'active':
          result.active = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$MedicineRecord extends MedicineRecord {
  @override
  final int id;
  @override
  final String medicineName;
  @override
  final bool active;

  factory _$MedicineRecord([void updates(MedicineRecordBuilder b)]) =>
      (new MedicineRecordBuilder()..update(updates)).build();

  _$MedicineRecord._({this.id, this.medicineName, this.active}) : super._();

  @override
  MedicineRecord rebuild(void updates(MedicineRecordBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  MedicineRecordBuilder toBuilder() =>
      new MedicineRecordBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MedicineRecord &&
        id == other.id &&
        medicineName == other.medicineName &&
        active == other.active;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, id.hashCode), medicineName.hashCode), active.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MedicineRecord')
          ..add('id', id)
          ..add('medicineName', medicineName)
          ..add('active', active))
        .toString();
  }
}

class MedicineRecordBuilder
    implements Builder<MedicineRecord, MedicineRecordBuilder> {
  _$MedicineRecord _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _medicineName;
  String get medicineName => _$this._medicineName;
  set medicineName(String medicineName) => _$this._medicineName = medicineName;

  bool _active;
  bool get active => _$this._active;
  set active(bool active) => _$this._active = active;

  MedicineRecordBuilder();

  MedicineRecordBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _medicineName = _$v.medicineName;
      _active = _$v.active;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MedicineRecord other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MedicineRecord;
  }

  @override
  void update(void updates(MedicineRecordBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$MedicineRecord build() {
    final _$result = _$v ??
        new _$MedicineRecord._(
            id: id, medicineName: medicineName, active: active);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
