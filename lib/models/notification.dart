import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:osler_health/models/mixins/base_model.dart';

part 'notification.g.dart';

abstract class AppNotification
    with BaseModel
    implements Built<AppNotification, AppNotificationBuilder> {
  int get id;

  String get title;

  @BuiltValueField(wireName: "patient_id")
  int get patientId;

  bool get read;

  @BuiltValueField(wireName: "notification_type")
  @nullable
  String get type;

  @nullable
  String get body;

  @BuiltValueField(wireName: "created_at")
  String get createdAt;

  @BuiltValueField(wireName: "updated_at")
  String get updatedAt;

  @nullable
  @BuiltValueField(wireName: "start_date")
  String get startDate;

  @nullable
  @BuiltValueField(wireName: "end_date")
  String get endDate;

  AppNotification._();

  factory AppNotification([updates(AppNotificationBuilder b)]) =
      _$AppNotification;

  static Serializer<AppNotification> get serializer =>
      _$appNotificationSerializer;
}
