// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chronic_diseases.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ChronicDiseases> _$chronicDiseasesSerializer =
    new _$ChronicDiseasesSerializer();

class _$ChronicDiseasesSerializer
    implements StructuredSerializer<ChronicDiseases> {
  @override
  final Iterable<Type> types = const [ChronicDiseases, _$ChronicDiseases];
  @override
  final String wireName = 'ChronicDiseases';

  @override
  Iterable serialize(Serializers serializers, ChronicDiseases object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'subchronic_diseases',
      serializers.serialize(object.subchronicDiseases,
          specifiedType: const FullType(
              BuiltList, const [const FullType(ChronicDiseases)])),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'label',
      serializers.serialize(object.label,
          specifiedType: const FullType(String)),
    ];
    if (object.diagnosisDate != null) {
      result
        ..add('diagnosis_date')
        ..add(serializers.serialize(object.diagnosisDate,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  ChronicDiseases deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ChronicDiseasesBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'subchronic_diseases':
          result.subchronicDiseases.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ChronicDiseases)]))
              as BuiltList);
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'label':
          result.label = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'diagnosis_date':
          result.diagnosisDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ChronicDiseases extends ChronicDiseases {
  @override
  final BuiltList<ChronicDiseases> subchronicDiseases;
  @override
  final int id;
  @override
  final String label;
  @override
  final String diagnosisDate;

  factory _$ChronicDiseases([void Function(ChronicDiseasesBuilder) updates]) =>
      (new ChronicDiseasesBuilder()..update(updates)).build();

  _$ChronicDiseases._(
      {this.subchronicDiseases, this.id, this.label, this.diagnosisDate})
      : super._() {
    if (subchronicDiseases == null) {
      throw new BuiltValueNullFieldError(
          'ChronicDiseases', 'subchronicDiseases');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('ChronicDiseases', 'id');
    }
    if (label == null) {
      throw new BuiltValueNullFieldError('ChronicDiseases', 'label');
    }
  }

  @override
  ChronicDiseases rebuild(void Function(ChronicDiseasesBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChronicDiseasesBuilder toBuilder() =>
      new ChronicDiseasesBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChronicDiseases &&
        subchronicDiseases == other.subchronicDiseases &&
        id == other.id &&
        label == other.label &&
        diagnosisDate == other.diagnosisDate;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, subchronicDiseases.hashCode), id.hashCode),
            label.hashCode),
        diagnosisDate.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ChronicDiseases')
          ..add('subchronicDiseases', subchronicDiseases)
          ..add('id', id)
          ..add('label', label)
          ..add('diagnosisDate', diagnosisDate))
        .toString();
  }
}

class ChronicDiseasesBuilder
    implements Builder<ChronicDiseases, ChronicDiseasesBuilder> {
  _$ChronicDiseases _$v;

  ListBuilder<ChronicDiseases> _subchronicDiseases;
  ListBuilder<ChronicDiseases> get subchronicDiseases =>
      _$this._subchronicDiseases ??= new ListBuilder<ChronicDiseases>();
  set subchronicDiseases(ListBuilder<ChronicDiseases> subchronicDiseases) =>
      _$this._subchronicDiseases = subchronicDiseases;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _label;
  String get label => _$this._label;
  set label(String label) => _$this._label = label;

  String _diagnosisDate;
  String get diagnosisDate => _$this._diagnosisDate;
  set diagnosisDate(String diagnosisDate) =>
      _$this._diagnosisDate = diagnosisDate;

  ChronicDiseasesBuilder();

  ChronicDiseasesBuilder get _$this {
    if (_$v != null) {
      _subchronicDiseases = _$v.subchronicDiseases?.toBuilder();
      _id = _$v.id;
      _label = _$v.label;
      _diagnosisDate = _$v.diagnosisDate;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ChronicDiseases other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ChronicDiseases;
  }

  @override
  void update(void Function(ChronicDiseasesBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChronicDiseases build() {
    _$ChronicDiseases _$result;
    try {
      _$result = _$v ??
          new _$ChronicDiseases._(
              subchronicDiseases: subchronicDiseases.build(),
              id: id,
              label: label,
              diagnosisDate: diagnosisDate);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'subchronicDiseases';
        subchronicDiseases.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ChronicDiseases', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
