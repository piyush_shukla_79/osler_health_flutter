// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setting.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Setting> _$settingSerializer = new _$SettingSerializer();

class _$SettingSerializer implements StructuredSerializer<Setting> {
  @override
  final Iterable<Type> types = const [Setting, _$Setting];
  @override
  final String wireName = 'Setting';

  @override
  Iterable serialize(Serializers serializers, Setting object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(bool)));
    }
    if (object.notification != null) {
      result
        ..add('notification')
        ..add(serializers.serialize(object.notification,
            specifiedType: const FullType(bool)));
    }
    if (object.messages != null) {
      result
        ..add('messages')
        ..add(serializers.serialize(object.messages,
            specifiedType: const FullType(bool)));
    }

    return result;
  }

  @override
  Setting deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SettingBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'notification':
          result.notification = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'messages':
          result.messages = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$Setting extends Setting {
  @override
  final bool email;
  @override
  final bool notification;
  @override
  final bool messages;

  factory _$Setting([void updates(SettingBuilder b)]) =>
      (new SettingBuilder()..update(updates)).build();

  _$Setting._({this.email, this.notification, this.messages}) : super._();

  @override
  Setting rebuild(void updates(SettingBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SettingBuilder toBuilder() => new SettingBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Setting &&
        email == other.email &&
        notification == other.notification &&
        messages == other.messages;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, email.hashCode), notification.hashCode), messages.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Setting')
          ..add('email', email)
          ..add('notification', notification)
          ..add('messages', messages))
        .toString();
  }
}

class SettingBuilder implements Builder<Setting, SettingBuilder> {
  _$Setting _$v;

  bool _email;
  bool get email => _$this._email;
  set email(bool email) => _$this._email = email;

  bool _notification;
  bool get notification => _$this._notification;
  set notification(bool notification) => _$this._notification = notification;

  bool _messages;
  bool get messages => _$this._messages;
  set messages(bool messages) => _$this._messages = messages;

  SettingBuilder();

  SettingBuilder get _$this {
    if (_$v != null) {
      _email = _$v.email;
      _notification = _$v.notification;
      _messages = _$v.messages;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Setting other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Setting;
  }

  @override
  void update(void updates(SettingBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Setting build() {
    final _$result = _$v ??
        new _$Setting._(
            email: email, notification: notification, messages: messages);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
