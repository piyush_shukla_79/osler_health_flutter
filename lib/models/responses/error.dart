import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'error.g.dart';

abstract class NetworkError
    implements Built<NetworkError, NetworkErrorBuilder> {
  String get message;

  @nullable
  @BuiltValueField(wireName: "status_code")
  int get statusCode;

//   @nullable
//  BuiltMap<String, BuiltList<String>> get errors;

  NetworkError._();

//  factory NetworkError(){
//    return _$NetworkError((b) =>
//    b
//      ..message = ""
//      ..statusCode = 0
//      ..errors = MapBuilder<String, BuiltList<String>>()
//    );
//  }

  factory NetworkError([updates(NetworkErrorBuilder b)]) = _$NetworkError;

  static Serializer<NetworkError> get serializer => _$networkErrorSerializer;
}