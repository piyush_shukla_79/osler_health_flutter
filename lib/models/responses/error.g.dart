// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'error.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<NetworkError> _$networkErrorSerializer =
    new _$NetworkErrorSerializer();

class _$NetworkErrorSerializer implements StructuredSerializer<NetworkError> {
  @override
  final Iterable<Type> types = const [NetworkError, _$NetworkError];
  @override
  final String wireName = 'NetworkError';

  @override
  Iterable serialize(Serializers serializers, NetworkError object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];
    if (object.statusCode != null) {
      result
        ..add('status_code')
        ..add(serializers.serialize(object.statusCode,
            specifiedType: const FullType(int)));
    }

    return result;
  }

  @override
  NetworkError deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new NetworkErrorBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status_code':
          result.statusCode = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$NetworkError extends NetworkError {
  @override
  final String message;
  @override
  final int statusCode;

  factory _$NetworkError([void updates(NetworkErrorBuilder b)]) =>
      (new NetworkErrorBuilder()..update(updates)).build();

  _$NetworkError._({this.message, this.statusCode}) : super._() {
    if (message == null) {
      throw new BuiltValueNullFieldError('NetworkError', 'message');
    }
  }

  @override
  NetworkError rebuild(void updates(NetworkErrorBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  NetworkErrorBuilder toBuilder() => new NetworkErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is NetworkError &&
        message == other.message &&
        statusCode == other.statusCode;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, message.hashCode), statusCode.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('NetworkError')
          ..add('message', message)
          ..add('statusCode', statusCode))
        .toString();
  }
}

class NetworkErrorBuilder
    implements Builder<NetworkError, NetworkErrorBuilder> {
  _$NetworkError _$v;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  int _statusCode;
  int get statusCode => _$this._statusCode;
  set statusCode(int statusCode) => _$this._statusCode = statusCode;

  NetworkErrorBuilder();

  NetworkErrorBuilder get _$this {
    if (_$v != null) {
      _message = _$v.message;
      _statusCode = _$v.statusCode;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(NetworkError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$NetworkError;
  }

  @override
  void update(void updates(NetworkErrorBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$NetworkError build() {
    final _$result =
        _$v ?? new _$NetworkError._(message: message, statusCode: statusCode);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
