// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResponseData> _$responseDataSerializer =
    new _$ResponseDataSerializer();
Serializer<ResponseDataList> _$responseDataListSerializer =
    new _$ResponseDataListSerializer();

class _$ResponseDataSerializer implements StructuredSerializer<ResponseData> {
  @override
  final Iterable<Type> types = const [ResponseData, _$ResponseData];
  @override
  final String wireName = 'ResponseData';

  @override
  Iterable serialize(Serializers serializers, ResponseData object,
      {FullType specifiedType = FullType.unspecified}) {
    final isUnderspecified =
        specifiedType.isUnspecified || specifiedType.parameters.isEmpty;
    if (!isUnderspecified) serializers.expectBuilder(specifiedType);
    final parameterT =
        isUnderspecified ? FullType.object : specifiedType.parameters[0];

    final result = <Object>[
      'data',
      serializers.serialize(object.data, specifiedType: parameterT),
    ];

    return result;
  }

  @override
  ResponseData deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final isUnderspecified =
        specifiedType.isUnspecified || specifiedType.parameters.isEmpty;
    if (!isUnderspecified) serializers.expectBuilder(specifiedType);
    final parameterT =
        isUnderspecified ? FullType.object : specifiedType.parameters[0];

    final result = isUnderspecified
        ? new ResponseDataBuilder<Object>()
        : serializers.newBuilder(specifiedType) as ResponseDataBuilder;

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'data':
          result.data =
              serializers.deserialize(value, specifiedType: parameterT);
          break;
      }
    }

    return result.build();
  }
}

class _$ResponseDataListSerializer
    implements StructuredSerializer<ResponseDataList> {
  @override
  final Iterable<Type> types = const [ResponseDataList, _$ResponseDataList];
  @override
  final String wireName = 'ResponseDataList';

  @override
  Iterable serialize(Serializers serializers, ResponseDataList object,
      {FullType specifiedType = FullType.unspecified}) {
    final isUnderspecified =
        specifiedType.isUnspecified || specifiedType.parameters.isEmpty;
    if (!isUnderspecified) serializers.expectBuilder(specifiedType);
    final parameterT =
        isUnderspecified ? FullType.object : specifiedType.parameters[0];

    final result = <Object>[
      'data',
      serializers.serialize(object.data,
          specifiedType: new FullType(BuiltList, [parameterT])),
    ];

    return result;
  }

  @override
  ResponseDataList deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final isUnderspecified =
        specifiedType.isUnspecified || specifiedType.parameters.isEmpty;
    if (!isUnderspecified) serializers.expectBuilder(specifiedType);
    final parameterT =
        isUnderspecified ? FullType.object : specifiedType.parameters[0];

    final result = isUnderspecified
        ? new ResponseDataListBuilder<Object>()
        : serializers.newBuilder(specifiedType) as ResponseDataListBuilder;

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'data':
          result.data.replace(serializers.deserialize(value,
                  specifiedType: new FullType(BuiltList, [parameterT]))
              as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$ResponseData<T> extends ResponseData<T> {
  @override
  final T data;

  factory _$ResponseData([void updates(ResponseDataBuilder<T> b)]) =>
      (new ResponseDataBuilder<T>()..update(updates)).build();

  _$ResponseData._({this.data}) : super._() {
    if (data == null) {
      throw new BuiltValueNullFieldError('ResponseData', 'data');
    }
    if (T == dynamic) {
      throw new BuiltValueMissingGenericsError('ResponseData', 'T');
    }
  }

  @override
  ResponseData<T> rebuild(void updates(ResponseDataBuilder<T> b)) =>
      (toBuilder()..update(updates)).build();

  @override
  ResponseDataBuilder<T> toBuilder() =>
      new ResponseDataBuilder<T>()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResponseData && data == other.data;
  }

  @override
  int get hashCode {
    return $jf($jc(0, data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResponseData')..add('data', data))
        .toString();
  }
}

class ResponseDataBuilder<T>
    implements Builder<ResponseData<T>, ResponseDataBuilder<T>> {
  _$ResponseData<T> _$v;

  T _data;
  T get data => _$this._data;
  set data(T data) => _$this._data = data;

  ResponseDataBuilder();

  ResponseDataBuilder<T> get _$this {
    if (_$v != null) {
      _data = _$v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResponseData<T> other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResponseData<T>;
  }

  @override
  void update(void updates(ResponseDataBuilder<T> b)) {
    if (updates != null) updates(this);
  }

  @override
  _$ResponseData<T> build() {
    final _$result = _$v ?? new _$ResponseData<T>._(data: data);
    replace(_$result);
    return _$result;
  }
}

class _$ResponseDataList<T> extends ResponseDataList<T> {
  @override
  final BuiltList<T> data;

  factory _$ResponseDataList([void updates(ResponseDataListBuilder<T> b)]) =>
      (new ResponseDataListBuilder<T>()..update(updates)).build();

  _$ResponseDataList._({this.data}) : super._() {
    if (data == null) {
      throw new BuiltValueNullFieldError('ResponseDataList', 'data');
    }
    if (T == dynamic) {
      throw new BuiltValueMissingGenericsError('ResponseDataList', 'T');
    }
  }

  @override
  ResponseDataList<T> rebuild(void updates(ResponseDataListBuilder<T> b)) =>
      (toBuilder()..update(updates)).build();

  @override
  ResponseDataListBuilder<T> toBuilder() =>
      new ResponseDataListBuilder<T>()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResponseDataList && data == other.data;
  }

  @override
  int get hashCode {
    return $jf($jc(0, data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResponseDataList')..add('data', data))
        .toString();
  }
}

class ResponseDataListBuilder<T>
    implements Builder<ResponseDataList<T>, ResponseDataListBuilder<T>> {
  _$ResponseDataList<T> _$v;

  ListBuilder<T> _data;
  ListBuilder<T> get data => _$this._data ??= new ListBuilder<T>();
  set data(ListBuilder<T> data) => _$this._data = data;

  ResponseDataListBuilder();

  ResponseDataListBuilder<T> get _$this {
    if (_$v != null) {
      _data = _$v.data?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResponseDataList<T> other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResponseDataList<T>;
  }

  @override
  void update(void updates(ResponseDataListBuilder<T> b)) {
    if (updates != null) updates(this);
  }

  @override
  _$ResponseDataList<T> build() {
    _$ResponseDataList<T> _$result;
    try {
      _$result = _$v ?? new _$ResponseDataList<T>._(data: data.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'data';
        data.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ResponseDataList', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
