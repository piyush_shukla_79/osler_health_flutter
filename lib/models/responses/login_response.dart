import 'package:osler_health/models/user.dart';

class LoginResponse {
  String token;
  User user;

  LoginResponse(this.token, this.user);
}
