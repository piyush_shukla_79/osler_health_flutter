import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'response_data.g.dart';

abstract class ResponseData<T>
    implements Built<ResponseData<T>, ResponseDataBuilder<T>> {
  T get data;

  ResponseData._();

  factory ResponseData([updates(ResponseDataBuilder<T> b)]) = _$ResponseData<T>;

  static Serializer<ResponseData> get serializer => _$responseDataSerializer;
}

abstract class ResponseDataList<T>
    implements Built<ResponseDataList<T>, ResponseDataListBuilder<T>> {
  BuiltList<T> get data;

  ResponseDataList._();

  factory ResponseDataList([updates(ResponseDataListBuilder<T> b)]) =
      _$ResponseDataList<T>;

  static Serializer<ResponseDataList> get serializer =>
      _$responseDataListSerializer;
}
