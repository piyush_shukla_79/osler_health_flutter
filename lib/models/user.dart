import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:osler_health/app_config.dart';
import 'package:osler_health/models/chronic_diseases.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/responses/response_data.dart';
import 'package:osler_health/models/user_doctor.dart';
import 'package:osler_health/models/user_notes.dart';
import 'package:osler_health/models/user_payer.dart';
import 'package:osler_health/models/user_practice.dart';

part 'user.g.dart';

abstract class User implements Built<User, UserBuilder> {
  static Serializer<User> get serializer => _$userSerializer;

  int get id;

  @nullable
  @BuiltValueField(wireName: "first_name")
  String get firstName;

  @nullable
  @BuiltValueField(wireName: "last_name")
  String get lastName;

  @nullable
  String get status;

  @nullable
  @BuiltValueField(wireName: "date_of_birth")
  String get dob;

  @nullable
  @BuiltValueField(wireName: "date_added")
  String get dateAdded;

  @nullable
  int get age;

  @nullable
  @BuiltValueField(wireName: "address_line_1")
  String get address1;

  @nullable
  @BuiltValueField(wireName: "address_line_2")
  String get address2;

  @nullable
  String get city;

  @nullable
  String get state;

  @nullable
  int get points;

  @nullable
  @BuiltValueField(wireName: "zip_code")
  String get zipCode;

  @nullable
  @BuiltValueField(wireName: "preferred_contact_method")
  String get preferredContact;

  @nullable
  @BuiltValueField(wireName: "blood_group")
  String get bloodGroup;

  @nullable
  @BuiltValueField(wireName: "decease_date")
  String get deceaseDate;

  @nullable
  bool get deceased;

  @nullable
  bool get subscribed;

  @nullable
  @BuiltValueField(wireName: "notification_subscribed")
  bool get notificationSubscribed;

  @nullable
  @BuiltValueField(wireName: "email_subscribed")
  bool get emailSubscribed;

  @nullable
  @BuiltValueField(wireName: "sms_subscribed")
  bool get smsSubscribed;

  @nullable
  double get height;

  @nullable
  double get weight;

  @nullable
  double get breathing;

  @nullable
  double get bloodPressure;

  @nullable
  double get pulse;

  @nullable
  double get temperature;

  @nullable
  @BuiltValueField(wireName: "inpatient_relative_risk")
  String get impatientRelativeRisk;

  @nullable
  @BuiltValueField(wireName: "chronic_disease_count")
  double get chronicDiseaseCount;

  @nullable
  String get quickNotes;

  @nullable
  Gender get gender;

  @nullable
  String get code;

  @nullable
  @BuiltValueField(wireName: "work_email")
  String get workEmail;

  @nullable
  @BuiltValueField(wireName: "personal_email")
  String get personalEmail;

  @nullable
  ResponseDataList<Medicine> get medicines;

  @nullable
  ResponseDataList<UserNotes> get notes;

  @nullable
  @BuiltValueField(wireName: "cell_phone")
  String get cellPhone;

  @nullable
  @BuiltValueField(wireName: "work_phone")
  String get workPhone;

  @nullable
  @BuiltValueField(wireName: "home_phone")
  String get homePhone;

  @nullable
  ResponseDataList<Reminder> get reminders;

  @nullable
  ResponseData<UserPractice> get practice;

  @nullable
  ResponseData<UserDoctor> get doctor;

  @nullable
  ResponseData<UserPayer> get payer;

  @nullable
  @BuiltValueField(wireName: "chronic_diseases")
  BuiltList<ChronicDiseases> get chronicDiseases;

  @nullable
  @BuiltValueField(wireName: "updated_at")
  String get updatedAt;

  @nullable
  @BuiltValueField(wireName: "created_at")
  String get createdAt;

  User._();

  @memoized
  String getFullName() {
    return firstName +
        ((lastName != null) ? (lastName.length > 0) ? " $lastName" : "" : "");
  }

  @memoized
  String getAvatarUrl() {
    final names = getFullName().split(" ");
    String prefix = names[0].substring(0, 1);

    if (names.length > 1) {
      prefix += names[1].substring(0, 1);
    }

    return AppConfig.dummyImageUrl + prefix;
  }

  factory User([updates(UserBuilder b)]) = _$User;

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "first_name": firstName,
      "last_name": lastName,
      "status": status,
      "date_of_birth": dob,
      "date_added": dateAdded,
      "address_line_1": address1,
      "address_line_2": address2,
      "city": city,
      "points": points,
      "state": state,
      "zip_code": zipCode,
      "preferred_contact_method": preferredContact,
      "subscribed": subscribed,
      "sms_subscribed": smsSubscribed,
      "notification_subscribed": notificationSubscribed,
      "email_subscribed": emailSubscribed,
      "blood_group": bloodGroup,
      "blood_pressure": bloodPressure,
      "personal_email": personalEmail,
      "work_email": workEmail,
      "cell_phone": cellPhone,
      "work_phone": workPhone,
      "home_phone": homePhone,
    };
  }

  String getOneSignalEmail() {
    return "Patient$id@osler4network.com";
  }
}

class Gender extends EnumClass {
  static Serializer<Gender> get serializer => _$genderSerializer;

  static const Gender male = _$male;
  static const Gender female = _$female;
  static const Gender other = _$other;
  static const Gender NOT_DEFINED = _$NOT_DEFINED;

  const Gender._(String name) : super(name);

  static BuiltSet<Gender> get values => _$vlu;

  static Gender valueOf(String name) => _$vluOf(name);
}
