import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_payer.g.dart';

abstract class UserPayer implements Built<UserPayer, UserPayerBuilder> {
  @nullable
  int get id;

  @nullable
  String get label;

  @nullable
  String get logo;

  @nullable
  BuiltList<int> get levels;

  @nullable
  @BuiltValueField(wireName: "updated_at")
  String get updatedAt;

  @nullable
  @BuiltValueField(wireName: "created_at")
  String get createdAt;

  UserPayer._();

  factory UserPayer([updates(UserPayerBuilder b)]) = _$UserPayer;

  static Serializer<UserPayer> get serializer => _$userPayerSerializer;
}
