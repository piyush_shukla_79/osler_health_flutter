// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AppNotification> _$appNotificationSerializer =
    new _$AppNotificationSerializer();

class _$AppNotificationSerializer
    implements StructuredSerializer<AppNotification> {
  @override
  final Iterable<Type> types = const [AppNotification, _$AppNotification];
  @override
  final String wireName = 'AppNotification';

  @override
  Iterable serialize(Serializers serializers, AppNotification object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'patient_id',
      serializers.serialize(object.patientId,
          specifiedType: const FullType(int)),
      'read',
      serializers.serialize(object.read, specifiedType: const FullType(bool)),
      'created_at',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(String)),
      'updated_at',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(String)),
    ];
    if (object.type != null) {
      result
        ..add('notification_type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(String)));
    }
    if (object.body != null) {
      result
        ..add('body')
        ..add(serializers.serialize(object.body,
            specifiedType: const FullType(String)));
    }
    if (object.startDate != null) {
      result
        ..add('start_date')
        ..add(serializers.serialize(object.startDate,
            specifiedType: const FullType(String)));
    }
    if (object.endDate != null) {
      result
        ..add('end_date')
        ..add(serializers.serialize(object.endDate,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  AppNotification deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AppNotificationBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'patient_id':
          result.patientId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'read':
          result.read = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'notification_type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'body':
          result.body = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updated_at':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'start_date':
          result.startDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'end_date':
          result.endDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$AppNotification extends AppNotification {
  @override
  final int id;
  @override
  final String title;
  @override
  final int patientId;
  @override
  final bool read;
  @override
  final String type;
  @override
  final String body;
  @override
  final String createdAt;
  @override
  final String updatedAt;
  @override
  final String startDate;
  @override
  final String endDate;

  factory _$AppNotification([void Function(AppNotificationBuilder) updates]) =>
      (new AppNotificationBuilder()..update(updates)).build();

  _$AppNotification._(
      {this.id,
      this.title,
      this.patientId,
      this.read,
      this.type,
      this.body,
      this.createdAt,
      this.updatedAt,
      this.startDate,
      this.endDate})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('AppNotification', 'id');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('AppNotification', 'title');
    }
    if (patientId == null) {
      throw new BuiltValueNullFieldError('AppNotification', 'patientId');
    }
    if (read == null) {
      throw new BuiltValueNullFieldError('AppNotification', 'read');
    }
    if (createdAt == null) {
      throw new BuiltValueNullFieldError('AppNotification', 'createdAt');
    }
    if (updatedAt == null) {
      throw new BuiltValueNullFieldError('AppNotification', 'updatedAt');
    }
  }

  @override
  AppNotification rebuild(void Function(AppNotificationBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AppNotificationBuilder toBuilder() =>
      new AppNotificationBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AppNotification &&
        id == other.id &&
        title == other.title &&
        patientId == other.patientId &&
        read == other.read &&
        type == other.type &&
        body == other.body &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        startDate == other.startDate &&
        endDate == other.endDate;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc($jc(0, id.hashCode), title.hashCode),
                                    patientId.hashCode),
                                read.hashCode),
                            type.hashCode),
                        body.hashCode),
                    createdAt.hashCode),
                updatedAt.hashCode),
            startDate.hashCode),
        endDate.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AppNotification')
          ..add('id', id)
          ..add('title', title)
          ..add('patientId', patientId)
          ..add('read', read)
          ..add('type', type)
          ..add('body', body)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('startDate', startDate)
          ..add('endDate', endDate))
        .toString();
  }
}

class AppNotificationBuilder
    implements Builder<AppNotification, AppNotificationBuilder> {
  _$AppNotification _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  int _patientId;
  int get patientId => _$this._patientId;
  set patientId(int patientId) => _$this._patientId = patientId;

  bool _read;
  bool get read => _$this._read;
  set read(bool read) => _$this._read = read;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  String _body;
  String get body => _$this._body;
  set body(String body) => _$this._body = body;

  String _createdAt;
  String get createdAt => _$this._createdAt;
  set createdAt(String createdAt) => _$this._createdAt = createdAt;

  String _updatedAt;
  String get updatedAt => _$this._updatedAt;
  set updatedAt(String updatedAt) => _$this._updatedAt = updatedAt;

  String _startDate;
  String get startDate => _$this._startDate;
  set startDate(String startDate) => _$this._startDate = startDate;

  String _endDate;
  String get endDate => _$this._endDate;
  set endDate(String endDate) => _$this._endDate = endDate;

  AppNotificationBuilder();

  AppNotificationBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _title = _$v.title;
      _patientId = _$v.patientId;
      _read = _$v.read;
      _type = _$v.type;
      _body = _$v.body;
      _createdAt = _$v.createdAt;
      _updatedAt = _$v.updatedAt;
      _startDate = _$v.startDate;
      _endDate = _$v.endDate;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AppNotification other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AppNotification;
  }

  @override
  void update(void Function(AppNotificationBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AppNotification build() {
    final _$result = _$v ??
        new _$AppNotification._(
            id: id,
            title: title,
            patientId: patientId,
            read: read,
            type: type,
            body: body,
            createdAt: createdAt,
            updatedAt: updatedAt,
            startDate: startDate,
            endDate: endDate);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
