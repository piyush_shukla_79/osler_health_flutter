import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'medicine_record.g.dart';

abstract class MedicineRecord
    implements Built<MedicineRecord, MedicineRecordBuilder> {
  static Serializer<MedicineRecord> get serializer =>
      _$medicineRecordSerializer;

  @nullable
  int get id;

  @nullable
  String get medicineName;

  @nullable
  bool get active;

  MedicineRecord._();

  factory MedicineRecord([updates(MedicineRecordBuilder b)]) = _$MedicineRecord;
}
