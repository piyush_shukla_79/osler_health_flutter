// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reminder_notification.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ReminderNotification> _$reminderNotificationSerializer =
    new _$ReminderNotificationSerializer();

class _$ReminderNotificationSerializer
    implements StructuredSerializer<ReminderNotification> {
  @override
  final Iterable<Type> types = const [
    ReminderNotification,
    _$ReminderNotification
  ];
  @override
  final String wireName = 'ReminderNotification';

  @override
  Iterable serialize(Serializers serializers, ReminderNotification object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'hour',
      serializers.serialize(object.hour, specifiedType: const FullType(int)),
      'minute',
      serializers.serialize(object.minute, specifiedType: const FullType(int)),
      'enabled',
      serializers.serialize(object.enabled,
          specifiedType: const FullType(bool)),
    ];
    if (object.date != null) {
      result
        ..add('date')
        ..add(serializers.serialize(object.date,
            specifiedType: const FullType(int)));
    }

    return result;
  }

  @override
  ReminderNotification deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ReminderNotificationBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'hour':
          result.hour = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'minute':
          result.minute = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'enabled':
          result.enabled = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'date':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ReminderNotification extends ReminderNotification {
  @override
  final int hour;
  @override
  final int minute;
  @override
  final bool enabled;
  @override
  final int date;

  factory _$ReminderNotification(
          [void updates(ReminderNotificationBuilder b)]) =>
      (new ReminderNotificationBuilder()..update(updates)).build();

  _$ReminderNotification._({this.hour, this.minute, this.enabled, this.date})
      : super._() {
    if (hour == null) {
      throw new BuiltValueNullFieldError('ReminderNotification', 'hour');
    }
    if (minute == null) {
      throw new BuiltValueNullFieldError('ReminderNotification', 'minute');
    }
    if (enabled == null) {
      throw new BuiltValueNullFieldError('ReminderNotification', 'enabled');
    }
  }

  @override
  ReminderNotification rebuild(void updates(ReminderNotificationBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  ReminderNotificationBuilder toBuilder() =>
      new ReminderNotificationBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ReminderNotification &&
        hour == other.hour &&
        minute == other.minute &&
        enabled == other.enabled &&
        date == other.date;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, hour.hashCode), minute.hashCode), enabled.hashCode),
        date.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ReminderNotification')
          ..add('hour', hour)
          ..add('minute', minute)
          ..add('enabled', enabled)
          ..add('date', date))
        .toString();
  }
}

class ReminderNotificationBuilder
    implements Builder<ReminderNotification, ReminderNotificationBuilder> {
  _$ReminderNotification _$v;

  int _hour;
  int get hour => _$this._hour;
  set hour(int hour) => _$this._hour = hour;

  int _minute;
  int get minute => _$this._minute;
  set minute(int minute) => _$this._minute = minute;

  bool _enabled;
  bool get enabled => _$this._enabled;
  set enabled(bool enabled) => _$this._enabled = enabled;

  int _date;
  int get date => _$this._date;
  set date(int date) => _$this._date = date;

  ReminderNotificationBuilder();

  ReminderNotificationBuilder get _$this {
    if (_$v != null) {
      _hour = _$v.hour;
      _minute = _$v.minute;
      _enabled = _$v.enabled;
      _date = _$v.date;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ReminderNotification other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ReminderNotification;
  }

  @override
  void update(void updates(ReminderNotificationBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$ReminderNotification build() {
    final _$result = _$v ??
        new _$ReminderNotification._(
            hour: hour, minute: minute, enabled: enabled, date: date);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
