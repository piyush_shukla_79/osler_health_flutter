import 'package:rxdart/rxdart.dart';

mixin UnsubscribeMixin {
  PublishSubject<bool> destroy$ = new PublishSubject();

  void onDispose() async {
    destroy$.add(true);
    await destroy$.close();
  }
}
