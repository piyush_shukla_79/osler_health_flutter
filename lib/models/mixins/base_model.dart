import 'package:osler_health/utils/helpers.dart';

mixin BaseModel {
  String get createdAt;

  String get updatedAt;

  String get startDate;

  String get endDate;

  DateTime getCreatedAt() {
    return getDateTime(createdAt);
  }

  DateTime getUpdatedAt() {
    return getDateTime(updatedAt);
  }

  String getYMMDCreatedAt() {
    return yMMMd(getCreatedAt());
  }

  String getYMMDUpdatedAt() {
    return yMMMd(getUpdatedAt());
  }

  String getHmCreatedAt() {
    return date(getCreatedAt());
  }
}
