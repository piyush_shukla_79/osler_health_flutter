import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'chronic_diseases.g.dart';

abstract class ChronicDiseases
    implements Built<ChronicDiseases, ChronicDiseasesBuilder> {
  @BuiltValueField(wireName: "subchronic_diseases")
  BuiltList<ChronicDiseases> get subchronicDiseases;

  int get id;

  String get label;

  @nullable
  @BuiltValueField(wireName: "diagnosis_date")
  String get diagnosisDate;

  ChronicDiseases._();

  factory ChronicDiseases([updates(ChronicDiseasesBuilder b)]) =
      _$ChronicDiseases;

  static Serializer<ChronicDiseases> get serializer =>
      _$chronicDiseasesSerializer;
}
