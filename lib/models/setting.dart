import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'setting.g.dart';

abstract class Setting implements Built<Setting, SettingBuilder> {
  static Serializer<Setting> get serializer => _$settingSerializer;

  @nullable
  bool get email;

  @nullable
  bool get notification;

  @nullable
  bool get messages;

  Setting._();

  factory Setting([updates(SettingBuilder b)]) = _$Setting;
}
