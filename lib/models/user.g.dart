// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const Gender _$male = const Gender._('male');
const Gender _$female = const Gender._('female');
const Gender _$other = const Gender._('other');
const Gender _$NOT_DEFINED = const Gender._('NOT_DEFINED');

Gender _$vluOf(String name) {
  switch (name) {
    case 'male':
      return _$male;
    case 'female':
      return _$female;
    case 'other':
      return _$other;
    case 'NOT_DEFINED':
      return _$NOT_DEFINED;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<Gender> _$vlu = new BuiltSet<Gender>(const <Gender>[
  _$male,
  _$female,
  _$other,
  _$NOT_DEFINED,
]);

Serializer<User> _$userSerializer = new _$UserSerializer();
Serializer<Gender> _$genderSerializer = new _$GenderSerializer();

class _$UserSerializer implements StructuredSerializer<User> {
  @override
  final Iterable<Type> types = const [User, _$User];
  @override
  final String wireName = 'User';

  @override
  Iterable serialize(Serializers serializers, User object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
    ];
    if (object.firstName != null) {
      result
        ..add('first_name')
        ..add(serializers.serialize(object.firstName,
            specifiedType: const FullType(String)));
    }
    if (object.lastName != null) {
      result
        ..add('last_name')
        ..add(serializers.serialize(object.lastName,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(String)));
    }
    if (object.dob != null) {
      result
        ..add('date_of_birth')
        ..add(serializers.serialize(object.dob,
            specifiedType: const FullType(String)));
    }
    if (object.dateAdded != null) {
      result
        ..add('date_added')
        ..add(serializers.serialize(object.dateAdded,
            specifiedType: const FullType(String)));
    }
    if (object.age != null) {
      result
        ..add('age')
        ..add(serializers.serialize(object.age,
            specifiedType: const FullType(int)));
    }
    if (object.address1 != null) {
      result
        ..add('address_line_1')
        ..add(serializers.serialize(object.address1,
            specifiedType: const FullType(String)));
    }
    if (object.address2 != null) {
      result
        ..add('address_line_2')
        ..add(serializers.serialize(object.address2,
            specifiedType: const FullType(String)));
    }
    if (object.city != null) {
      result
        ..add('city')
        ..add(serializers.serialize(object.city,
            specifiedType: const FullType(String)));
    }
    if (object.state != null) {
      result
        ..add('state')
        ..add(serializers.serialize(object.state,
            specifiedType: const FullType(String)));
    }
    if (object.points != null) {
      result
        ..add('points')
        ..add(serializers.serialize(object.points,
            specifiedType: const FullType(int)));
    }
    if (object.zipCode != null) {
      result
        ..add('zip_code')
        ..add(serializers.serialize(object.zipCode,
            specifiedType: const FullType(String)));
    }
    if (object.preferredContact != null) {
      result
        ..add('preferred_contact_method')
        ..add(serializers.serialize(object.preferredContact,
            specifiedType: const FullType(String)));
    }
    if (object.bloodGroup != null) {
      result
        ..add('blood_group')
        ..add(serializers.serialize(object.bloodGroup,
            specifiedType: const FullType(String)));
    }
    if (object.deceaseDate != null) {
      result
        ..add('decease_date')
        ..add(serializers.serialize(object.deceaseDate,
            specifiedType: const FullType(String)));
    }
    if (object.deceased != null) {
      result
        ..add('deceased')
        ..add(serializers.serialize(object.deceased,
            specifiedType: const FullType(bool)));
    }
    if (object.subscribed != null) {
      result
        ..add('subscribed')
        ..add(serializers.serialize(object.subscribed,
            specifiedType: const FullType(bool)));
    }
    if (object.notificationSubscribed != null) {
      result
        ..add('notification_subscribed')
        ..add(serializers.serialize(object.notificationSubscribed,
            specifiedType: const FullType(bool)));
    }
    if (object.emailSubscribed != null) {
      result
        ..add('email_subscribed')
        ..add(serializers.serialize(object.emailSubscribed,
            specifiedType: const FullType(bool)));
    }
    if (object.smsSubscribed != null) {
      result
        ..add('sms_subscribed')
        ..add(serializers.serialize(object.smsSubscribed,
            specifiedType: const FullType(bool)));
    }
    if (object.height != null) {
      result
        ..add('height')
        ..add(serializers.serialize(object.height,
            specifiedType: const FullType(double)));
    }
    if (object.weight != null) {
      result
        ..add('weight')
        ..add(serializers.serialize(object.weight,
            specifiedType: const FullType(double)));
    }
    if (object.breathing != null) {
      result
        ..add('breathing')
        ..add(serializers.serialize(object.breathing,
            specifiedType: const FullType(double)));
    }
    if (object.bloodPressure != null) {
      result
        ..add('bloodPressure')
        ..add(serializers.serialize(object.bloodPressure,
            specifiedType: const FullType(double)));
    }
    if (object.pulse != null) {
      result
        ..add('pulse')
        ..add(serializers.serialize(object.pulse,
            specifiedType: const FullType(double)));
    }
    if (object.temperature != null) {
      result
        ..add('temperature')
        ..add(serializers.serialize(object.temperature,
            specifiedType: const FullType(double)));
    }
    if (object.impatientRelativeRisk != null) {
      result
        ..add('inpatient_relative_risk')
        ..add(serializers.serialize(object.impatientRelativeRisk,
            specifiedType: const FullType(String)));
    }
    if (object.chronicDiseaseCount != null) {
      result
        ..add('chronic_disease_count')
        ..add(serializers.serialize(object.chronicDiseaseCount,
            specifiedType: const FullType(double)));
    }
    if (object.quickNotes != null) {
      result
        ..add('quickNotes')
        ..add(serializers.serialize(object.quickNotes,
            specifiedType: const FullType(String)));
    }
    if (object.gender != null) {
      result
        ..add('gender')
        ..add(serializers.serialize(object.gender,
            specifiedType: const FullType(Gender)));
    }
    if (object.code != null) {
      result
        ..add('code')
        ..add(serializers.serialize(object.code,
            specifiedType: const FullType(String)));
    }
    if (object.workEmail != null) {
      result
        ..add('work_email')
        ..add(serializers.serialize(object.workEmail,
            specifiedType: const FullType(String)));
    }
    if (object.personalEmail != null) {
      result
        ..add('personal_email')
        ..add(serializers.serialize(object.personalEmail,
            specifiedType: const FullType(String)));
    }
    if (object.medicines != null) {
      result
        ..add('medicines')
        ..add(serializers.serialize(object.medicines,
            specifiedType: const FullType(
                ResponseDataList, const [const FullType(Medicine)])));
    }
    if (object.notes != null) {
      result
        ..add('notes')
        ..add(serializers.serialize(object.notes,
            specifiedType: const FullType(
                ResponseDataList, const [const FullType(UserNotes)])));
    }
    if (object.cellPhone != null) {
      result
        ..add('cell_phone')
        ..add(serializers.serialize(object.cellPhone,
            specifiedType: const FullType(String)));
    }
    if (object.workPhone != null) {
      result
        ..add('work_phone')
        ..add(serializers.serialize(object.workPhone,
            specifiedType: const FullType(String)));
    }
    if (object.homePhone != null) {
      result
        ..add('home_phone')
        ..add(serializers.serialize(object.homePhone,
            specifiedType: const FullType(String)));
    }
    if (object.reminders != null) {
      result
        ..add('reminders')
        ..add(serializers.serialize(object.reminders,
            specifiedType: const FullType(
                ResponseDataList, const [const FullType(Reminder)])));
    }
    if (object.practice != null) {
      result
        ..add('practice')
        ..add(serializers.serialize(object.practice,
            specifiedType: const FullType(
                ResponseData, const [const FullType(UserPractice)])));
    }
    if (object.doctor != null) {
      result
        ..add('doctor')
        ..add(serializers.serialize(object.doctor,
            specifiedType: const FullType(
                ResponseData, const [const FullType(UserDoctor)])));
    }
    if (object.payer != null) {
      result
        ..add('payer')
        ..add(serializers.serialize(object.payer,
            specifiedType: const FullType(
                ResponseData, const [const FullType(UserPayer)])));
    }
    if (object.chronicDiseases != null) {
      result
        ..add('chronic_diseases')
        ..add(serializers.serialize(object.chronicDiseases,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ChronicDiseases)])));
    }
    if (object.updatedAt != null) {
      result
        ..add('updated_at')
        ..add(serializers.serialize(object.updatedAt,
            specifiedType: const FullType(String)));
    }
    if (object.createdAt != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  User deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'first_name':
          result.firstName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'last_name':
          result.lastName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'date_of_birth':
          result.dob = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'date_added':
          result.dateAdded = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'age':
          result.age = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'address_line_1':
          result.address1 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'address_line_2':
          result.address2 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'city':
          result.city = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'state':
          result.state = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'points':
          result.points = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'zip_code':
          result.zipCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'preferred_contact_method':
          result.preferredContact = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'blood_group':
          result.bloodGroup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'decease_date':
          result.deceaseDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'deceased':
          result.deceased = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'subscribed':
          result.subscribed = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'notification_subscribed':
          result.notificationSubscribed = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'email_subscribed':
          result.emailSubscribed = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'sms_subscribed':
          result.smsSubscribed = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'height':
          result.height = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'weight':
          result.weight = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'breathing':
          result.breathing = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'bloodPressure':
          result.bloodPressure = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'pulse':
          result.pulse = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'temperature':
          result.temperature = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'inpatient_relative_risk':
          result.impatientRelativeRisk = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'chronic_disease_count':
          result.chronicDiseaseCount = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'quickNotes':
          result.quickNotes = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'gender':
          result.gender = serializers.deserialize(value,
              specifiedType: const FullType(Gender)) as Gender;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'work_email':
          result.workEmail = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'personal_email':
          result.personalEmail = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'medicines':
          result.medicines.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      ResponseDataList, const [const FullType(Medicine)]))
              as ResponseDataList<Medicine>);
          break;
        case 'notes':
          result.notes.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      ResponseDataList, const [const FullType(UserNotes)]))
              as ResponseDataList<UserNotes>);
          break;
        case 'cell_phone':
          result.cellPhone = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'work_phone':
          result.workPhone = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'home_phone':
          result.homePhone = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'reminders':
          result.reminders.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      ResponseDataList, const [const FullType(Reminder)]))
              as ResponseDataList<Reminder>);
          break;
        case 'practice':
          result.practice.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      ResponseData, const [const FullType(UserPractice)]))
              as ResponseData<UserPractice>);
          break;
        case 'doctor':
          result.doctor.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      ResponseData, const [const FullType(UserDoctor)]))
              as ResponseData<UserDoctor>);
          break;
        case 'payer':
          result.payer.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      ResponseData, const [const FullType(UserPayer)]))
              as ResponseData<UserPayer>);
          break;
        case 'chronic_diseases':
          result.chronicDiseases.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ChronicDiseases)]))
              as BuiltList);
          break;
        case 'updated_at':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GenderSerializer implements PrimitiveSerializer<Gender> {
  @override
  final Iterable<Type> types = const <Type>[Gender];
  @override
  final String wireName = 'Gender';

  @override
  Object serialize(Serializers serializers, Gender object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  Gender deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      Gender.valueOf(serialized as String);
}

class _$User extends User {
  @override
  final int id;
  @override
  final String firstName;
  @override
  final String lastName;
  @override
  final String status;
  @override
  final String dob;
  @override
  final String dateAdded;
  @override
  final int age;
  @override
  final String address1;
  @override
  final String address2;
  @override
  final String city;
  @override
  final String state;
  @override
  final int points;
  @override
  final String zipCode;
  @override
  final String preferredContact;
  @override
  final String bloodGroup;
  @override
  final String deceaseDate;
  @override
  final bool deceased;
  @override
  final bool subscribed;
  @override
  final bool notificationSubscribed;
  @override
  final bool emailSubscribed;
  @override
  final bool smsSubscribed;
  @override
  final double height;
  @override
  final double weight;
  @override
  final double breathing;
  @override
  final double bloodPressure;
  @override
  final double pulse;
  @override
  final double temperature;
  @override
  final String impatientRelativeRisk;
  @override
  final double chronicDiseaseCount;
  @override
  final String quickNotes;
  @override
  final Gender gender;
  @override
  final String code;
  @override
  final String workEmail;
  @override
  final String personalEmail;
  @override
  final ResponseDataList<Medicine> medicines;
  @override
  final ResponseDataList<UserNotes> notes;
  @override
  final String cellPhone;
  @override
  final String workPhone;
  @override
  final String homePhone;
  @override
  final ResponseDataList<Reminder> reminders;
  @override
  final ResponseData<UserPractice> practice;
  @override
  final ResponseData<UserDoctor> doctor;
  @override
  final ResponseData<UserPayer> payer;
  @override
  final BuiltList<ChronicDiseases> chronicDiseases;
  @override
  final String updatedAt;
  @override
  final String createdAt;

  factory _$User([void updates(UserBuilder b)]) =>
      (new UserBuilder()..update(updates)).build();

  _$User._(
      {this.id,
      this.firstName,
      this.lastName,
      this.status,
      this.dob,
      this.dateAdded,
      this.age,
      this.address1,
      this.address2,
      this.city,
      this.state,
      this.points,
      this.zipCode,
      this.preferredContact,
      this.bloodGroup,
      this.deceaseDate,
      this.deceased,
      this.subscribed,
      this.notificationSubscribed,
      this.emailSubscribed,
      this.smsSubscribed,
      this.height,
      this.weight,
      this.breathing,
      this.bloodPressure,
      this.pulse,
      this.temperature,
      this.impatientRelativeRisk,
      this.chronicDiseaseCount,
      this.quickNotes,
      this.gender,
      this.code,
      this.workEmail,
      this.personalEmail,
      this.medicines,
      this.notes,
      this.cellPhone,
      this.workPhone,
      this.homePhone,
      this.reminders,
      this.practice,
      this.doctor,
      this.payer,
      this.chronicDiseases,
      this.updatedAt,
      this.createdAt})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('User', 'id');
    }
  }

  @override
  User rebuild(void updates(UserBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserBuilder toBuilder() => new UserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is User &&
        id == other.id &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        status == other.status &&
        dob == other.dob &&
        dateAdded == other.dateAdded &&
        age == other.age &&
        address1 == other.address1 &&
        address2 == other.address2 &&
        city == other.city &&
        state == other.state &&
        points == other.points &&
        zipCode == other.zipCode &&
        preferredContact == other.preferredContact &&
        bloodGroup == other.bloodGroup &&
        deceaseDate == other.deceaseDate &&
        deceased == other.deceased &&
        subscribed == other.subscribed &&
        notificationSubscribed == other.notificationSubscribed &&
        emailSubscribed == other.emailSubscribed &&
        smsSubscribed == other.smsSubscribed &&
        height == other.height &&
        weight == other.weight &&
        breathing == other.breathing &&
        bloodPressure == other.bloodPressure &&
        pulse == other.pulse &&
        temperature == other.temperature &&
        impatientRelativeRisk == other.impatientRelativeRisk &&
        chronicDiseaseCount == other.chronicDiseaseCount &&
        quickNotes == other.quickNotes &&
        gender == other.gender &&
        code == other.code &&
        workEmail == other.workEmail &&
        personalEmail == other.personalEmail &&
        medicines == other.medicines &&
        notes == other.notes &&
        cellPhone == other.cellPhone &&
        workPhone == other.workPhone &&
        homePhone == other.homePhone &&
        reminders == other.reminders &&
        practice == other.practice &&
        doctor == other.doctor &&
        payer == other.payer &&
        chronicDiseases == other.chronicDiseases &&
        updatedAt == other.updatedAt &&
        createdAt == other.createdAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    $jc(
                                                                        $jc(
                                                                            $jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc($jc(0, id.hashCode), firstName.hashCode), lastName.hashCode), status.hashCode), dob.hashCode), dateAdded.hashCode), age.hashCode), address1.hashCode), address2.hashCode), city.hashCode), state.hashCode), points.hashCode), zipCode.hashCode), preferredContact.hashCode), bloodGroup.hashCode), deceaseDate.hashCode), deceased.hashCode), subscribed.hashCode), notificationSubscribed.hashCode), emailSubscribed.hashCode), smsSubscribed.hashCode), height.hashCode), weight.hashCode), breathing.hashCode), bloodPressure.hashCode), pulse.hashCode), temperature.hashCode),
                                                                                impatientRelativeRisk.hashCode),
                                                                            chronicDiseaseCount.hashCode),
                                                                        quickNotes.hashCode),
                                                                    gender.hashCode),
                                                                code.hashCode),
                                                            workEmail.hashCode),
                                                        personalEmail.hashCode),
                                                    medicines.hashCode),
                                                notes.hashCode),
                                            cellPhone.hashCode),
                                        workPhone.hashCode),
                                    homePhone.hashCode),
                                reminders.hashCode),
                            practice.hashCode),
                        doctor.hashCode),
                    payer.hashCode),
                chronicDiseases.hashCode),
            updatedAt.hashCode),
        createdAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('User')
          ..add('id', id)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('status', status)
          ..add('dob', dob)
          ..add('dateAdded', dateAdded)
          ..add('age', age)
          ..add('address1', address1)
          ..add('address2', address2)
          ..add('city', city)
          ..add('state', state)
          ..add('points', points)
          ..add('zipCode', zipCode)
          ..add('preferredContact', preferredContact)
          ..add('bloodGroup', bloodGroup)
          ..add('deceaseDate', deceaseDate)
          ..add('deceased', deceased)
          ..add('subscribed', subscribed)
          ..add('notificationSubscribed', notificationSubscribed)
          ..add('emailSubscribed', emailSubscribed)
          ..add('smsSubscribed', smsSubscribed)
          ..add('height', height)
          ..add('weight', weight)
          ..add('breathing', breathing)
          ..add('bloodPressure', bloodPressure)
          ..add('pulse', pulse)
          ..add('temperature', temperature)
          ..add('impatientRelativeRisk', impatientRelativeRisk)
          ..add('chronicDiseaseCount', chronicDiseaseCount)
          ..add('quickNotes', quickNotes)
          ..add('gender', gender)
          ..add('code', code)
          ..add('workEmail', workEmail)
          ..add('personalEmail', personalEmail)
          ..add('medicines', medicines)
          ..add('notes', notes)
          ..add('cellPhone', cellPhone)
          ..add('workPhone', workPhone)
          ..add('homePhone', homePhone)
          ..add('reminders', reminders)
          ..add('practice', practice)
          ..add('doctor', doctor)
          ..add('payer', payer)
          ..add('chronicDiseases', chronicDiseases)
          ..add('updatedAt', updatedAt)
          ..add('createdAt', createdAt))
        .toString();
  }
}

class UserBuilder implements Builder<User, UserBuilder> {
  _$User _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _firstName;
  String get firstName => _$this._firstName;
  set firstName(String firstName) => _$this._firstName = firstName;

  String _lastName;
  String get lastName => _$this._lastName;
  set lastName(String lastName) => _$this._lastName = lastName;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  String _dob;
  String get dob => _$this._dob;
  set dob(String dob) => _$this._dob = dob;

  String _dateAdded;
  String get dateAdded => _$this._dateAdded;
  set dateAdded(String dateAdded) => _$this._dateAdded = dateAdded;

  int _age;
  int get age => _$this._age;
  set age(int age) => _$this._age = age;

  String _address1;
  String get address1 => _$this._address1;
  set address1(String address1) => _$this._address1 = address1;

  String _address2;
  String get address2 => _$this._address2;
  set address2(String address2) => _$this._address2 = address2;

  String _city;
  String get city => _$this._city;
  set city(String city) => _$this._city = city;

  String _state;
  String get state => _$this._state;
  set state(String state) => _$this._state = state;

  int _points;
  int get points => _$this._points;
  set points(int points) => _$this._points = points;

  String _zipCode;
  String get zipCode => _$this._zipCode;
  set zipCode(String zipCode) => _$this._zipCode = zipCode;

  String _preferredContact;
  String get preferredContact => _$this._preferredContact;
  set preferredContact(String preferredContact) =>
      _$this._preferredContact = preferredContact;

  String _bloodGroup;
  String get bloodGroup => _$this._bloodGroup;
  set bloodGroup(String bloodGroup) => _$this._bloodGroup = bloodGroup;

  String _deceaseDate;
  String get deceaseDate => _$this._deceaseDate;
  set deceaseDate(String deceaseDate) => _$this._deceaseDate = deceaseDate;

  bool _deceased;
  bool get deceased => _$this._deceased;
  set deceased(bool deceased) => _$this._deceased = deceased;

  bool _subscribed;
  bool get subscribed => _$this._subscribed;
  set subscribed(bool subscribed) => _$this._subscribed = subscribed;

  bool _notificationSubscribed;
  bool get notificationSubscribed => _$this._notificationSubscribed;
  set notificationSubscribed(bool notificationSubscribed) =>
      _$this._notificationSubscribed = notificationSubscribed;

  bool _emailSubscribed;
  bool get emailSubscribed => _$this._emailSubscribed;
  set emailSubscribed(bool emailSubscribed) =>
      _$this._emailSubscribed = emailSubscribed;

  bool _smsSubscribed;
  bool get smsSubscribed => _$this._smsSubscribed;
  set smsSubscribed(bool smsSubscribed) =>
      _$this._smsSubscribed = smsSubscribed;

  double _height;
  double get height => _$this._height;
  set height(double height) => _$this._height = height;

  double _weight;
  double get weight => _$this._weight;
  set weight(double weight) => _$this._weight = weight;

  double _breathing;
  double get breathing => _$this._breathing;
  set breathing(double breathing) => _$this._breathing = breathing;

  double _bloodPressure;
  double get bloodPressure => _$this._bloodPressure;
  set bloodPressure(double bloodPressure) =>
      _$this._bloodPressure = bloodPressure;

  double _pulse;
  double get pulse => _$this._pulse;
  set pulse(double pulse) => _$this._pulse = pulse;

  double _temperature;
  double get temperature => _$this._temperature;
  set temperature(double temperature) => _$this._temperature = temperature;

  String _impatientRelativeRisk;
  String get impatientRelativeRisk => _$this._impatientRelativeRisk;
  set impatientRelativeRisk(String impatientRelativeRisk) =>
      _$this._impatientRelativeRisk = impatientRelativeRisk;

  double _chronicDiseaseCount;
  double get chronicDiseaseCount => _$this._chronicDiseaseCount;
  set chronicDiseaseCount(double chronicDiseaseCount) =>
      _$this._chronicDiseaseCount = chronicDiseaseCount;

  String _quickNotes;
  String get quickNotes => _$this._quickNotes;
  set quickNotes(String quickNotes) => _$this._quickNotes = quickNotes;

  Gender _gender;
  Gender get gender => _$this._gender;
  set gender(Gender gender) => _$this._gender = gender;

  String _code;
  String get code => _$this._code;
  set code(String code) => _$this._code = code;

  String _workEmail;
  String get workEmail => _$this._workEmail;
  set workEmail(String workEmail) => _$this._workEmail = workEmail;

  String _personalEmail;
  String get personalEmail => _$this._personalEmail;
  set personalEmail(String personalEmail) =>
      _$this._personalEmail = personalEmail;

  ResponseDataListBuilder<Medicine> _medicines;
  ResponseDataListBuilder<Medicine> get medicines =>
      _$this._medicines ??= new ResponseDataListBuilder<Medicine>();
  set medicines(ResponseDataListBuilder<Medicine> medicines) =>
      _$this._medicines = medicines;

  ResponseDataListBuilder<UserNotes> _notes;
  ResponseDataListBuilder<UserNotes> get notes =>
      _$this._notes ??= new ResponseDataListBuilder<UserNotes>();
  set notes(ResponseDataListBuilder<UserNotes> notes) => _$this._notes = notes;

  String _cellPhone;
  String get cellPhone => _$this._cellPhone;
  set cellPhone(String cellPhone) => _$this._cellPhone = cellPhone;

  String _workPhone;
  String get workPhone => _$this._workPhone;
  set workPhone(String workPhone) => _$this._workPhone = workPhone;

  String _homePhone;
  String get homePhone => _$this._homePhone;
  set homePhone(String homePhone) => _$this._homePhone = homePhone;

  ResponseDataListBuilder<Reminder> _reminders;
  ResponseDataListBuilder<Reminder> get reminders =>
      _$this._reminders ??= new ResponseDataListBuilder<Reminder>();
  set reminders(ResponseDataListBuilder<Reminder> reminders) =>
      _$this._reminders = reminders;

  ResponseDataBuilder<UserPractice> _practice;
  ResponseDataBuilder<UserPractice> get practice =>
      _$this._practice ??= new ResponseDataBuilder<UserPractice>();
  set practice(ResponseDataBuilder<UserPractice> practice) =>
      _$this._practice = practice;

  ResponseDataBuilder<UserDoctor> _doctor;
  ResponseDataBuilder<UserDoctor> get doctor =>
      _$this._doctor ??= new ResponseDataBuilder<UserDoctor>();
  set doctor(ResponseDataBuilder<UserDoctor> doctor) => _$this._doctor = doctor;

  ResponseDataBuilder<UserPayer> _payer;
  ResponseDataBuilder<UserPayer> get payer =>
      _$this._payer ??= new ResponseDataBuilder<UserPayer>();
  set payer(ResponseDataBuilder<UserPayer> payer) => _$this._payer = payer;

  ListBuilder<ChronicDiseases> _chronicDiseases;
  ListBuilder<ChronicDiseases> get chronicDiseases =>
      _$this._chronicDiseases ??= new ListBuilder<ChronicDiseases>();
  set chronicDiseases(ListBuilder<ChronicDiseases> chronicDiseases) =>
      _$this._chronicDiseases = chronicDiseases;

  String _updatedAt;
  String get updatedAt => _$this._updatedAt;
  set updatedAt(String updatedAt) => _$this._updatedAt = updatedAt;

  String _createdAt;
  String get createdAt => _$this._createdAt;
  set createdAt(String createdAt) => _$this._createdAt = createdAt;

  UserBuilder();

  UserBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _firstName = _$v.firstName;
      _lastName = _$v.lastName;
      _status = _$v.status;
      _dob = _$v.dob;
      _dateAdded = _$v.dateAdded;
      _age = _$v.age;
      _address1 = _$v.address1;
      _address2 = _$v.address2;
      _city = _$v.city;
      _state = _$v.state;
      _points = _$v.points;
      _zipCode = _$v.zipCode;
      _preferredContact = _$v.preferredContact;
      _bloodGroup = _$v.bloodGroup;
      _deceaseDate = _$v.deceaseDate;
      _deceased = _$v.deceased;
      _subscribed = _$v.subscribed;
      _notificationSubscribed = _$v.notificationSubscribed;
      _emailSubscribed = _$v.emailSubscribed;
      _smsSubscribed = _$v.smsSubscribed;
      _height = _$v.height;
      _weight = _$v.weight;
      _breathing = _$v.breathing;
      _bloodPressure = _$v.bloodPressure;
      _pulse = _$v.pulse;
      _temperature = _$v.temperature;
      _impatientRelativeRisk = _$v.impatientRelativeRisk;
      _chronicDiseaseCount = _$v.chronicDiseaseCount;
      _quickNotes = _$v.quickNotes;
      _gender = _$v.gender;
      _code = _$v.code;
      _workEmail = _$v.workEmail;
      _personalEmail = _$v.personalEmail;
      _medicines = _$v.medicines?.toBuilder();
      _notes = _$v.notes?.toBuilder();
      _cellPhone = _$v.cellPhone;
      _workPhone = _$v.workPhone;
      _homePhone = _$v.homePhone;
      _reminders = _$v.reminders?.toBuilder();
      _practice = _$v.practice?.toBuilder();
      _doctor = _$v.doctor?.toBuilder();
      _payer = _$v.payer?.toBuilder();
      _chronicDiseases = _$v.chronicDiseases?.toBuilder();
      _updatedAt = _$v.updatedAt;
      _createdAt = _$v.createdAt;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(User other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$User;
  }

  @override
  void update(void updates(UserBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$User build() {
    _$User _$result;
    try {
      _$result = _$v ??
          new _$User._(
              id: id,
              firstName: firstName,
              lastName: lastName,
              status: status,
              dob: dob,
              dateAdded: dateAdded,
              age: age,
              address1: address1,
              address2: address2,
              city: city,
              state: state,
              points: points,
              zipCode: zipCode,
              preferredContact: preferredContact,
              bloodGroup: bloodGroup,
              deceaseDate: deceaseDate,
              deceased: deceased,
              subscribed: subscribed,
              notificationSubscribed: notificationSubscribed,
              emailSubscribed: emailSubscribed,
              smsSubscribed: smsSubscribed,
              height: height,
              weight: weight,
              breathing: breathing,
              bloodPressure: bloodPressure,
              pulse: pulse,
              temperature: temperature,
              impatientRelativeRisk: impatientRelativeRisk,
              chronicDiseaseCount: chronicDiseaseCount,
              quickNotes: quickNotes,
              gender: gender,
              code: code,
              workEmail: workEmail,
              personalEmail: personalEmail,
              medicines: _medicines?.build(),
              notes: _notes?.build(),
              cellPhone: cellPhone,
              workPhone: workPhone,
              homePhone: homePhone,
              reminders: _reminders?.build(),
              practice: _practice?.build(),
              doctor: _doctor?.build(),
              payer: _payer?.build(),
              chronicDiseases: _chronicDiseases?.build(),
              updatedAt: updatedAt,
              createdAt: createdAt);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'medicines';
        _medicines?.build();
        _$failedField = 'notes';
        _notes?.build();

        _$failedField = 'reminders';
        _reminders?.build();
        _$failedField = 'practice';
        _practice?.build();
        _$failedField = 'doctor';
        _doctor?.build();
        _$failedField = 'payer';
        _payer?.build();
        _$failedField = 'chronicDiseases';
        _chronicDiseases?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'User', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
