// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reminder.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Reminder> _$reminderSerializer = new _$ReminderSerializer();

class _$ReminderSerializer implements StructuredSerializer<Reminder> {
  @override
  final Iterable<Type> types = const [Reminder, _$Reminder];
  @override
  final String wireName = 'Reminder';

  @override
  Iterable serialize(Serializers serializers, Reminder object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'notifications',
      serializers.serialize(object.notifications,
          specifiedType: const FullType(
              BuiltList, const [const FullType(ReminderNotification)])),
      'entity_type',
      serializers.serialize(object.entityType,
          specifiedType: const FullType(String)),
      'entity_id',
      serializers.serialize(object.entityId,
          specifiedType: const FullType(int)),
    ];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.title != null) {
      result
        ..add('title')
        ..add(serializers.serialize(object.title,
            specifiedType: const FullType(String)));
    }
    if (object.endDate != null) {
      result
        ..add('end_date')
        ..add(serializers.serialize(object.endDate,
            specifiedType: const FullType(String)));
    }
    if (object.createdAt != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(String)));
    }
    if (object.updatedAt != null) {
      result
        ..add('updated_at')
        ..add(serializers.serialize(object.updatedAt,
            specifiedType: const FullType(String)));
    }
    if (object.startDate != null) {
      result
        ..add('start_date')
        ..add(serializers.serialize(object.startDate,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  Reminder deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ReminderBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'notifications':
          result.notifications.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ReminderNotification)]))
              as BuiltList);
          break;
        case 'entity_type':
          result.entityType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'entity_id':
          result.entityId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'end_date':
          result.endDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updated_at':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'start_date':
          result.startDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Reminder extends Reminder {
  @override
  final int id;
  @override
  final String title;
  @override
  final BuiltList<ReminderNotification> notifications;
  @override
  final String entityType;
  @override
  final int entityId;
  @override
  final String endDate;
  @override
  final String createdAt;
  @override
  final String updatedAt;
  @override
  final String startDate;

  factory _$Reminder([void Function(ReminderBuilder) updates]) =>
      (new ReminderBuilder()..update(updates)).build();

  _$Reminder._(
      {this.id,
      this.title,
      this.notifications,
      this.entityType,
      this.entityId,
      this.endDate,
      this.createdAt,
      this.updatedAt,
      this.startDate})
      : super._() {
    if (notifications == null) {
      throw new BuiltValueNullFieldError('Reminder', 'notifications');
    }
    if (entityType == null) {
      throw new BuiltValueNullFieldError('Reminder', 'entityType');
    }
    if (entityId == null) {
      throw new BuiltValueNullFieldError('Reminder', 'entityId');
    }
  }

  @override
  Reminder rebuild(void Function(ReminderBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ReminderBuilder toBuilder() => new ReminderBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Reminder &&
        id == other.id &&
        title == other.title &&
        notifications == other.notifications &&
        entityType == other.entityType &&
        entityId == other.entityId &&
        endDate == other.endDate &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        startDate == other.startDate;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, id.hashCode), title.hashCode),
                                notifications.hashCode),
                            entityType.hashCode),
                        entityId.hashCode),
                    endDate.hashCode),
                createdAt.hashCode),
            updatedAt.hashCode),
        startDate.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Reminder')
          ..add('id', id)
          ..add('title', title)
          ..add('notifications', notifications)
          ..add('entityType', entityType)
          ..add('entityId', entityId)
          ..add('endDate', endDate)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('startDate', startDate))
        .toString();
  }
}

class ReminderBuilder implements Builder<Reminder, ReminderBuilder> {
  _$Reminder _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  ListBuilder<ReminderNotification> _notifications;
  ListBuilder<ReminderNotification> get notifications =>
      _$this._notifications ??= new ListBuilder<ReminderNotification>();
  set notifications(ListBuilder<ReminderNotification> notifications) =>
      _$this._notifications = notifications;

  String _entityType;
  String get entityType => _$this._entityType;
  set entityType(String entityType) => _$this._entityType = entityType;

  int _entityId;
  int get entityId => _$this._entityId;
  set entityId(int entityId) => _$this._entityId = entityId;

  String _endDate;
  String get endDate => _$this._endDate;
  set endDate(String endDate) => _$this._endDate = endDate;

  String _createdAt;
  String get createdAt => _$this._createdAt;
  set createdAt(String createdAt) => _$this._createdAt = createdAt;

  String _updatedAt;
  String get updatedAt => _$this._updatedAt;
  set updatedAt(String updatedAt) => _$this._updatedAt = updatedAt;

  String _startDate;
  String get startDate => _$this._startDate;
  set startDate(String startDate) => _$this._startDate = startDate;

  ReminderBuilder();

  ReminderBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _title = _$v.title;
      _notifications = _$v.notifications?.toBuilder();
      _entityType = _$v.entityType;
      _entityId = _$v.entityId;
      _endDate = _$v.endDate;
      _createdAt = _$v.createdAt;
      _updatedAt = _$v.updatedAt;
      _startDate = _$v.startDate;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Reminder other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Reminder;
  }

  @override
  void update(void Function(ReminderBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Reminder build() {
    _$Reminder _$result;
    try {
      _$result = _$v ??
          new _$Reminder._(
              id: id,
              title: title,
              notifications: notifications.build(),
              entityType: entityType,
              entityId: entityId,
              endDate: endDate,
              createdAt: createdAt,
              updatedAt: updatedAt,
              startDate: startDate);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'notifications';
        notifications.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Reminder', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
