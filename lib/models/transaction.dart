import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'transaction.g.dart';

abstract class Transaction implements Built<Transaction, TransactionBuilder> {
  Transaction._();

  factory Transaction([updates(TransactionBuilder b)]) = _$Transaction;

  int get id;

  int get points;

  String get type;

  BuiltMap<String, String> get meta;

  @BuiltValueField(wireName: "created_at")
  String get createdAt;

  static Serializer<Transaction> get serializer => _$transactionSerializer;

  bool isCredit() {
    return points > 0;
  }
}
