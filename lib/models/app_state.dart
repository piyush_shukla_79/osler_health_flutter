import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:entity_state/entity_state.dart';
import 'package:osler_health/models/medicine.dart';
import 'package:osler_health/models/notification.dart';
import 'package:osler_health/models/reminder.dart';
import 'package:osler_health/models/transaction.dart';
import 'package:osler_health/models/user.dart';

part 'app_state.g.dart';

abstract class AppState implements Built<AppState, AppStateBuilder> {
  @nullable
  UserState get user;

  @nullable
  AppNotificationState get appNotification;

  @nullable
  ReminderState get reminder;

  @nullable
  MedicineState get medicines;

  static Serializer<AppState> get serializer => _$appStateSerializer;

  factory AppState() {
    return _$AppState((b) => b
      ..user = UserState().toBuilder()
      ..appNotification = AppNotificationState().toBuilder()
      ..reminder = ReminderState().toBuilder()
      ..medicines = MedicineState().toBuilder());
  }

  AppState._();
}

abstract class UserState implements Built<UserState, UserStateBuilder> {
  @nullable
  bool get isLoading;

  @nullable
  bool get isLoaded;

  @nullable
  bool get isLoggedIn;

  @nullable
  User get profile;

  @nullable
  BuiltList<Transaction> get transactions;

  static Serializer<UserState> get serializer => _$userStateSerializer;

  factory UserState() {
    return _$UserState((b) => b
      ..isLoggedIn = false
      ..isLoading = false
      ..isLoaded = false
      ..transactions = BuiltList<Transaction>().toBuilder());
  }

  UserState._();
}

abstract class AppNotificationState
    with
        EntityState<AppNotification, int, AppNotificationState,
            AppNotificationBuilder>
    implements
        Built<AppNotificationState, AppNotificationStateBuilder> {
  bool get isLoading;

  bool get isLoaded;

  BuiltList<int> get ids;

  BuiltMap<int, AppNotification> get entities;

  static Serializer<AppNotificationState> get serializer =>
      _$appNotificationStateSerializer;

  factory AppNotificationState() {
    return _$AppNotificationState((b) => b
      ..isLoading = false
      ..isLoaded = false
      ..ids = BuiltList<int>().toBuilder()
      ..entities = MapBuilder<int, AppNotification>());
  }

  AppNotificationState._();

  @override
  int getId(AppNotification data) {
    return data.id;
  }
}

abstract class ReminderState
    with EntityState<Reminder, int, ReminderState, ReminderStateBuilder>
    implements Built<ReminderState, ReminderStateBuilder> {
  BuiltList<int> get ids;

  BuiltMap<int, Reminder> get entities;

  bool get isLoading;

  ReminderState._();

  factory ReminderState([updates(ReminderStateBuilder b)]) {
    return _$ReminderState((b) => b
      ..isLoading = false
      ..ids = ListBuilder<int>()
      ..entities = MapBuilder<int, Reminder>());
  }

  static Serializer<ReminderState> get serializer => _$reminderStateSerializer;

  @override
  int getId(Reminder data) {
    return data.entityId;
  }
}

abstract class MedicineState
    with EntityState<Medicine, int, MedicineState, MedicineStateBuilder>
    implements Built<MedicineState, MedicineStateBuilder> {
  BuiltList<int> get ids;

  BuiltMap<int, Medicine> get entities;

  bool get isLoading;

  MedicineState._();

  factory MedicineState([updates(MedicineStateBuilder b)]) {
    return _$MedicineState((b) => b
      ..isLoading = false
      ..ids = ListBuilder<int>()
      ..entities = MapBuilder<int, Medicine>());
  }

  static Serializer<MedicineState> get serializer => _$medicineStateSerializer;

  @override
  int getId(Medicine data) {
    return data.medicinePatientId;
  }
}
