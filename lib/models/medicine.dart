import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:osler_health/models/mixins/base_model.dart';

part 'medicine.g.dart';

abstract class Medicine
    with BaseModel
    implements Built<Medicine, MedicineBuilder> {
  int get id;

  String get name;

  @nullable
  @BuiltValueField(wireName: "company_name")
  String get companyName;

  @nullable
  String get price;

  @nullable
  int get quantity;

  @nullable
  @BuiltValueField(wireName: "medicine_patient_id")
  int get medicinePatientId;

  String get dosage;

  @nullable
  String get frequency;

  @nullable
  @BuiltValueField(wireName: "start_date")
  String get startDate;

  @nullable
  @BuiltValueField(wireName: "end_date")
  String get endDate;

  @nullable
  @BuiltValueField(wireName: "occurrence")
  int get occurrence;

  @nullable
  BuiltList<bool> get value;

  @nullable
  int get duration;

  @nullable
  int get interval;

  @nullable
  @BuiltValueField(wireName: "created_at")
  String get createdAt;

  @nullable
  @BuiltValueField(wireName: "updated_at")
  String get updatedAt;

  bool hasEndDate() {
    return endDate != null;
  }

  bool hasInterval() {
    return interval != null;
  }

  Medicine._();

  factory Medicine([updates(MedicineBuilder b)]) = _$Medicine;

  static Serializer<Medicine> get serializer => _$medicineSerializer;
}
