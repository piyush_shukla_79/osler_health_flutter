// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medicine.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Medicine> _$medicineSerializer = new _$MedicineSerializer();

class _$MedicineSerializer implements StructuredSerializer<Medicine> {
  @override
  final Iterable<Type> types = const [Medicine, _$Medicine];
  @override
  final String wireName = 'Medicine';

  @override
  Iterable serialize(Serializers serializers, Medicine object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'dosage',
      serializers.serialize(object.dosage,
          specifiedType: const FullType(String)),
    ];
    if (object.companyName != null) {
      result
        ..add('company_name')
        ..add(serializers.serialize(object.companyName,
            specifiedType: const FullType(String)));
    }
    if (object.price != null) {
      result
        ..add('price')
        ..add(serializers.serialize(object.price,
            specifiedType: const FullType(String)));
    }
    if (object.quantity != null) {
      result
        ..add('quantity')
        ..add(serializers.serialize(object.quantity,
            specifiedType: const FullType(int)));
    }
    if (object.medicinePatientId != null) {
      result
        ..add('medicine_patient_id')
        ..add(serializers.serialize(object.medicinePatientId,
            specifiedType: const FullType(int)));
    }
    if (object.frequency != null) {
      result
        ..add('frequency')
        ..add(serializers.serialize(object.frequency,
            specifiedType: const FullType(String)));
    }
    if (object.startDate != null) {
      result
        ..add('start_date')
        ..add(serializers.serialize(object.startDate,
            specifiedType: const FullType(String)));
    }
    if (object.endDate != null) {
      result
        ..add('end_date')
        ..add(serializers.serialize(object.endDate,
            specifiedType: const FullType(String)));
    }
    if (object.occurrence != null) {
      result
        ..add('occurrence')
        ..add(serializers.serialize(object.occurrence,
            specifiedType: const FullType(int)));
    }
    if (object.value != null) {
      result
        ..add('value')
        ..add(serializers.serialize(object.value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(bool)])));
    }
    if (object.duration != null) {
      result
        ..add('duration')
        ..add(serializers.serialize(object.duration,
            specifiedType: const FullType(int)));
    }
    if (object.interval != null) {
      result
        ..add('interval')
        ..add(serializers.serialize(object.interval,
            specifiedType: const FullType(int)));
    }
    if (object.createdAt != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(String)));
    }
    if (object.updatedAt != null) {
      result
        ..add('updated_at')
        ..add(serializers.serialize(object.updatedAt,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  Medicine deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MedicineBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'company_name':
          result.companyName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'price':
          result.price = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'quantity':
          result.quantity = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'medicine_patient_id':
          result.medicinePatientId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'dosage':
          result.dosage = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'frequency':
          result.frequency = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'start_date':
          result.startDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'end_date':
          result.endDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'occurrence':
          result.occurrence = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'value':
          result.value.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(bool)]))
              as BuiltList);
          break;
        case 'duration':
          result.duration = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'interval':
          result.interval = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updated_at':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Medicine extends Medicine {
  @override
  final int id;
  @override
  final String name;
  @override
  final String companyName;
  @override
  final String price;
  @override
  final int quantity;
  @override
  final int medicinePatientId;
  @override
  final String dosage;
  @override
  final String frequency;
  @override
  final String startDate;
  @override
  final String endDate;
  @override
  final int occurrence;
  @override
  final BuiltList<bool> value;
  @override
  final int duration;
  @override
  final int interval;
  @override
  final String createdAt;
  @override
  final String updatedAt;

  factory _$Medicine([void Function(MedicineBuilder) updates]) =>
      (new MedicineBuilder()..update(updates)).build();

  _$Medicine._(
      {this.id,
      this.name,
      this.companyName,
      this.price,
      this.quantity,
      this.medicinePatientId,
      this.dosage,
      this.frequency,
      this.startDate,
      this.endDate,
      this.occurrence,
      this.value,
      this.duration,
      this.interval,
      this.createdAt,
      this.updatedAt})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('Medicine', 'id');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('Medicine', 'name');
    }
    if (dosage == null) {
      throw new BuiltValueNullFieldError('Medicine', 'dosage');
    }
  }

  @override
  Medicine rebuild(void Function(MedicineBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MedicineBuilder toBuilder() => new MedicineBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Medicine &&
        id == other.id &&
        name == other.name &&
        companyName == other.companyName &&
        price == other.price &&
        quantity == other.quantity &&
        medicinePatientId == other.medicinePatientId &&
        dosage == other.dosage &&
        frequency == other.frequency &&
        startDate == other.startDate &&
        endDate == other.endDate &&
        occurrence == other.occurrence &&
        value == other.value &&
        duration == other.duration &&
        interval == other.interval &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(0,
                                                                    id.hashCode),
                                                                name.hashCode),
                                                            companyName.hashCode),
                                                        price.hashCode),
                                                    quantity.hashCode),
                                                medicinePatientId.hashCode),
                                            dosage.hashCode),
                                        frequency.hashCode),
                                    startDate.hashCode),
                                endDate.hashCode),
                            occurrence.hashCode),
                        value.hashCode),
                    duration.hashCode),
                interval.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Medicine')
          ..add('id', id)
          ..add('name', name)
          ..add('companyName', companyName)
          ..add('price', price)
          ..add('quantity', quantity)
          ..add('medicinePatientId', medicinePatientId)
          ..add('dosage', dosage)
          ..add('frequency', frequency)
          ..add('startDate', startDate)
          ..add('endDate', endDate)
          ..add('occurrence', occurrence)
          ..add('value', value)
          ..add('duration', duration)
          ..add('interval', interval)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class MedicineBuilder implements Builder<Medicine, MedicineBuilder> {
  _$Medicine _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _companyName;
  String get companyName => _$this._companyName;
  set companyName(String companyName) => _$this._companyName = companyName;

  String _price;
  String get price => _$this._price;
  set price(String price) => _$this._price = price;

  int _quantity;
  int get quantity => _$this._quantity;
  set quantity(int quantity) => _$this._quantity = quantity;

  int _medicinePatientId;
  int get medicinePatientId => _$this._medicinePatientId;
  set medicinePatientId(int medicinePatientId) =>
      _$this._medicinePatientId = medicinePatientId;

  String _dosage;
  String get dosage => _$this._dosage;
  set dosage(String dosage) => _$this._dosage = dosage;

  String _frequency;
  String get frequency => _$this._frequency;
  set frequency(String frequency) => _$this._frequency = frequency;

  String _startDate;
  String get startDate => _$this._startDate;
  set startDate(String startDate) => _$this._startDate = startDate;

  String _endDate;
  String get endDate => _$this._endDate;
  set endDate(String endDate) => _$this._endDate = endDate;

  int _occurrence;
  int get occurrence => _$this._occurrence;
  set occurrence(int occurrence) => _$this._occurrence = occurrence;

  ListBuilder<bool> _value;
  ListBuilder<bool> get value => _$this._value ??= new ListBuilder<bool>();
  set value(ListBuilder<bool> value) => _$this._value = value;

  int _duration;
  int get duration => _$this._duration;
  set duration(int duration) => _$this._duration = duration;

  int _interval;
  int get interval => _$this._interval;
  set interval(int interval) => _$this._interval = interval;

  String _createdAt;
  String get createdAt => _$this._createdAt;
  set createdAt(String createdAt) => _$this._createdAt = createdAt;

  String _updatedAt;
  String get updatedAt => _$this._updatedAt;
  set updatedAt(String updatedAt) => _$this._updatedAt = updatedAt;

  MedicineBuilder();

  MedicineBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _companyName = _$v.companyName;
      _price = _$v.price;
      _quantity = _$v.quantity;
      _medicinePatientId = _$v.medicinePatientId;
      _dosage = _$v.dosage;
      _frequency = _$v.frequency;
      _startDate = _$v.startDate;
      _endDate = _$v.endDate;
      _occurrence = _$v.occurrence;
      _value = _$v.value?.toBuilder();
      _duration = _$v.duration;
      _interval = _$v.interval;
      _createdAt = _$v.createdAt;
      _updatedAt = _$v.updatedAt;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Medicine other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Medicine;
  }

  @override
  void update(void Function(MedicineBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Medicine build() {
    _$Medicine _$result;
    try {
      _$result = _$v ??
          new _$Medicine._(
              id: id,
              name: name,
              companyName: companyName,
              price: price,
              quantity: quantity,
              medicinePatientId: medicinePatientId,
              dosage: dosage,
              frequency: frequency,
              startDate: startDate,
              endDate: endDate,
              occurrence: occurrence,
              value: _value?.build(),
              duration: duration,
              interval: interval,
              createdAt: createdAt,
              updatedAt: updatedAt);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'value';
        _value?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Medicine', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
