import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_doctor.g.dart';

abstract class UserDoctor implements Built<UserDoctor, UserDoctorBuilder> {
  @nullable
  int get id;

  @nullable
  @BuiltValueField(wireName: "first_name")
  String get firstName;

  @nullable
  @BuiltValueField(wireName: "last_name")
  String get lastName;

  @nullable
  String get email;

  @nullable
  String get phone;

  @nullable
  String get address;

  @nullable
  @BuiltValueField(wireName: "specialty_id")
  int get specialtyId;

  @nullable
  @BuiltValueField(wireName: "practice_id")
  int get practiceId;

  @nullable
  @BuiltValueField(wireName: "date_of_birth")
  String get dob;

  @nullable
  @BuiltValueField(wireName: "updated_at")
  String get updatedAt;

  @nullable
  @BuiltValueField(wireName: "created_at")
  String get createdAt;

  @memoized
  String getFullName() {
    return firstName +
        ((lastName != null) ? (lastName.length > 0) ? " $lastName" : "" : "");
  }

  UserDoctor._();

  factory UserDoctor([updates(UserDoctorBuilder b)]) = _$UserDoctor;

  static Serializer<UserDoctor> get serializer => _$userDoctorSerializer;
}
