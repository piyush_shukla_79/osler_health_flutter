import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_notes.g.dart';

abstract class UserNotes implements Built<UserNotes, UserNotesBuilder> {
  @nullable
  int get id;

  @nullable
  @BuiltValueField(wireName: "patient_id")
  int get patientId;

  @nullable
  @BuiltValueField(wireName: "user_id")
  int get userId;

  @nullable
  String get notes;

  @nullable
  String get date;

  UserNotes._();

  factory UserNotes([updates(UserNotesBuilder b)]) = _$UserNotes;

  static Serializer<UserNotes> get serializer => _$userNotesSerializer;
}
