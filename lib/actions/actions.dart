import 'package:osler_health/models/user.dart';

class LoadingAction {
  LoadingAction();
}

class LoadingCompleteAction {
  LoadingCompleteAction();
}

class CheckSession {
  CheckSession();
}

class CheckSessionComplete {
  User currentUser;
  bool isSuccess;

  CheckSessionComplete();
}

class Logout {}
