import 'package:osler_health/models/reminder.dart';

class ListReminders {}

class ListRemindersComplete {
  List<Reminder> reminders;

  ListRemindersComplete(this.reminders);
}

class UpdateReminder {
  Reminder reminder;

  UpdateReminder(this.reminder);
}

class UpdateReminderComplete {
  Reminder reminder;

  UpdateReminderComplete(this.reminder);
}

class CreateReminder {
  Reminder reminder;

  CreateReminder(this.reminder);
}

class CreateReminderComplete {
  Reminder reminder;

  CreateReminderComplete(this.reminder);
}
