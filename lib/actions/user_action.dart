import 'package:meta/meta.dart';
import 'package:osler_health/models/transaction.dart';
import 'package:osler_health/models/user.dart';

class LoadLocalUsers {}

class LoadLocalUsersComplete {
  List<User> users;

  LoadLocalUsersComplete({this.users});
}

class LogIn extends LoadingTrigger {
  String email;
  String password;

  LogIn(this.email, this.password);
}

class LogInComplete extends LoadingTrigger {
  User currentUser;
  bool isSuccess;

  LogInComplete({@required this.currentUser, this.isSuccess});
}

class UpdateUserInfo {
  User user;

  UpdateUserInfo(this.user);
}

class FetchLoggedInUserDetails extends LoadingTrigger {
  FetchLoggedInUserDetails();
}

class UpdateLoggedInUser extends LoadingTrigger {
  User user;

  UpdateLoggedInUser(this.user);
}

class LoadTransactions {}

class LoadTransactionsComplete {
  List<Transaction> transactions;

  LoadTransactionsComplete(this.transactions);
}

class LoadingTrigger {}

class StopLoading{}
