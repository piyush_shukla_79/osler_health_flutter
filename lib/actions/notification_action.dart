import 'package:meta/meta.dart';
import 'package:osler_health/models/notification.dart';

class ListNotification {
  ListNotification();
}

class ListNotificationComplete {
  List<AppNotification> notifications;

  ListNotificationComplete(this.notifications);
}

class UpdateNotification {
  AppNotification notification;

  UpdateNotification({@required this.notification});
}
