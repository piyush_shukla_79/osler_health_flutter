import 'package:osler_health/models/medicine.dart';

class ListMedicinesComplete {
  List<Medicine> medicines;

  ListMedicinesComplete(this.medicines);
}

class CreateMedicine {
  Map<String, dynamic> medicine;

  CreateMedicine(this.medicine);
}

class CreateMedicineComplete {
  Medicine medicine;

  CreateMedicineComplete(this.medicine);
}
