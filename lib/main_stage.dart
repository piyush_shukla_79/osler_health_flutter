import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:osler_health/app_config.dart';
import 'package:osler_health/main.dart';
import 'package:osler_health/utils/styles.dart';

void main() {
  AppConfig.setupEnv(Environment.stage);
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(OslerHealthApp());
  });

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Styles.appColor,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark
  ));
}
